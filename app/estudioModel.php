<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estudioModel extends Model
{
    protected $table = 'estudio';
    public function tipo_estudio()
    {
        return $this->belongsTo('App\tipoEstudioModel','tipo_estudio_id');
    }
    public function institucion()
    {
        return $this->belongsTo('App\institucionModel','institucion_id');
    }
    public function carrera()
    {
        return $this->belongsTo('App\carreraModel','carrera_id');
    }

    public function estadoestudio()
    {
        return $this->belongsTo('App\estado_estudioModel','estado_estudio_id');
    }
}
