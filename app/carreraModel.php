<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class carreraModel extends Model
{

    use SoftDeletes;

    protected $table = 'carrera';
    protected $dates = ['deleted_at'];

    public function facultad()
    {
        return $this->belongsTo('App\facultadModel', 'facultad_id');
    }

     public function institucion()
    {
        return $this->belongsTo('App\institucionModel', 'institucion_id');
    }



    public function titulo()
    {
        return $this->belongsTo('App\tituloModel', 'titulo_id');
    }

    public function usuarios()
    {
        return $this->hasMany('App\usuarioModel', 'carrera_id');
    }

}
