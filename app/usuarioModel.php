<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;

class usuarioModel extends Model implements AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;//, SoftDeletes;

    protected $table = 'usuario';

    public function carrera()
    {
        return $this->belongsTo('App\carreraModel', 'carrera_id');
    }

    public function rol()
    {
        return $this->belongsTo('App\rolModel', 'rol_id');
    }

     // full_name
    public function getFullNameAttribute($value)
    {
        return "{$this->apellidos} {$this->nombres}";
    }

    

}
