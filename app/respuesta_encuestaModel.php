<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class respuesta_encuestaModel extends Model
{
    protected $table='respuesta_encuesta';

     public function profesional()
    {
        return $this->belongsTo('App\usuarioModel','usuario_id');
    }

    public function encuesta()
    {
        return $this->belongsTo('App\encuestaModel','encuesta_id');
    }
}
