<?php 
use Illuminate\Support\Facades\Storage;
use App\respuesta_encuestaModel;
use App\encuestaModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App\configuracionModel;
use Illuminate\Http\Request;
    function sanear_string($s)
    {
     
        $s = mb_convert_encoding($s, 'UTF-8','UTF-8');
            $s = preg_replace("/á|à|â|ã|ª/","a",$s);
            $s = preg_replace("/Á|À|Â|Ã/","A",$s);
            $s = preg_replace("/é|è|ê/","e",$s);
            $s = preg_replace("/É|È|Ê/","E",$s);
            $s = preg_replace("/í|ì|î/","i",$s);
            $s = preg_replace("/Í|Ì|Î/","I",$s);
            $s = preg_replace("/ó|ò|ô|õ|º/","o",$s);
            $s = preg_replace("/Ó|Ò|Ô|Õ/","O",$s);
            $s = preg_replace("/ú|ù|û/","u",$s);
            $s = preg_replace("/Ú|Ù|Û/","U",$s);
            $s = str_replace(" ","_",$s);
            $s = str_replace("ñ","n",$s);
            $s = str_replace(".","",$s);
            $s = str_replace("Ñ","N",$s);
            
            $s = preg_replace('/[^a-zA-Z0-9_.-]/', '', $s);
            return $s;
    }
    function obtener_url($documento){
        $ruta_area=App\areaModel::find($documento->area_id)->ruta;           
        $ruta_tipo_doc=App\tipodocumentoModel::find($documento->tipo_documento_id)->ruta;
        $ruta_anio='/'.date( "Y", strtotime($documento->fecha_emision));
        $dir='/archivos'.$ruta_area.$ruta_tipo_doc.$ruta_anio; 
        $full_path=$dir.'/'.decrypt($documento->archivo); 
        return asset('storage/app'.$full_path);
        //return Storage::response($full_path,'sweet.pdf');
    }


    function Autorizar($url)
   {
      // die($url);
       $urlar=  explode("/", $url);

       if($urlar[0]=="resultados"){
        $url=$urlar[0];
       }else{
        if(isset($urlar[1])){
          $url=$urlar[1];
        }else{
          $url=$urlar[0];
        }
       }
      //dd($url);
       //$url=$urlar[1];
       //dd($url);
       //show(\Auth::user()->tipousuario_id);
       if(!Auth::check()){
          die(Response::view('errors/403', array(), 403));
       }

       if (\Auth::user()->rol_id!=1)
       {
            $tabla=DB::table("menu")
                ->join("menu_rol as b","menu.id","=","b.idmenu") 
                ->select("menu.*")
                ->where(function($query) use ($url){
                    $query->where("menu.ruta","like",$url."%")->where("b.idrol",\Auth::user()->rol_id);
                })
                ->first();
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            if($tabla==null)  
            {
              //dd("No tiene permiso");
                //Session::flash("msgacceso","Se detectó un acceso no permitido el ".date("Y-m-d"). " a las ". date("H:i:s").".");
                
              //  MisFunciones::auditoria("Acceso no permitido 403");                
                //return "Ruta no permitida";
                die(Response::view('errors/403', array(), 403));          
            }
       }
   }
   function hojas_anexos($id){
    return anexoModel::where('documento_id',$id)->sum('hojas');
   }
   function obtener_mes($id){
    $meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
    return $meses[$id];
   }
   function numero_acta($numero){
    return sprintf("%04d", $numero);
   }
   function primeras_letras($nombres){

    $ret = ''; 
    foreach (explode(' ', $nombres) as $word) {
     $ret .= strtoupper($word[0]); 
    }
    return $ret;
   }
  

   function guardar_accion($idaccion, $mensajes)
   {
    $bitacora = new bitacoraModel();
    $bitacora->usuario_id= Auth::id();
    $bitacora->accion_id=$idaccion;
    $bitacora->ip=getRealIP();
    $bitacora->pcname=str_replace('.', '', gethostname());  
    $bitacora->device=getDevice();
    //$bitacora->save();
    return 1;


   }

   // Para Obtener la ip del cliente
 function getRealIP()
   {
    $ip=null;
    if (isset($_SERVER["HTTP_CLIENT_IP"]))
    {
        $ip=$_SERVER["HTTP_CLIENT_IP"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
        $ip=$_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
    {
        $ip=$_SERVER["HTTP_X_FORWARDED"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
    {
        $ip=$_SERVER["HTTP_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED"]))
    {
        $ip=$_SERVER["HTTP_FORWARDED"];
    }
    else
    {
        $ip=$_SERVER["REMOTE_ADDR"];
    }

   if ($ip=="::1") {
        $ip="127.0.0.1";
      }  
    return $ip;

}


function getDevice()
{

$tablet_browser = 0;
$mobile_browser = 0;
$body_class = 'desktop';
 
if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $tablet_browser++;
    $body_class = "tablet";
}
 
if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $mobile_browser++;
    $body_class = "mobile";
}
 
if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
    $mobile_browser++;
    $body_class = "mobile";
}
 
$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
    'newt','noki','palm','pana','pant','phil','play','port','prox',
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
    'wapr','webc','winw','winw','xda ','xda-');
 
if (in_array($mobile_ua,$mobile_agents)) {
    $mobile_browser++;
}
 
if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
    $mobile_browser++;
    //Check for tablets on opera mini alternative headers
    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
      $tablet_browser++;
    }
}
  if ($tablet_browser > 0) {
  // Si es tablet has lo que necesites
     return 'tablet '.$_SERVER['HTTP_USER_AGENT'];
  }
  else if ($mobile_browser > 0) {
  // Si es dispositivo mobil has lo que necesites
     return 'celular '.$_SERVER['HTTP_USER_AGENT'];
  }
  else {
  // Si es ordenador de escritorio has lo que necesites
     return 'Pc '.$_SERVER['HTTP_USER_AGENT'];
  }  
}



function obtenerNombres_Apellidos($nombres_unidos)
{
    $tokens = explode(' ', trim($nombres_unidos));
  /* arreglo donde se guardan las "palabras" del nombre */
  $names = array();
  /* palabras de apellidos (y nombres) compuetos */
  $special_tokens = array('da', 'de', 'del', 'la', 'las', 'los', 'mac', 'mc', 'van', 'von', 'y', 'i', 'san', 'santa');
  
  $prev = "";
  foreach($tokens as $token) {
      $_token = strtolower($token);
      if(in_array($_token, $special_tokens)) {
          $prev .= "$token ";
      } else {
          $names[] = $prev. $token;
          $prev = "";
      }
  }
  
  $num_nombres = count($names);
  $nombres = $apellidos = "";
  switch ($num_nombres) {
      case 0:
          $nombres = '';
          break;
      case 1: 
          $nombres = $names[0];
          break;
      case 2:
          $nombres    = $names[0];
          $apellidos  = $names[1];
          break;
      case 3:
          $apellidos = $names[0] . ' ' . $names[1];
          $nombres   = $names[2];
      default:
          $apellidos = $names[0] . ' '. $names[1];
          unset($names[0]);
          unset($names[1]);
          
          $nombres = implode(' ', $names);
          break;
  }
  
  //CONVERTIR CON FORMATO PRIMER LETRA EN MAYUSCULA Y LO OTRO EN MINUSCULA
  $nombres    = mb_convert_case($nombres, MB_CASE_TITLE, 'UTF-8');
  $apellidos  = mb_convert_case($apellidos, MB_CASE_TITLE, 'UTF-8');

  //CONVERTIR TODO EN MAYUSCULA-> strtoupper


  $resultado=array('nombres' =>mb_strtoupper($nombres, 'UTF-8'), 'apellidos' =>mb_strtoupper($apellidos, 'UTF-8'));
  return $resultado;
}


function comprobar_si_respondio($id){
  return respuesta_encuestaModel::where('encuesta_id',$id)
  ->where('usuario_id',Auth::id())->count();
}
function estado_respuesta_encuesta($id){ 
  $encuesta=encuestaModel::find($id);

  if($encuesta->especial==1){
    $rrr=App\preguntaModel::where('order',5)
    ->where('carrera_id',Auth::user()->carrera_id)
    ->first();
    if($rrr==null){
      return -1;
    }
  }
  /*
  $anios=$encuesta->anios;
  $anio_graduacion=intval(Auth::user()->anio_graduacion);
  $anio_ingreso_valido=$anio_graduacion+$anios;
  $anio_actual=intval(date('Y'));
  if($anio_actual<$anio_ingreso_valido){
    //aun no esta habilitada
    return 2;
  }*/
  if(Auth::user()->periodo_academico==null||!contiene_palabra($encuesta->periodos,Auth::user()->periodo_academico)){
    return 2;
  }

   $respondido=respuesta_encuestaModel::where('encuesta_id',$id)
    ->where('usuario_id',Auth::id())->count();
    //si ya fue respondida
    if($respondido>0){
      return 1;
    }

    return 3;
}

function contiene_palabra($texto, $palabra){
    return preg_match('*\b' . preg_quote($palabra) . '\b*i', $texto);
}


function envio_correo_uleam($dato){
  $dato['codigoapp']=configuracionModel::find(2)->valor;
    $url_consulta="https://prod-03.westus.logic.azure.com:443/workflows/1e1f213de2bc46aabbe07dc2cc625de5/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=zS9xZsD4ICsFS_a6av_RJsCf3qJMAatsAIgodW1L-YM";
     $client = new \GuzzleHttp\Client([
        'headers' => [ 'Content-Type' => 'application/json' ]
      ]);
      $res=$client->post($url_consulta,['body' => json_encode($dato)]);
      $resultado= json_decode($res->getBody()->getContents());
      return $resultado;
  }



?>