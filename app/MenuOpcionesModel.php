<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuOpcionesModel extends Model
{
    //
    protected $table = 'menu';
    protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [       
        	//'letra'=>'required|unique:comportamiento'. ($id ? ",id,$id" : ''),         
			//'menu'=>'required|unique:menu'. ($id ? ",id,$id" : ''),
			'menu'=>'required',
			'ruta'=>'required',
			'nivel'=>'required',
			'idmain'=>'required',
			'orden'=>'required'
		], $merge);
        } 
    /**public  function tiposolicitudes () {
		return $this->belongsToMany('App\TipoSolicitudesModel', 'tiposolicitudescamposextras', 'id_campos_extras', 'id_tipo_solicitudes')->withTimestamps();
	} 
	*/
}