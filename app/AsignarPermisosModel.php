<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignarPermisosModel extends Model
{
    protected $table  = 'menu_rol';
    protected $hidden = [];
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'idrol'  => 'required',
                'idmenu' => 'required',
            ], $merge);
    }
}
