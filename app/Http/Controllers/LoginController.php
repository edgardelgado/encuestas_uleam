<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*   function inicio(){
    dd('ss');
    }
    if(Auth::Check()){
    if(Auth::user()->rol_id==1){
    // dd('ss');
    return View('admin.inicio');
    }elseif(Auth::user()->rol_id==2){
    return View('encargado.inicio');
    }elseif(Auth::user()->rol_id==4){
    return View('profesional.inicio');
    }elseif(Auth::user()->rol_id==3){
    return View('empleador.inicio');
    }

    }else{
    return View('welcome');
    }
    }*/

    public function index()
    {

        //vista de login
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function consulta_usuario($id)
    {

        //  $id=strtoupper($id);
        $resultado = usuarioModel::where('identificacion', 'like', '%' . $id . '%')
            ->first(['email', 'nombres', 'apellidos', 'identificacion as cedula', 'id']);
        return view('resultado_busqueda', compact('resultado'));
    }

    public function enviar_clave(Request $request)
    {
        ini_set('max_execution_time', 300); 
        $usuario           = usuarioModel::find($request->id_usuario);
        $clave             = strtoupper(str_random(1) . $request->id_usuario . str_random(1));
        $usuario->password = bcrypt($clave);
        $usuario->save();
        //la que se envia al correo es $clave
        //aqui se envia el crreo
        $email     = $usuario->email;
        $nombres   = $usuario->nombres;
        $apellidos = $usuario->apellidos;
        $clave2    = $clave;
        $vista=view('cuenta.recuperacion_clave',compact('clave2','email','nombres','apellidos'))->render(); 
        $dato['usuario']=$email;
        $dato['asunto']="SISTEMA DE ENCUESTAS - CLAVE DE ACCESO";
        $dato['mensaje']=$vista;
      return envio_correo_uleam($dato);
       /* Mail::send('cuenta.recuperacion_clave', ['clave2' => $clave2, 'email' => $email, 'nombres' => $nombres, 'apellidos' => $apellidos],
            function ($m) use ($email, $nombres, $apellidos, $clave2) {
                $nombres = "Uleam";
                $m->from("kiss30octu@hotmail.com", "SISTEMA DE ENCUESTAS ULEAM");
                $m->to($email, $nombres, $clave2)->subject('Clave de Acceso');

            });
            */
        //Session::flash('message', 'A su correo eletrónico se le envió la clave para acceder a las encuestas');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'identificacion'=>'required',
            'password'=>'required'
        ]);
        $resul=usuarioModel::where('identificacion',$request['identificacion'])->first();
        if(is_null($resul)){
            Session::flash('message-error', 'Usuario no registrado en Plataforma');
            return Redirect::to('login');
        }
        
        if (Auth::attempt(['identificacion' => $request['identificacion'], 'password' => $request['password']])) {
            return Redirect::to('/inicio');
        } else {
            Session::flash('message-error', 'Usuario o clave incorrecto');
            return Redirect::to('login');
        }
    }

    public function login_profesional(Request $request)
    {
        $cedula=mb_strtoupper($request['cedula']);
        if (Auth::attempt(['identificacion' =>$cedula , 'password' => $request['password']])) {
            Session::flash('message', 'Iniciado correctamente');
            return 1;
        } else {
            return 2;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        $rol=Auth::user()->rol_id;
        Auth::logout();


        if($rol==3){
            return redirect('/');
        }
        return redirect('login');

    }
}
