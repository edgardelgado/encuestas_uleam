<?php

namespace App\Http\Controllers;

use App\carreraModel;
use App\ciudadModel;
use App\facultadModel;
use App\usuarioModel;
use App\institucionModel;
use App\direccion_carreraModel;
use App\tituloModel;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class carreraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$carreras=carreraModel::join('facultad','facultad.id','=','carrera.facultad_id')
        //->get(['carrera.id','carrera.nombre','carrera.titulo','facultad.nombre as facultad']);
        // return View('admin.carreras.index',compact('carreras'));
        return View('admin.carreras.index');
    }

    public function carrera_ajax()
    {
        $request = request();
        $limit   = $request->input('length');
        $start   = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = carreraModel::with('titulo','facultad.ciudad')
            ->skip($start)->take($limit)->get();
            $totalData     = carreraModel::count();
            $totalFiltered = $totalData;
        } else {
            $search    = mb_strtoupper($request->input('search.value'));
            $registros = carreraModel::with('titulo','facultad','institucion')
            ->orWhereHas('facultad', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
            })->orWhereHas('titulo', function ($query) use ($search) {
                $query->where("titulo", 'LIKE', '%' . $search . '%');
            })->orWhereHas('institucion', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
            })->skip($start)->take($limit)->get();


            $totalData     = carreraModel::with('titulo','facultad.ciudad')
            ->orWhereHas('facultad.ciudad', function ($query) use ($search) {
                $query->where("facultad.nombre", 'LIKE', '%' . $search . '%')
                ->orwhere("ciudad.nombre", 'LIKE', '%' . $search . '%');
            })->orWhereHas('titulo', function ($query) use ($search) {
                $query->where("titulo", 'LIKE', '%' . $search . '%');
            })->orWhereHas('institucion', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
            })->skip($start)->take($limit)->count();
            $totalFiltered = $totalData;
        }
        $data = array();

        if (!empty($registros)) {


            foreach ($registros as $key => $value) {

             //   dd($value);

                $acciones = "<a href=" . route('carreras.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar Carrera'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Carrera'><i class='fa fa-trash-o text-danger'></i></a>";

                $usuario_data['id']     = ' <span>' . $value->id . '</span>';
                
                $usuario_data['titulo'] = '<span>' . $value->titulo->titulo . '</span>';
                $usuario_data['institucion'] = '';
                $usuario_data['facultad']   = '';

                  if($value->institucion){
                  $usuario_data['institucion']   = '<span>' . $value->institucion->nombre . '</span>';  
                }


                if($value->facultad){
                    $ciudad='';
                    if($value->facultad->ciudad){
                        $ciudad=$value->facultad->ciudad->nombre;
                    }
                  $usuario_data['facultad']   = '<span>' . $value->facultad->nombre .' - '.$ciudad . '</span>';  
                }
                $usuario_data['accion'] = $acciones;
                $data[]                 = $usuario_data;

                // dd($usuario_data);
            }
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        ];
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titulos=tituloModel::pluck('titulo','id');
        $instituciones=institucionModel::pluck('nombre','id');
         $encargados=usuarioModel::where('rol_id',2)
        ->select(DB::raw("CONCAT(nombres,' - ', apellidos,' - ', identificacion) as value"),'id')
        ->pluck('value','id');
        return View('admin.carreras.crear',compact('titulos','instituciones','encargados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo_id'      => 'required',
            'institucion_id'      => 'required',
        ]);
        $nueva_carrera              = new carreraModel();
        $nueva_carrera->institucion_id       = $request->institucion_id;
        $nueva_carrera->titulo_id      = $request->titulo_id;
        $nueva_carrera->facultad_id = $request->facultad_id;
        $nueva_carrera->save();
        if(count($request->encargados)){
            foreach ($request->encargados as $key => $value) {
                    $dir_car=new direccion_carreraModel();
                    $dir_car->carrera_id=$nueva_carrera->id;
                    $dir_car->usuario_id=$value;
                    $dir_car->save();
            }
        }
        Session::flash('message', 'Se ha ingresado una carrera a la plataforma');
        return redirect()->route('carreras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function show(carreraModel $carreraModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carrera  = carreraModel::find($id);
        $facultad = facultadModel::join('ciudad', 'ciudad.id', '=', 'facultad.ciudad_id')
            ->where('facultad.id', $carrera->facultad_id)
            ->select(DB::raw("CONCAT(facultad.nombre,' - ', ciudad.nombre) as value"), 'facultad.id')
            ->first();
        $titulos=tituloModel::pluck('titulo','id');
        $encargados=usuarioModel::where('rol_id',2)
        ->select(DB::raw("CONCAT(nombres,' - ', apellidos,' - ', identificacion) as value"),'id')
        ->pluck('value','id');

        $encargados_selected=direccion_carreraModel::join('carrera','carrera.id','direccion_carrera.carrera_id')
        ->where('direccion_carrera.carrera_id',$id)
        ->select('usuario_id as id')
        ->pluck('id');

        $instituciones=institucionModel::pluck('nombre','id');
        return View('admin.carreras.editar', compact('carrera', 'facultad','titulos','instituciones','encargados','encargados_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo_id'      => 'required',
            'institucion_id'      => 'required',
        ]);
        $nueva_carrera              = carreraModel::find($id);
        $nueva_carrera->institucion_id       = $request->institucion_id;
        $nueva_carrera->titulo_id      = $request->titulo_id;
        $nueva_carrera->facultad_id = $request->facultad_id;
        $nueva_carrera->save();
        direccion_carreraModel::where('carrera_id',$id)->delete();
        if(count($request->encargados)){
            foreach ($request->encargados as $key => $value) {
                    $dir_car=new direccion_carreraModel();
                   $dir_car->carrera_id=$nueva_carrera->id;
                    $dir_car->usuario_id=$value;
                    $dir_car->save();
            }
        }
        Session::flash('message', 'Se ha actualizado correctamente la carrera');
        return redirect()->route('carreras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        carreraModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente la carrera');
        return 1;
    }

    public function subir_carreras()
    {
        return view('admin.carreras.subida_masiva');
    }

    public function subir_profesionales()
    {
        return view('admin.profesionales.subida_masiva');
    }

    public function subir_empresas()
    {
        return view('admin.empresas.subida_masiva');
    }

    public function subir_carreras_post(Request $request)
    {
        ini_set('max_execution_time', 3000);
        $this->validate($request, [
            'carreras' => 'required',
        ]);

        if ($request->hasFile('carreras')) {
            $path     = $request->file('carreras')->getRealPath();
            $carreras = \Excel::load($path)->toArray();
            if (count($carreras)) {
                DB::beginTransaction();
                try {
                    foreach ($carreras as $key => $value) {
                        //obtengo datos de ciudad
                        $ciudad = ciudadModel::where('nombre', $value['ciudad'])->first();
                        if (is_null($ciudad)) {
                            $ciudad               = new ciudadModel();
                            $ciudad->nombre       = $value['ciudad'];
                            $ciudad->provincia_id = 1;
                            $ciudad->save();
                        }
                        //obtengo datos de facultad
                        $facultad = facultadModel::where('ciudad_id', $ciudad->id)->where('nombre', $value['facultad'])->first();
                        if (is_null($facultad)) {
                            $facultad            = new facultadModel();
                            $facultad->nombre    = $value['facultad'];
                            $facultad->ciudad_id = $ciudad->id;
                            $facultad->save();
                        }
                        $identificacion = (string) $value['cedula'];

                        $responsable = usuarioModel::where('identificacion', $identificacion)->first();

                        if (is_null($responsable)) {
                            $responsable                 = new usuarioModel();
                            $responsable->identificacion = $identificacion;
                            $responsable->apellidos      = $value['apellidos_responsable'];
                            $responsable->nombres        = $value['nombres_responsable'];
                            $responsable->email          = $value['correo_responsable'];
                            $responsable->rol_id         = 2;
                            $responsable->password       = bcrypt($identificacion);
                            $responsable->estadox        = 1;
                            $responsable->estado         = 'ACT';
                            $responsable->save();
                        }

                        //obtener carrera
                        $carrera = carreraModel::where('nombre', $value['carrera'])
                            ->where('facultad_id', $facultad->id)
                            ->first();
                        if (is_null($carrera)) {
                            $carrera = new carreraModel();
                        }
                        $carrera->nombre         = preg_replace("/[\r\n|\n|\r]+/", " ", $value['carrera']);
                        $carrera->facultad_id    = $facultad->id;
                        $carrera->titulo         = $value['titulo'];
                        $carrera->responsable_id = $responsable->id;
                        $carrera->save();
                    }
                    DB::commit();
                    Session::flash('message', 'Carrera ingresadas de manera masiva correctamente');
                    return Redirect('carreras');
                } catch (\Illuminate\Database\QueryException $e) {
                    DB::rollBack();
                    Session::flash('message-error', 'Hubo un error al registrarse ->' . $e);
                    return Redirect::back()->withInput(Input::all());
                }

            } else {
                Session::flash('message-error', 'Noo se ha recibido ninguno archivo');
                return Redirect::back();
            }
        }
        Session::flash('message-error', 'Noo se ha recibido ninguno archivo');
        return Redirect::back();

    }

}
