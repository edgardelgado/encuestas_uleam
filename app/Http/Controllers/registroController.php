<?php

namespace App\Http\Controllers;
use App\respuestaModel;
use App\respuesta_encuestaModel;
use App\direccion_carreraModel;
use App\carreraModel;
use App\encuestaModel;
use App\grupopreguntasModel;
use App\facultadModel;
use App\tituloModel;
use App\estudioModel;
use App\ciudadModel;
use App\opcionModel;
use App\preguntaModel;
use App\rolModel;
use App\usuarioModel;


use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Hash;

class registroController extends Controller
{

public function registro()
        {
  //          $ciudad    = ciudadModel::pluck('nombre', 'id');
//
           // $carreras    = carreraModel::pluck('nombre', 'id');

        $carreras = carreraModel::join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->join('institucion','institucion.id','carrera.institucion_id')
       // ->whereraw($where_carrera_id)
        ->select(DB::raw("CONCAT(institucion.nombre,' - ',ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->pluck('descripcion', 'id');
         $periodos=usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->orderBy('periodo_academico')->pluck('periodo_academico','periodo_academico');
         $anios=usuarioModel::whereNotNull('anio_graduacion')->distinct('anio_graduacion')
         ->orderBy('anio_graduacion')->pluck('anio_graduacion','anio_graduacion');

            
            return view('layouts.registro', compact('carreras','periodos','anios'));
        }


    public function registro_publico(Request $request)
    {
       
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'identificacion' => 'required|unique:usuario',
            'email'          => 'required|unique:usuario',
            'carrera_id'     => 'required',
            'carrera_id'     => 'required',
            'anio_graduacion'     => 'required',
            'fecha_graduacion'     => 'required',

        ]);

        $nuevo_usuario                 = new usuarioModel();
        $nuevo_usuario->apellidos      = strtoupper($request->apellidos);
        $nuevo_usuario->nombres        = strtoupper($request->nombres);
        $nuevo_usuario->identificacion = $request->identificacion;
        $nuevo_usuario->email          = $request->email;
        $nuevo_usuario->telefono       = $request->telefono;
        $nuevo_usuario->periodo_academico       = $request->periodo_academico;
        $nuevo_usuario->rol_id         = 3;
        $nuevo_usuario->estado         = 'ACT';
        $nuevo_usuario->carrera_id     = $request->carrera_id;
        $nuevo_usuario->password       = bcrypt($request->identificacion);
        $nuevo_usuario->anio_graduacion     = $request->anio_graduacion;
        $nuevo_usuario->fecha_graduacion     = $request->fecha_graduacion;
        $nuevo_usuario->save();


        Session::flash('message','Se ha registrado exitosamente al sistema de encuestamientos a los graduados de la Uleam');
        if (Auth::attempt(['identificacion' => $nuevo_usuario->identificacion, 'password' => $nuevo_usuario->identificacion])) {
            return Redirect::to('/inicio');
        }    
    }

   


   
}
