<?php

namespace App\Http\Controllers;

use App\areaModel;
use App\usuarioModel;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class areaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $areas = areaModel::all();
        return view('admin.areas.index', compact('areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.areas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|unique:area',
        ]);
        $area              = new areaModel();
        $area->nombre      = strtoupper($request->nombre);
        $area->ruta        = '/' . sanear_string($area->nombre);
        $area->descripcion = $request->descripcion;
        $area->save();
        guardar_accion(2, null);
        Session::flash('message', 'Se ha ingresado una nueva area al Sistema');
        return Redirect::to('configuracion/areas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = areaModel::find($id);
        return view('admin.areas.edit', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|unique:area,nombre,' . $id,
        ]);
        $area              = areaModel::find($id);
        $area->nombre      = strtoupper($request->nombre);
        $area->ruta        = '/' . sanear_string($area->nombre);
        $area->descripcion = $request->descripcion;
        $area->save();
        guardar_accion(3, null);
        Session::flash('message', 'Se ha actualizado correctamente el area del Sistema');
        return Redirect::to('configuracion/areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = usuarioModel::where('area_id', $id)->count();
        if ($usuario > 0) {
            return 0;
        } else {
            areaModel::find($id)->delete();
            guardar_accion(4, null);
            return 1;
        }
    }

    public function subida_masiva($id)
    {
        return view('admin.areas.subida_masiva');
    }

    public function guadar_subida_masiva(Request $request)
    {
        ini_set('max_execution_time', 3000);
        $this->validate($request, [
            'areas' => 'required',
        ]);

        if ($request->hasFile('areas')) {
            $path  = $request->file('areas')->getRealPath();
            $areas = \Excel::load($path)->toArray();
            if (count($areas)) {
                DB::beginTransaction();
                try {
                    foreach ($areas as $key => $value) {
                        $nombre_area = strtoupper($value['nombre']);
                        $area_n      = areaModel::where('nombre', $nombre_area)->first();
                        if (is_null($area_n)) {
                            $area_n = new areaModel();
                        }
                        $area_n->descripcion = $value['tipo'];
                        $area_n->nombre      = $nombre_area;
                        $area_n->ruta        = '/' . sanear_string($area_n->nombre);
                        $area_n->save();

                        if (!is_null($value['correo_electronico'])) {
                            $usuario = usuarioModel::where('email', $value['correo_electronico'])->first();
                            if (is_null($usuario)) {
                                $usuario = new usuarioModel();
                            }
                            $usuario->apellidos = $value['responsable_apellidos'];
                            $usuario->nombres   = $value['responsable_nombres'];
                            $usuario->password  = bcrypt('12345678');
                            $usuario->area_id   = $area_n->id;
                            $usuario->rol_id    = 2;
                            $usuario->email     = $value['correo_electronico'];
                            $username           = explode('@', $usuario->email);
                            $usuario->usuario   = $username[0];
                            $usuario->estado    = 'ACT';
                            $usuario->save();
                        }

                    }
                    DB::commit();
                    Session::flash('message', 'Areas ingresadas de manera masiva correctamente');
                    return Redirect('configuracion/areas');

                } catch (\Illuminate\Database\QueryException $e) {
                    DB::rollBack();
                    Session::flash('message-error', 'Hubo un error al registrarse ->' . $e->getMessage());
                    return Redirect::back();
                }
            }
        } else {
            Session::flash('message-error', 'Noo se ha recibido ninguno archivo');
            return Redirect::back();
        }

        dd($request->all());
    }

}
