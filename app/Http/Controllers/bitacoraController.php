<?php

namespace App\Http\Controllers;

use App\bitacoraModel;
use Illuminate\Http\Request;
use View;

class bitacoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function inicio()
    {
        return View('admin.visitas.inicio');
    }
    public function obtener_datos_bitacora(Request $request)
    {
        $desde = $request->fecha_afiliados_desde . ' 00:00:00';
        $hasta = $request->fecha_afiliados_hasta . ' 23:59:59';
        //$visitas=bitacoraModel::join('usuario','usuario.id','=','bitacora.usuario_id')
        //->join('accion','accion.id','=','bitacora.accion_id')
        //->whereBetween('bitacora.created_at',array($desde,$hasta))->orderBy('bitacora.created_at','DESC')
        //  ->get(['accion.nombre','usuario.nombres','bitacora.id','ip','pcname','device','bitacora.created_at']);
        // $visitas=bitacoraModel::all();
        //dd($visitas);
        $visitas = bitacoraModel::all();
        return View('admin.visitas.desnudas.index', compact('visitas'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bitacoraModel  $bitacoraModel
     * @return \Illuminate\Http\Response
     */
    public function show(bitacoraModel $bitacoraModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bitacoraModel  $bitacoraModel
     * @return \Illuminate\Http\Response
     */
    public function edit(bitacoraModel $bitacoraModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bitacoraModel  $bitacoraModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bitacoraModel $bitacoraModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bitacoraModel  $bitacoraModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(bitacoraModel $bitacoraModel)
    {
        //
    }
}
