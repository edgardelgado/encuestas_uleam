<?php

namespace App\Http\Controllers;

use App\carreraModel;
use App\ciudadModel;
use App\facultadModel;
use App\provinciaModel;
use Auth;
use DB;

class ubicacionController extends Controller
{
    public function consultar_provincias($nombre)
    {
        return provinciaModel::where('nombre', 'like', '%' . $nombre . '%')
            ->select('nombre as value', 'id')
            ->get();
    }

    public function consultar_ciudades($nombre)
    {
        return ciudadModel::join('provincia', 'provincia.id', '=', 'ciudad.provincia_id')
            ->where('ciudad.nombre', 'like', '%' . $nombre . '%')
            ->select(DB::raw("CONCAT(ciudad.nombre,' - ', provincia.nombre) as value"), 'ciudad.id')
            ->get();
    }

    public function consultar_facultades($id)
    {
        return facultadModel::Leftjoin('ciudad', 'ciudad.id', '=', 'facultad.ciudad_id')
        ->where('facultad.nombre', 'ilike', '%' . $id . '%')
            ->select(DB::raw("CONCAT(facultad.nombre,' - ', ciudad.nombre) as value"), 'facultad.id')
            ->get();
    }

    public function consultar_carreras($id)
    {
        $where_carrera = '0=0';
        if (Auth::Check()) {
            if (Auth::user()->rol_id == 2) {
                $where_carrera = 'carrera.responsable_id=' . Auth::id();
            }
        }
        return carreraModel::join('facultad', 'facultad.id', '=', 'carrera.facultad_id')
            ->join('ciudad', 'ciudad.id', '=', 'facultad.ciudad_id')
            ->where('carrera.nombre', 'like', '%' . $id . '%')
            ->whereraw($where_carrera)
            ->select(DB::raw("CONCAT(carrera.nombre,' - ', ciudad.nombre) as value"), 'carrera.id')
            ->get();
    }

}
