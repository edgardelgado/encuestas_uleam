<?php

namespace App\Http\Controllers;

use App\accionModel;
use Illuminate\Http\Request;

class accionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $accion = accionModel::all();
        return view('admin.accion.index', compact('accion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $accion = null;
        if (is_null($request->id)) {
            $accion = new accionModel();
        } else {
            $accion = accionModel::find($request->id);
        }
        $accion->nombre      = $request->nombre;
        $accion->descripcion = $request->descripcion;
        //$accion->orden=$request->orden;
        $accion->save();
        return $accion->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\accionModel  $accionModel
     * @return \Illuminate\Http\Response
     */
    public function show(accionModel $accionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\accionModel  $accionModel
     * @return \Illuminate\Http\Response
     */
    public function edit(accionModel $accionModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\accionModel  $accionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, accionModel $accionModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\accionModel  $accionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            accionModel::find($id)->delete();
            return 1;
        } catch (\Illuminate\Database\QueryException $e) {
            return 0;
        }
    }
}
