<?php

namespace App\Http\Controllers;

use App\ciudadModel;
use App\facultadModel;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class facultadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $facultades=facultadModel::join('ciudad','ciudad.id','=','facultad.ciudad_id')
        //->get(['facultad.id','facultad.nombre as facultad','ciudad.nombre as ciudad']);
        //return view('admin.facultades.index',compact('facultades'));
        return view('admin.facultades.index');
    }

    public function facultad_ajax()
    {
        $request = request();
        $columns = array();
        array_push($columns, "id", "nombre", "ciudad");
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir   = $request->input('order.0.dir');
        if (empty($request->input('search.value'))) {
            $facultades = facultadModel::with('ciudad')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalData     = facultadModel::count();
            $totalFiltered = $totalData;
        } else {
            $search     = $request->input('search.value');
            $search     = strtoupper($search);
            $facultades = facultadModel::with('ciudad')
                ->Where("facultad.nombre", 'LIKE', '%' . $search . '%')
                ->orWhereHas('ciudad', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
                })->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalData     = count($facultades);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($facultades)) {
            //   dd($facultades);

            foreach ($facultades as $campo) {
                $length   = strlen($campo->id);
                $acciones = "<a href=" . route('facultades.edit',$campo->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar facultad'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $campo->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar facultad'><i class='fa fa-trash-o text-danger'></i></a>";

                $usuario_data['id']       = $campo->id;
                $usuario_data['facultad'] = $campo->nombre;
                $usuario_data['ciudad']   = $campo->ciudad->nombre;
                $usuario_data['acciones'] = $acciones;

                $data[] = $usuario_data;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        );
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.facultades.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ciudad_id' => 'required',
            'nombre'    => 'required',
        ]);
        $facultad            = new facultadModel();
        $facultad->nombre    = mb_strtoupper($request->nombre);
        $facultad->ciudad_id = $request->ciudad_id;

        $facultad->save();

        Session::flash('message', 'Se ha registrado correctamente la facultad');
        return redirect()->route('facultades.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\facultadModel  $facultadModel
     * @return \Illuminate\Http\Response
     */
    public function show(facultadModel $facultadModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\facultadModel  $facultadModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facultad        = facultadModel::find($id);
        $ciudad_selected = ciudadModel::Leftjoin('provincia', 'provincia.id', '=', 'ciudad.provincia_id')
            ->where('ciudad.id', $facultad->ciudad_id)
            ->select(DB::raw("CONCAT(ciudad.nombre,' - ', provincia.nombre) as value"), 'ciudad.id')
            ->first();
        return view('admin.facultades.editar', compact('facultad', 'ciudad_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\facultadModel  $facultadModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ciudad_id' => 'required',
            'nombre'    => 'required',
        ]);
        $facultad            = facultadModel::find($id);
        $facultad->nombre    = mb_strtoupper($request->nombre);
        $facultad->ciudad_id = $request->ciudad_id;
        $facultad->save();
        Session::flash('message', 'Se ha actualizado correctamente la facultad');
        return redirect()->route('facultades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\facultadModel  $facultadModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        facultadModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente la facultad');
        return 1;
    }
}
