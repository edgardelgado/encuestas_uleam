<?php

namespace App\Http\Controllers;

use App\menuModel;
use Illuminate\Http\Request;

class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $menus = menuModel::where('proyecto',1)->get();

        return view('admin.menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $menu = null;
        if (is_null($request->id)) {
            $menu = new menuModel();
        } else {
            $menu = menuModel::find($request->id);
        }
        $menu->menu    = $request->menu;
        $menu->iconfaw = $request->icono;
        $menu->nivel   = $request->nivel;
        $menu->idmain  = intval($request->idmain);
        $menu->orden   = $request->orden;
        $menu->ruta    = $request->ruta;
        $menu->visible = $request->visible;
       $menu->proyecto = 1;

        $menu->save();
        return $menu->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\menuModel  $menuModel
     * @return \Illuminate\Http\Response
     */
    public function show(menuModel $menuModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\menuModel  $menuModel
     * @return \Illuminate\Http\Response
     */
    public function edit(menuModel $menuModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\menuModel  $menuModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, menuModel $menuModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\menuModel  $menuModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            menuModel::find($id)->delete();
            return 1;
        } catch (\Illuminate\Database\QueryException $e) {
            return 0;
        }
    }
}
