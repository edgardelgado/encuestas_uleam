<?php

namespace App\Http\Controllers;

use App\carreraModel;
use App\usuarioModel;
use App\estudioModel;
use App\respuesta_encuestaModel;
use App\respuestaModel;
use App\direccion_carreraModel;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use DB;

class profesionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.profesionales.index');

    }

    public function profesional_ajax()
    {
        $where_carrera_id = '0=0';
        if (Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) {
            $where_carrera_id = '0=1';
            $carreras=direccion_carreraModel::where('usuario_id',Auth::id())->get()->groupBy('carrera_id')->keys()->implode(',');
            if($carreras!=''){
                $carreras = trim($carreras, ",");
                $where_carrera_id="usuario.carrera_id IN ($carreras)";
            }
        }

        $request = request();
        $limit   = $request->input('length');
        $start   = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = usuarioModel::whereraw($where_carrera_id)
            ->where('rol_id',3)
            ->with('carrera.titulo')
            ->orderBy('apellidos')
            ->skip($start)->take($limit)->get();
            $totalData     = usuarioModel::whereraw($where_carrera_id)
            ->where('rol_id',3)->count();
            $totalFiltered = $totalData;
        } else {
            $search    = mb_strtoupper($request->input('search.value'));
            $registros = usuarioModel::where(function ($query) use ($search) {
                $query->where("nombres", 'ilike', '%' . $search . '%')
                    ->orWhere("apellidos", 'ilike', '%' . $search . '%')
                    ->orWhere("identificacion", 'ilike', '%' . $search . '%')
                    ->orWhere("email", 'ilike', '%' . $search . '%')
                    ->orWhere("telefono", 'ilike', '%' . $search . '%')
                    ->orWhere("periodo_academico", 'ilike', '%' . $search . '%')
                    ->orWhereHas('carrera.titulo', function ($queryx) use ($search) {
                        $queryx->where("titulo.titulo", 'ilike', '%' . $search . '%');
                    });
            })->where('rol_id',3)
            ->whereraw($where_carrera_id)
            ->orderBy('apellidos')
            ->with('carrera.titulo')
            ->skip($start)->take($limit)->get();
            $totalData     = usuarioModel::where(function ($query) use ($search) {
                $query->where("nombres", 'ilike', '%' . $search . '%')
                    ->orWhere("apellidos", 'ilike', '%' . $search . '%')
                    ->orWhere("identificacion", 'ilike', '%' . $search . '%')
                    ->orWhere("email", 'ilike', '%' . $search . '%')
                    ->orWhere("telefono", 'ilike', '%' . $search . '%')
                    ->orWhere("periodo_academico", 'ilike', '%' . $search . '%')
                    ->orWhereHas('carrera.titulo', function ($queryx) use ($search) {
                        $queryx->where("titulo.titulo", 'ilike', '%' . $search . '%');
                    });
            })->where('rol_id',3)
            ->whereraw($where_carrera_id)
            ->with('carrera.titulo')
            ->count();
            $totalFiltered = $totalData;
        }
        $data = array();

        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                $length   = strlen($value->id);
                $acciones = "<a href=" . route('profesionales.edit',encrypt($value->id)) . " class='btn btn-lg btn-default' data-toggle='tooltip' data-placement='top' title='Editar profesional'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-lg btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Profesional'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']             = ' <span>' . $value->id . '</span>';
                $usuario_data['identificacion'] = ' <span>' . $value->identificacion . '</span>';
                $usuario_data['nombres']        = '<span>' . $value->nombres . '</span>';
                $usuario_data['apellidos']      = '<span>' . $value->apellidos . '</span>';
                $usuario_data['email']          = '<span>' . $value->email . '</span>';
                $usuario_data['telefono']       = '<span>' . $value->telefono . '</span>';
                $usuario_data['periodo_academico']       = '<span>' . $value->periodo_academico . '</span>';
                $usuario_data['anio_graduacion']       = '<span>' . $value->anio_graduacion . '</span>';
                $carrera=null;
                if($value->carrera){
                    $carrera=$value->carrera->titulo->titulo;
                }
                $usuario_data['carrera']       = '<span>' . $carrera . '</span>';
                $usuario_data['acciones'] = $acciones;
                $data[]= $usuario_data;
            }
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        ];
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $where_carrera_id = '0=0';
        if (Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) {
            $where_carrera_id = '0=1';
            $carreras=direccion_carreraModel::where('usuario_id',Auth::id())->get()->groupBy('carrera_id')->keys()->implode(',');
            if($carreras!=''){
                $carreras = trim($carreras, ",");
                $where_carrera_id="carrera.id IN ($carreras)";
            }
        }

        $carreras = carreraModel::join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->join('institucion','institucion.id','carrera.institucion_id')
        ->whereraw($where_carrera_id)
        ->select(DB::raw("CONCAT(institucion.nombre,' - ',ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->pluck('descripcion', 'id');
         $periodos=usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
         $anios=usuarioModel::whereNotNull('anio_graduacion')->distinct('anio_graduacion')->pluck('anio_graduacion','anio_graduacion');
        return view('admin.profesionales.crear', compact('carreras','periodos','anios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'identificacion' => 'required|unique:usuario',
            'email'          => 'required|unique:usuario',
            'carrera_id'     => 'required',
            'carrera_id'     => 'required',
            'anio_graduacion'     => 'required',
            'fecha_graduacion'     => 'required',

        ]);

        $nuevo_usuario                 = new usuarioModel();
        $nuevo_usuario->apellidos      = strtoupper($request->apellidos);
        $nuevo_usuario->nombres        = strtoupper($request->nombres);
        $nuevo_usuario->identificacion = $request->identificacion;
        $nuevo_usuario->email          = $request->email;
        $nuevo_usuario->telefono       = $request->telefono;
        $nuevo_usuario->periodo_academico       = $request->periodo_academico;
        $nuevo_usuario->rol_id         = 3;
        $nuevo_usuario->estado         = 'ACT';
        $nuevo_usuario->carrera_id     = $request->carrera_id;
        $nuevo_usuario->password       = bcrypt($request->identificacion);
        $nuevo_usuario->anio_graduacion     = $request->anio_graduacion;
        $nuevo_usuario->fecha_graduacion     = $request->fecha_graduacion;
        $nuevo_usuario->save();
        Session::flash('message', 'Se ha ingresado un nuevo profesional');
        return redirect()->route('profesionales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $usuario = usuarioModel::find(decrypt($id));
         $where_carrera_id = '0=0';
        if (Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) {
            $where_carrera_id = '0=1';
            $carreras=direccion_carreraModel::where('usuario_id',Auth::id())->get()->groupBy('carrera_id')->keys()->implode(',');
            if($carreras!=''){
                $carreras = trim($carreras, ",");
                $where_carrera_id="carrera.id IN ($carreras)";
            }
        }

        $carreras = carreraModel::join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->join('institucion','institucion.id','carrera.institucion_id')
        ->whereraw($where_carrera_id)
        ->select(DB::raw("CONCAT(institucion.nombre,' - ',ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->pluck('descripcion', 'id');
         $periodos=usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
          $anios=usuarioModel::whereNotNull('anio_graduacion')->distinct('anio_graduacion')->pluck('anio_graduacion','anio_graduacion');
        return view('admin.profesionales.editar', compact('usuario','carreras','periodos','anios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'identificacion' => 'required|unique:usuario,identificacion,'.$id,
            'email'          => 'required|email|unique:usuario,email,'.$id,
            'carrera_id'     => 'required',
            'anio_graduacion'     => 'required',
            'fecha_graduacion'     => 'required',
        ]);



        $nuevo_usuario                 = usuarioModel::find($id);
        $nuevo_usuario->apellidos      = strtoupper($request->apellidos);
        $nuevo_usuario->nombres        = strtoupper($request->nombres);
        $nuevo_usuario->identificacion = $request->identificacion;
        $nuevo_usuario->periodo_academico       = $request->periodo_academico;
        $nuevo_usuario->email          = $request->email;
        $nuevo_usuario->telefono       = $request->telefono;
        $nuevo_usuario->rol_id         = 3;
        $nuevo_usuario->estado         = 'ACT';
        $nuevo_usuario->estadox        = 1;
        $nuevo_usuario->carrera_id     = $request->carrera_id;
        $nuevo_usuario->anio_graduacion     = $request->anio_graduacion;
        $nuevo_usuario->fecha_graduacion     = $request->fecha_graduacion;
        $nuevo_usuario->password       = bcrypt($request->identificacion);
        $nuevo_usuario->save();
        Session::flash('message', 'Se ha actualizado exitosamente al profesional');
        return redirect()->route('profesionales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $usuario=usuarioModel::find($id);
            if(!is_null($usuario)){
                $rp_encuesta=respuesta_encuestaModel::where('usuario_id',$id)->get();
                if(count($rp_encuesta)){
                    foreach ($rp_encuesta as $rpe) {
                        respuestaModel::where('respuesta_encuesta_id',$rpe->id)->delete();
                        $rpe->delete();
                    }
                }
                estudioModel::where('usuario_id',$id)->delete();
                $usuario->delete();
                Session::flash('message', 'Se ha eliminado correctamente el usuario');
                return 1;
            }
        } catch (\Illuminate\Database\QueryException $e) {
            dd($e->getMessage());
        Session::flash('message-error', 'No se ha podido eliminar el profesional');
       // return -1; 
        }
        
    }
}
