<?php

namespace App\Http\Controllers;

use App\carreraModel;
use App\respuesta_encuestaModel;
use App\usuarioModel;
use App\direccion_carreraModel;
use Auth;
use DB;
use File;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Session;

class backendController extends Controller
{
    // aqui se pone que las funciones que se ejecuten en este controlador solo sean para usarios que esten logueados
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function actualizarclave()
    {
        return view('cuenta.cambiar_clave');
    }
    public function actualizardatos()
    {
        $usuario = Auth::user();
        return view('cuenta.actualizar_datos', compact('usuario'));
    }

    public function actualizar_clave_post(Request $request)
    {
        $this->validate($request, [
            'password'  => 'required',
            'password1' => 'required',
            'password2' => 'required|same:password1',
        ], [
            'password2.same' => 'Las contraseñas no coinciden',
        ]);

        if (Hash::check($request->password, Auth::user()->password)) {
            $usuario           = usuarioModel::find(Auth::id());
            $usuario->password = bcrypt($request->password1);
            $usuario->save();
            Session::flash('message', 'Ha Actualizado su Contraseña correctamente');
            return Redirect::to('/');
        } else {
            Session::flash('message-error', 'Las Contraseña actual no es la correcta');
            return Redirect::back();
        }
    }

    public function actualizar_datos_post(Request $request)
    {

        $this->validate($request, [
            'nombres'   => 'required',
            'apellidos' => 'required',
            'email'     => 'required|email|unique:usuario,id,' . Auth::id(),
        ], [
            'nombres.required'   => 'Los nombres son requeridos',
            'apellidos.required' => 'Los apellidos son requeridos',
            'email.required'     => 'EL correo electrónico es requerido',
            'email.mail'         => 'EL formato del correo electrónico no es el correcto',
            'email.unique'       => 'El correo electrónico ingresado ya está registrado en la plataforma.',
        ]);
        DB::beginTransaction();
        try {
            $usuario            = usuarioModel::find(Auth::id());
            $usuario->nombres   = $request->nombres;
            $usuario->apellidos = $request->apellidos;
            $usuario->email     = $request->email;
            $usuario->telefono  = $request->telefono;

            if (Input::hasFile('foto')) {
                $dir = public_path() . '/images/usuarios/';
                if (!is_null($usuario->foto)) {
                    File::delete($dir . $usuario->foto);
                    $usuario->foto = null;
                }
                $foto     = $request->file('foto');
                $fileName = $usuario->id . '.' . $foto->getClientOriginalExtension();
                $img      = \Image::make($foto)->resize(80, 80);
                $img->save($dir . $fileName);
                $usuario->foto = $fileName;
                $usuario->save();
            }

            if (Input::hasFile('firma')) {
                $dir = public_path() . '/images/usuarios/';
                if (!is_null($usuario->firma)) {
                    File::delete($dir . $usuario->firma);
                    $usuario->foto = null;
                }
                $firma    = $request->file('firma');
                $fileName = 'firma' . $usuario->id . '.' . $firma->getClientOriginalExtension();
                $img      = \Image::make($firma)->resize(80, 80);
                $img->save($dir . $fileName);
                $usuario->firma = $fileName;
                $usuario->save();
            }

            $usuario->save();

            DB::commit();
            //aqui debe enviar un correo de confirmacion
            Session::flash('message', 'Se ha actualizado correctamente los datos');
            return Redirect::to('/');
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            Session::flash('message-error', 'Hubo un error al actualizar datos ->' . $e);
            return Redirect::back()->withInput(Input::all());
        }
    }

    public function inicio()
    {
        //SI ES PROFESIONAL
        if (Auth::user()->rol_id == 3) {
            return view('profesional.inicio');
        }

        //CASO QUE NO SEA PROFESIONAL
        $where_carrera_id = '0=0';
        $where_carrera_idx = '0=0';
        if (Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) {
            $where_carrera_id = '0=1';
            $where_carrera_idx = '0=1';
            $carreras=direccion_carreraModel::where('usuario_id',Auth::id())->get()->groupBy('carrera_id')->keys()->implode(',');
            if($carreras!=''){
                $carreras = trim($carreras, ",");
                $where_carrera_id="usuario.carrera_id IN ($carreras)";
                $where_carrera_idx="id IN ($carreras)";
            }
        }

        //DATOS CUANTITATIVOS
        $total_profesionales  = usuarioModel::join('carrera','carrera.id','usuario.carrera_id')
        ->where('carrera.institucion_id',1)
        ->whereraw($where_carrera_id)
        ->where('rol_id', 3)->count();
        $total_carreras       = carreraModel::whereraw($where_carrera_idx)->where('institucion_id',1)->count();
        $encuestas_respondida = respuesta_encuestaModel::join('usuario','usuario.id','respuesta_encuesta.usuario_id')
        ->whereraw($where_carrera_id)
        ->count();

        return view('admin.inicio', compact('total_profesionales', 'total_carreras', 'encuestas_respondida'));

    }

    public function envios_pendientes()
    {
        $subidas = subidaModel::join('estado_documento', 'estado_documento.id', '=', 'subida.estado_documento_id')
            ->where('area_id', Auth::user()->area_id)->where('estado_documento_id', 2)->get(['subida.*', 'estado_documento.clase', 'estado_documento.color', 'estado_documento.nombre as estado']);
        return view('funcionario.envios_pendientes', compact('subidas'));
    }

    public function plazos_conservacion()
    {
        if (Auth::user()->rol_id == 1) {
            //si es administrador
        } elseif (Auth::user()->rol_id == 2) {
            //si es de alguna area
            $tipos = tipodocumentoModel::all(['id']);
            foreach ($tipos as $tipo) {
                $plazo = plazo_conservacionModel::where('area_id', Auth::user()->area_id)->where('tipo_documento_id', $tipo->id)->first();
                if (is_null($plazo)) {
                    plazo_conservacionModel::create(['area_id' => Auth::user()->area_id, 'tipo_documento_id' => $tipo->id, 'anios' => 5]);
                }
            }

            $plazos = plazo_conservacionModel::join('tipo_documento', 'tipo_documento.id', '=', 'plazo_conservacion.tipo_documento_id')
                ->where('plazo_conservacion.area_id', Auth::user()->area_id)
                ->get(['tipo_documento.nombre', 'plazo_conservacion.anios', 'plazo_conservacion.id', 'plazo_conservacion.editado']);
            return view('funcionario.plazos_conservacion', compact('plazos'));
        }
    }

    public function actas_entregas()
    {
        $actas = subidaModel::where('area_id', Auth::user()->area_id)->where('estado_documento_id', 4)
            ->get(['codigo', 'detalle', 'created_at', 'aceptado', 'id']);
        return view('funcionario.actas_entregas', compact('actas'));

    }

    public function plazos_conservacion_post(Request $request)
    {
        plazo_conservacionModel::where('id', $request->id)->update(['anios' => $request->anios, 'editado' => 1]);
        return $request->id;
    }

    public function pendientes_recepcion()
    {
        $subidas = subidaModel::join('estado_documento', 'estado_documento.id', '=', 'subida.estado_documento_id')
            ->where('estado_documento_id', 3)->get(['subida.*', 'estado_documento.clase', 'estado_documento.color', 'estado_documento.nombre as estado']);
        return view('archivador.pendientes_recepcion', compact('subidas'));
    }

    public function pendientes_recepcion_detalle($id)
    {
        $documentos = documentoModel::join('subida', 'subida.id', '=', 'documento.subida_id')
            ->join('tipo_documento', 'tipo_documento.id', '=', 'documento.tipo_documento_id')
            ->where('subida.codigo', $id)
            ->select('documento.*', 'tipo_documento.nombre')
            ->get();
        $subida = subidaModel::where('codigo', $id)->first();
        return view('archivador.pendientes_recepcion_detalle', compact('documentos', 'subida'));
    }

    public function envios_pendientes_detalle($id)
    {
        $subida     = subidaModel::where('codigo', $id)->first();
        $documentos = documentoModel::join('subida', 'subida.id', '=', 'documento.subida_id')
            ->join('tipo_documento', 'tipo_documento.id', '=', 'documento.tipo_documento_id')
            ->where('subida.id', $subida->id)
            ->select('documento.*', 'tipo_documento.nombre as tipo')
            ->get();
        // dd($documentos);
        return view('funcionario.envios_pendientes_detalle', compact('documentos', 'subida'));
    }

    public function pendientes_archivar()
    {
        $documentos = documentoModel::join('tipo_documento', 'tipo_documento.id', '=', 'documento.tipo_documento_id')
            ->join('estado_documento', 'estado_documento.id', '=', 'documento.estado_documento_id')
            ->join('persona as remitente', 'remitente.id', '=', 'documento.remitente_id')
            ->join('persona as destinatario', 'destinatario.id', '=', 'documento.destinatario_id')
            ->where('documento.estado_documento_id', 4)
            ->orderBy('documento.subida_id')
            ->get(['tipo_documento.nombre as tipo', 'documento.fecha_emision', 'documento.asunto', 'documento.tipo_envio', 'documento.folder_id', 'documento.expediente_id', 'documento.hojas', 'documento.tipo_fisico', 'remitente.nombres as remitente', 'destinatario.nombres as destinatario', 'documento.ruta', 'documento.numero', 'documento.archivo', 'documento.id', 'documento.area_id', 'documento.tipo_documento_id', 'estado_documento.nombre as estado', 'estado_documento.color as color', 'documento.subida_id as numero_subida', 'tipo_documento.nombre as tipo']);
        $perchas = perchaModel::pluck('numero', 'id');
        return view('archivador.pendientes_archivar', compact('documentos', 'perchas'));
    }

    public function actualizar_plazos(Request $request)
    {
        $tipos   = tipodocumentoModel::all(['id']);
        $area_id = Auth::user()->area_id;
        foreach ($tipos as $tip) {
            $tipx = 'tipo' . $tip->id;
            if (!is_null($request[$tipx])) {
                $plazo = plazo_conservacionModel::where('area_id', $area_id)->where('tipo_documento_id', $tip->id)->first();
                if (is_null($plazo)) {
                    $plazo                    = new plazo_conservacionModel();
                    $plazo->area_id           = $area_id;
                    $plazo->tipo_documento_id = $tip->id;
                    $plazo->save();
                }
                $plazo->anios   = $request[$tipx];
                $plazo->editado = 1;
                $plazo->save();
            }
        }
        $archivo     = $request->file('archivo_comision');
        $dir_actual  = '/configuracion/plazos-conservacion/';
        $filename    = str_random(10) . $area_id . str_random(10) . '.' . $archivo->getClientOriginalExtension();
        $storagePath = Storage::putFileAs(
            $dir_actual, $archivo, $filename
        );
        $area                   = areaModel::find($area_id);
        $area->archivo_comision = $filename;
        $area->save();
        Session::flash('message', 'Se ha configurado correctamente los plazos de conservación');
        return Redirect::to('/');

    }

}
