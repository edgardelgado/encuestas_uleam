<?php

namespace App\Http\Controllers;

use App\provinciaModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;

class provinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.provincias.index');
    }



         public function provincias_ajax()
    {
        $request=request();
        $limit = $request->input('length');
        $start = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = provinciaModel::skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }else {
            $search = $request->input('search.value');
            $registros = provinciaModel::where(function($query) use ($search) {
                $query->where("nombre" , 'LIKE' , '%'.$search.'%')
                ->orWhere("descripcion", 'LIKE', '%' . $search . '%');                 
            })
            ->skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                

                $acciones = "<a href=" . route('provincias.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar Provincia'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Provincia'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']       = '<span>'.$value->id.'</span>'; 
                $usuario_data['nombre'] = '<span>'.$value->nombre.'</span>';  

                 $usuario_data['descripcion'] = '<span>'.$value->descripcion.'</span>';             
                $usuario_data['acciones']=$acciones;
                $data[] = $usuario_data;
            }
        }
        $json_data = [
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ];
        return json_encode($json_data);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
         return view('admin.provincias.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            //'nombre' => 'required|unique:tipo_pagos',
        ]);
        $provincia            = new provinciaModel();
        $provincia->nombre = $request->nombre;
        $provincia->descripcion         = $request->descripcion;
        $provincia->save();
        Session::flash('message', 'Se ha ingresado un nueva Provincia al Sistema');
        return redirect()->route('provincias.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\provinciaModel  $provinciaModel
     * @return \Illuminate\Http\Response
     */
    public function show(provinciaModel $provinciaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\provinciaModel  $provinciaModel
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $provincia  = provinciaModel::find($id);
        
        return View('admin.provincias.edit', compact('provincia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\provinciaModel  $provinciaModel
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
     {
        $this->validate($request, [
          //  'nombre' => 'required|unique:rol,nombre,' . $id,
        ]);
        $provincia              = provinciaModel::find($id);
        $provincia->nombre = $request->nombre;
        $provincia->descripcion         = $request->descripcion;
        $provincia->save();
        Session::flash('message', 'Se ha actualizado la Provincia al Sistema');
        return redirect()->route('provincias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\provinciaModel  $provinciaModel
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        provinciaModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente la Provincia');
        return 1;
    }
}
