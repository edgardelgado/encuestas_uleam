<?php

namespace App\Http\Controllers;

use App\encuestaModel;
use App\usuarioModel;
use App\direccion_carreraModel;
use App\carreraModel;
use App\opcionModel;
use App\preguntaModel;
use App\respuestaModel;
use App\respuesta_encuestaModel;
use App\grupopreguntasModel;
use Auth;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Barryvdh\DomPDF\Facade as PDF;

class encuestaController extends Controller
{
    public function vista_resultados()
    {
        $encuestas = encuestaModel::all();
        return view('resultados.index', compact('encuestas'));
    }
    public function index()
    {
        $encuestas = encuestaModel::all();
        return view('admin.encuestas.index', compact('encuestas'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

        function consultar_resultados_encuestas_cantidad(){
        $request=request();
        $where_carrera='0=0';
        $where_periodo="'0'='0'";
        $where_anios='0=0';

        
        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            $where_carrera='0=1';
        }
        

        if($request->carreras){
            $dat='';
            foreach ($request->carreras as $key => $value) {
                $dat.=$value.',';
            }
            $dat = trim($dat, ",");
            $where_carrera="usuario.carrera_id IN ($dat)";
        }

         if($request->periodos){
            $datx='';
            foreach ($request->periodos as $key => $periodo) {
                $datx.="'$periodo',";
            }
            $datx = trim($datx, ",");
            $where_periodo="usuario.periodo_academico IN ($datx)";
        }

         if($request->anios){
            $anios='';
            foreach ($request->anios as $key => $an) {
                $anios.=intval($an).',';
            }
            $anios = trim($anios, ",");
            $where_anios="usuario.anio_graduacion IN ($anios)";
        }

        $total_profesionales=usuarioModel::whereraw($where_carrera)
        ->whereraw($where_anios)
        ->whereraw($where_periodo)
        ->where('rol_id',3)
        ->count();


        $total_profesionales_respondidos=respuesta_encuestaModel::join('usuario','usuario.id','respuesta_encuesta.usuario_id')
        ->where('encuesta_id',$request->id)
        ->whereraw($where_carrera)
        ->whereraw($where_anios)
        ->whereraw($where_periodo)
        ->count();

        $sin_respuesta=$total_profesionales-$total_profesionales_respondidos;

        return response()->json(array('Total' => $total_profesionales, 'que han respondido' => $total_profesionales_respondidos, 'que no han respondido' => $sin_respuesta));              
    }

    public function total_respuestas_ajax()
    {
        $request=request();
        $tipo_respuesta=$request->tipo_respuestas;
        $where_carrera='0=0';
        $where_periodo="'0'='0'";
        $where_anios='0=0';
        $where_respuesta='0=0';
        $encuesta_id=$request->id;

        if($tipo_respuesta!=''&&$tipo_respuesta!=null){
            if($tipo_respuesta=='SI'){
                $where_respuesta='respuesta_encuesta.encuesta_id is not null';
            }
            if($tipo_respuesta=='NO'){
                $where_respuesta='respuesta_encuesta.encuesta_id is null';
            }

        }
        
        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            $where_carrera='0=1';
        }
        

        if($request->carreras){
            $dat='';
            foreach ($request->carreras as $key => $value) {
                $dat.=$value.',';
            }
            $dat = trim($dat, ",");
            $where_carrera="usuario.carrera_id IN ($dat)";
        }

         if($request->periodos){
            $datx='';
            foreach ($request->periodos as $key => $periodo) {
                $datx.="'$periodo',";
            }
            $datx = trim($datx, ",");
            $where_periodo="usuario.periodo_academico IN ($datx)";
        }

         if($request->anios){
            $anios='';
            foreach ($request->anios as $key => $an) {
                $anios.=intval($an).',';
            }
            $anios = trim($anios, ",");
            $where_anios="usuario.anio_graduacion IN ($anios)";
        }      

        $request = request();
        $limit   = $request->input('length');
        $start   = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = usuarioModel::Leftjoin('respuesta_encuesta', function($join) use($encuesta_id)
            {
                $join->on('respuesta_encuesta.usuario_id', '=', 'usuario.id')
                     ->where('respuesta_encuesta.encuesta_id',$encuesta_id);
            })->whereraw($where_carrera)
            ->whereraw($where_anios)
            ->whereraw($where_periodo)
            ->whereraw($where_respuesta)
            ->where('rol_id',3)
            ->orderBy('usuario.apellidos')
            ->skip($start)->take($limit)->get();
            
            $totalData     = usuarioModel::Leftjoin('respuesta_encuesta', function($join) use($encuesta_id)
            {
                $join->on('respuesta_encuesta.usuario_id', '=', 'usuario.id')
                     ->where('respuesta_encuesta.encuesta_id',$encuesta_id);
            })->whereraw($where_carrera)
            ->whereraw($where_anios)
            ->whereraw($where_periodo)
            ->whereraw($where_respuesta)
            ->where('rol_id',3)
            ->count();
            $totalFiltered = $totalData;
        } else {
            $search    = mb_strtoupper($request->input('search.value'));
            $registros = usuarioModel::Leftjoin('respuesta_encuesta', function($join) use($encuesta_id)
            {
                $join->on('respuesta_encuesta.usuario_id', '=', 'usuario.id')
                     ->where('respuesta_encuesta.encuesta_id',$encuesta_id);
            })
            ->where(function ($query) use ($search) {
                $query->where("nombres", 'ilike', '%' . $search . '%')
                    ->orWhere("apellidos", 'ilike', '%' . $search . '%')
                    ->orWhere("identificacion", 'ilike', '%' . $search . '%')
                    ->orWhere("email", 'ilike', '%' . $search . '%')
                    ->orWhere("periodo_academico", 'ilike', '%' . $search . '%')
                    ->orWhere("telefono", 'ilike', '%' . $search . '%');
            })->where('rol_id',3)
            ->whereraw($where_carrera)
            ->whereraw($where_anios)
            ->whereraw($where_periodo)
            ->whereraw($where_respuesta)
            ->orderBy('usuario.apellidos')
            ->skip($start)->take($limit)->get();
            $totalData     = usuarioModel::Leftjoin('respuesta_encuesta', function($join) use($encuesta_id)
            {
                $join->on('respuesta_encuesta.usuario_id', '=', 'usuario.id')
                     ->where('respuesta_encuesta.encuesta_id',$encuesta_id);
            })
            ->where(function ($query) use ($search) {
                $query->where("nombres", 'ilike', '%' . $search . '%')
                    ->orWhere("apellidos", 'ilike', '%' . $search . '%')
                    ->orWhere("identificacion", 'ilike', '%' . $search . '%')
                    ->orWhere("periodo_academico", 'ilike', '%' . $search . '%')
                    ->orWhere("email", 'ilike', '%' . $search . '%')
                    ->orWhere("telefono", 'ilike', '%' . $search . '%');
            })->where('rol_id',3)
            ->whereraw($where_carrera)
            ->whereraw($where_anios)
            ->whereraw($where_periodo)
            ->whereraw($where_respuesta)
            ->count();
            $totalFiltered = $totalData;
        }
        $data = array();



        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                //dd($value);
                $length   = strlen($value->id);

                //no han respondido
                $color='#DF4C10';
                $respuesta='NO';

                //en caso de que si ha respondido
                if($value->encuesta_id==$request->id){
                    $color='#B4DF10';
                    $respuesta='SI';
                }

                $usuario_data['identificacion'] = '<span>' . $value->identificacion . '</span>';
                $usuario_data['nombres']        = '<span>' . $value->apellidos . '<br> '.$value->nombres.'</span>';
                $usuario_data['email']          = '<span>' . $value->email . '</span>';
                $usuario_data['telefono']          = '<span>' . $value->telefono . '</span>';
                $usuario_data['respuesta']          = '<span class="dot" style=" background-color:  '.$color.' ;"></span><span> ' . $respuesta . '</span>';
                $usuario_data['telefono']       = '<span>' . $value->telefono . '</span>';                
                $usuario_data['periodo_academico']       = '<span>' . $value->periodo_academico . '</span>';
                $carrera=null;
                $usuario_data['carrera']       = '<span>' . $carrera . '</span>';
                $data[]= $usuario_data;                
            }
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        ];
        return json_encode($json_data);
    }
    public function responder_encuesta($codigo)
    {

        $id = decrypt($codigo);
       // dd($id);

        $encuesta = encuestaModel::find($id);
        if (is_null($encuesta)) {
            Session::flash('message-error', 'Usted esta ingresando a una url inválida');
            return Redirect::Back();
        }


     
            
        //compruebo que ya tenga la respuesta
        $respuesta = comprobar_si_respondio($id);
        if ($respuesta>0) {
            Session::flash('message', 'USTED YA RESPONDIÓ LA ' . $encuesta->nombre . '.');
            return Redirect::Back();
        }



        $respondida=estado_respuesta_encuesta($id);

        if($respondida!=3){
            Session::flash('message-error', 'LA ' . $encuesta->nombre . ' AÚN NO ESTA ACTIVA PARA SU PROMOCIÓN');
            return Redirect::Back();
        }
        

        $preguntas = preguntaModel::join('grupo_preguntas', 'grupo_preguntas.id', 'pregunta.grupo_preguntas_id')
            ->where('encuesta_id', $encuesta->id)
            ->where(function($query){
                $query->whereNull('carrera_id')
                ->orwhere('carrera_id',Auth::user()->carrera_id);
            })->with('opciones')
            ->select('pregunta.*')
            ->orderBy('order')
            ->get();

        if (!count($preguntas)) {
            Session::flash('message-error', 'Aún no existen preguntas registradas para la encuesta');
            return Redirect::Back();
        }

        return view('encuestas.responder', compact('encuesta', 'preguntas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $encuesta=encuestaModel::find($id);
        $preguntas = preguntaModel::join('grupo_preguntas', 'grupo_preguntas.id', 'pregunta.grupo_preguntas_id')
        ->where('encuesta_id', $encuesta->id)
        ->select('pregunta.*')
        ->orderBy('order')
        ->get();

        $grupo_preguntas=grupopreguntasModel::where('encuesta_id',$encuesta->id)->pluck('nombre','id');
        return view('admin.encuestas.edit',compact('encuesta','preguntas','grupo_preguntas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodos=null;
        if(count($request->periodos)){
            $periodos=join("|",$request->periodos); 
        }
        $encuesta=encuestaModel::find($id);
        $encuesta->nombre=$request->nombre;
        $encuesta->anios=$request->anios;
        $encuesta->periodos=$periodos;
        $encuesta->save();
        Session::flash('message', 'ENCUESTA ACTUALIZADA CORRECTAMENTE');
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function responder_encuesta_post(Request $request)
    {
        $id      = intval($request->id);
           //compruebo que ya tenga la respuesta
        $respuesta = comprobar_si_respondio($id);
        if ($respuesta>0) {
            $encuesta  = encuestaModel::find($id);
            Session::flash('message-error', 'USTED YA RESPONDIÓ LA ' . $encuesta->nombre . '.');
            return Redirect('inicio');
        }

        $valores = $request->all();
        DB::beginTransaction();
        try {

            if (count($valores) > 2) {
                $respuesta_encuesta              = new respuesta_encuestaModel();
                $respuesta_encuesta->encuesta_id = $id;
                $respuesta_encuesta->usuario_id  = Auth::id();
                $respuesta_encuesta->save();
                foreach ($valores as $pregunta => $opcion) {
                    if ($pregunta != "_token" && $pregunta != "id") {

                        //tomo el primer caracter
                        $tipo     = substr($pregunta, 0, 1);
                        $pregunta = substr($pregunta, 1);
                        $pregunta = intval($pregunta);
                        $opcion   = intval($opcion);

                        //creo la respuesta
                        $respuesta_enc                        = new respuestaModel();
                        $respuesta_enc->respuesta_encuesta_id = $respuesta_encuesta->id;

                        if ($tipo == 'p') {
                            $respuesta_enc->pregunta_id = $pregunta;
                            $respuesta_enc->opcion_id   = $opcion;
                        }

                        if ($tipo == 'o') {
                            $opcion_busc                          = opcionModel::find($pregunta);
                            $respuesta_enc->pregunta_id           = $opcion_busc->pregunta_id;
                            $respuesta_enc->opcion_id             = $pregunta;
                            $respuesta_enc->respuesta             = $opcion;
                            $respuesta_enc->respuesta_encuesta_id = $respuesta_encuesta->id;

                        }

                        $respuesta_enc->save();
                    }
                }
            }

            DB::commit();
            Session::flash('message', 'Encuesta respondida exitosamente');
            return redirect('inicio');
        } catch (Exception $e) {
            Session::flash('message-error', 'Ocurrió algo, no se pudo generar emisiones');
            DB::rollBack();
            return Redirect::Back()->withinputs();

        }

    }



    public function resultados($tipo)
    {
        $id        = decrypt($tipo);
        $encuesta  = encuestaModel::find($id);
        $where_usuario='0=0';
        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            $where_usuario='direccion_carrera.usuario_id='.Auth::id();
        }


         $carreras = direccion_carreraModel::join('carrera','carrera.id','direccion_carrera.carrera_id')
         ->join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->where('carrera.institucion_id',1)
        ->whereraw($where_usuario)
        ->select(DB::raw("CONCAT(ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->get();

        $periodos=usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
        $anios=usuarioModel::whereNotNull('anio_graduacion')->distinct('anio_graduacion')->pluck('anio_graduacion','anio_graduacion');
        
        return view('encuestas.resultados', compact('encuesta','carreras','periodos','anios'));
    }

    function resultados_general($tipo){
        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            Session::flash('message-error', 'su rol no puede acceder aquí');
            return Redirect::Back();
        }




         $id        = decrypt($tipo);
        $encuesta  = encuestaModel::find($id);

        $periodos=usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
        $anios=usuarioModel::whereNotNull('anio_graduacion')->distinct('anio_graduacion')->pluck('anio_graduacion','anio_graduacion');
        
        return view('encuestas.resultado_general', compact('encuesta','periodos','anios'));
    }

    public function obtener_pdf(Request $request){
    //OBTENGO EL ID
    $id=decrypt($request->id);
    //OBTENGO LA ENCUESTA
    $encuesta  = encuestaModel::find($id);
    //LOS FILTROS
    $where_carrera='0=0';
    $where_carrera2='0=0';
    //EL TEXTO DE CARRERA
    $carrerax=null;    

        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            $where_carrera='0=1';
            $where_carrera2='0=1';
        }

            //si tengo seleccionado carreras
            if($request->carreras){
                $dat='';
                foreach ($request->carreras as $key => $value) {
                    $dat.=$value.',';
                }
                $dat = trim($dat, ",");
                $where_carrera="usuario.carrera_id IN ($dat)";     
                $where_carrera2="id IN ($dat)";       
            }

            $carrera=carreraModel::whereraw($where_carrera2)->get();
            $carrerax=$carrera->groupBy('titulo.titulo')->keys()->implode(',');
            if(count($carrera)>5){
                $carrerax=count($carrera);
            }
        $request->id=$id;
        $request->aqui=$id;
        $resultados=$this->consultar_resultados($request);
        $total=$resultados['total'];
        $preguntas=$resultados['resultados'];
        return view('resultados.reporte_encuesta',compact('preguntas','total','encuesta','carrerax'));        
    }

    public function consultar_resultados(Request $request)
    {
        $where_carrera='0=0';
        $where_carrera2='0=0';
        $where_periodo="'0'='0'";
        $where_anios='0=0';
        $where_preguntas_id='0=0';

        
        if(Auth::user()->rol_id!=1&&Auth::user()->rol_id!=5){
            $where_carrera='0=1';
        }
        

        if($request->carreras){
            $dat='';
            foreach ($request->carreras as $key => $value) {
                $dat.=$value.',';
            }
            $dat = trim($dat, ",");
            $where_carrera="usuario.carrera_id IN ($dat)";

            if(count($request->carreras)==1){
                $where_carrera2="pregunta.carrera_id IS NULL or pregunta.carrera_id IN ($dat)";
            }else{
                $where_carrera2="pregunta.carrera_id IS NULL";
            }
            
        }else{
            $where_carrera2="pregunta.carrera_id IS NULL";
        }

         if($request->periodos){
            $datx='';
            foreach ($request->periodos as $key => $periodo) {
                $datx.="'$periodo',";
            }
            $datx = trim($datx, ",");
            $where_periodo="usuario.periodo_academico IN ($datx)";
        }

         if($request->anios){
            $anios='';
            foreach ($request->anios as $key => $an) {
                $anios.=intval($an).',';
            }
            $anios = trim($anios, ",");
            $where_anios="usuario.anio_graduacion IN ($anios)";
        }      

        $id = $request->id;
        $preguntas = preguntaModel::join('grupo_preguntas', 'grupo_preguntas.id', 'pregunta.grupo_preguntas_id')
            ->where('encuesta_id', $id)
            ->where(function($query) use($where_carrera2){
                $query->orwhereraw($where_carrera2);
            })
            ->select('pregunta.id', 'pregunta.opciones_respuesta', 'pregunta.pregunta','pregunta.order','pregunta.carrera_id')
            ->orderBy('order')
            ->get();
        foreach ($preguntas as $pregunta) {
            if ($pregunta->opciones_respuesta == 0) {
                $data=respuestaModel::join('opcion', 'opcion.id', '=', 'respuesta.opcion_id')
                ->join('respuesta_encuesta','respuesta_encuesta.id','respuesta.respuesta_encuesta_id')
                ->join('usuario','usuario.id','respuesta_encuesta.usuario_id')
                ->where('respuesta.pregunta_id', $pregunta->id)
                ->whereraw($where_carrera)
                ->whereraw($where_anios)
                ->whereraw($where_periodo)
                ->select(DB::raw('COUNT(opcion.id) as total'), "opcion.opcion as opcion")
                ->groupBy('opcion_id','opcion.opcion')
                ->orderBy('total', 'DESC')
                ->get();
                $pregunta->resultados = $data;
                $pregunta->total_respuestas=$pregunta->resultados->sum('total');
            } else {
                $opciones = opcionModel::where('pregunta_id', $pregunta->id)->get(['id']);
                foreach ($opciones as $opx) {
                    $resul=respuestaModel::join('opcion', 'opcion.id', '=', 'respuesta.respuesta')  
                    ->join('respuesta_encuesta','respuesta_encuesta.id','respuesta.respuesta_encuesta_id')
                        ->join('usuario','usuario.id','respuesta_encuesta.usuario_id')
                        ->where('respuesta.opcion_id', $opx->id)
                        ->whereraw($where_carrera)
                        ->whereraw($where_anios)
                        ->whereraw($where_periodo)
                        ->select(DB::raw('COUNT(opcion.id) as total'), "opcion.opcion as opcion")
                        ->groupBy('opcion_id','opcion.opcion')
                        ->groupBy('opcion.opcion')
                        ->orderBy('total', 'ASC')
                        ->get(); ;
                    $opx->resultados =$resul; 
                    $opx->total_respuestas=$opx->resultados->sum('total');
                        $pregunta->resultados = $opciones;
                }
            }
        }
        $total = respuesta_encuestaModel::join('usuario', 'usuario.id', 'respuesta_encuesta.usuario_id')
        ->whereraw($where_carrera)
        ->whereraw($where_anios)
        ->whereraw($where_periodo)
        ->where('encuesta_id', $id)
        ->count();
        if(isset($request->aqui)){
            return array('resultados' => $preguntas, 'total' => $total);
        }

        $vista=view('encuestas.res_preguntas',compact('preguntas'))->render();
        return response()->json(array('resultados' => $preguntas, 'total' => $total,'vista'=>$vista));
    }

    function actualizar_preguntas(Request $request){
        $valores = $request->all();
        DB::beginTransaction();
        try {
           if (count($valores) > 2) {
                foreach ($valores as $opcion => $valor) {

                    if ($opcion != "_token" && $opcion != "_method") {

                        $opciones=explode('-',$opcion);
                        $tabla=$opciones[0];
                        $opcion=$opciones[1];
                        $id=intval($opciones[2]);

                        if($tabla=='pregunta'){
                            $dato=preguntaModel::find($id);
                        }

                        if($tabla=='opcion'){
                            $dato=opcionModel::find($id);
                        }
           
                        $dato[$opcion]=$valor;
                        $dato->save();

                        if($opcion=="pregunta-pregunta-60"){
                            dd($dato);
                        }
                    }

                }
            }
        DB::commit();
            Session::flash('message', 'Datos de preguntas actualizadas correctamente');
            return redirect::Back();
        } catch (Exception $e) {
            Session::flash('message-error', 'Ocurrió algo, no se pudo editar las preguntas');
            DB::rollBack();
            return Redirect::Back()->withinputs();

        }
    }
    
    function actualizar_pregunta(Request $request){
        $numero=intval($request->orden);
        $texto=mb_strtoupper($request->texto);
        $id=intval($request->id);
        $pregunta=preguntaModel::find($id);
        $pregunta->pregunta=mb_strtoupper($request->texto);
        $pregunta->order=$numero;
        $pregunta->save();        
        return 1;
    }
    function agregar_opcion(Request $request){
        $tipo=intval($request->tipo_ingreso);
        $op=mb_strtoupper($request->opcion);
        if($tipo==1){
            //opcion
            $opcion=opcionModel::where('pregunta_id',$request->pregunta_id)
            ->where('orden',$request->orden)
            ->where('opcion',$op)
            ->first();
            if(is_null($opcion)){
                $opcion=new opcionModel();
                $opcion->pregunta_id=$request->pregunta_id;
                $opcion->orden=$request->orden;
                $opcion->opcion=$op;
                $opcion->save();
                Session::flash('message', 'Opción registrada correctamente');
            }else{
                Session::flash('message-error', 'Ocurrió algo, Opcion ya estuvo registrada anteriormente');
            }
        }

         if($tipo==2){
            $encuesta_id=intval($request->encuesta_id);
            $pregunta=preguntaModel::where('grupo_preguntas_id',intval($request->grupo_pregunta_id))
            ->where('pregunta',$op)
            ->first();
            if(is_null($pregunta)){
                $pregunta=new preguntaModel();
                $pregunta->pregunta=$op;
                $pregunta->titulo_respuesta=mb_strtoupper($request->titulo_respuesta);
                $pregunta->grupo_preguntas_id=intval($request->grupo_pregunta_id);
                $pregunta->order=$request->orden;
                $pregunta->opciones_respuesta=0;
                $pregunta->save();

            }
         }
        
        return 1;
    }

    function eliminar_dato_pregunta(Request $request){
        try {
            if($request->tipo==2){
                //OPCION
                opcionModel::find($request->id)->delete();
                return 1;
            }

            if($request->tipo==1){
                opcionModel::where('pregunta_id',$request->id)->delete();
                preguntaModel::find($request->id)->delete();
                return 1;
            }
          
        } catch (\Illuminate\Database\QueryException $e) {
            return 2;
        }
    }


    function regular_pregunta(){
        /*
        $respuestas=respuesta_encuestaModel::all();
        foreach ($respuestas as $resp) {
            for ($i=1; $i<=6 ; $i++) { 
                $preg1=respuestaModel::where('respuesta_encuesta_id',$resp->id)
                ->where('opcion_id',$i)
                ->where('pregunta_id',59)
                ->first();
                if(is_null($preg1)){
                    $preg1=new respuestaModel();
                    $preg1->respuesta_encuesta_id=$resp->id;        
                    $preg1->pregunta_id=59;
                    $preg1->opcion_id=$i;
                    $preg1->respuesta=rand(419,421);
                    $preg1->save();
                }                
            }
        }
        */
           
        //encuentro donde hayan respondido que si se graduaron en el tienmpo establecido y hayan respondido 3,4,5 y las elimino
        $respuestas_si=respuestaModel::where('opcion_id',345)
        ->where('pregunta_id',50)
        ->pluck('respuesta_encuesta_id');
        $respuestas= respuestaModel::whereIn('respuesta_encuesta_id',$respuestas_si)
        ->whereIn('pregunta_id',[51,52,53])
        ->delete();
        
        //regular_pregunta
        




        //encuentro donde hayan respondido que si se graduaron en el tienmpo establecido y hayan respondido 3,4,5 y las elimino
        /*
        $respuestas_no=respuestaModel::where('opcion_id',346)
        ->where('pregunta_id',50)
        ->pluck('respuesta_encuesta_id');
        $respuestas=respuestaModel::whereIn('respuesta_encuesta_id',$respuestas_no)
        ->select(DB::raw("count(respuesta.id) as total"),'respuesta_encuesta_id')
        ->groupBy('respuesta_encuesta_id')
        ->orderBy('total','DESC')
        ->get();
        dd($respuestas);*/
        /*
        $respuestas= respuestaModel::whereIn('respuesta_encuesta_id',$respuestas_si)
        ->whereIn('pregunta_id',[51,52,53])
        ->get();
        */










       // dd($respuestas);
       // ->delete();
     //   dd(count($respuestas));
       // ->delete();
      //  dd('listo');
    }
}
