<?php

namespace App\Http\Controllers;

use App\tituloModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;

class tituloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.titulos.index');
    }



         public function titulos_ajax()
    {
        $request=request();
        $limit = $request->input('length');
        $start = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = tituloModel::skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }else {
            $search = $request->input('search.value');
            $registros = tituloModel::where(function($query) use ($search) {
                $query->where("titulo" , 'ILIKE' , '%'.$search.'%')
                ->orWhere("icono", 'ILIKE', '%' . $search . '%')
                ->orWhere("profesion", 'ILIKE', '%' . $search . '%');                 
            })
            ->skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                

                $acciones = "<a href=" . route('titulos.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar título'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar título'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']       = '<span>'.$value->id.'</span>'; 
                $usuario_data['titulo'] = '<span>'.$value->titulo.'</span>';  
                  $usuario_data['icono'] = '<span>'.$value->icono.'</span>'; 
                 $usuario_data['profesion'] = '<span>'.$value->profesion.'</span>';             
                $usuario_data['acciones']=$acciones;
                $data[] = $usuario_data;
            }
        }
        $json_data = [
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ];
        return json_encode($json_data);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
         return view('admin.titulos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            //'nombre' => 'required|unique:tipo_pagos',
        ]);
        $titulo            = new tituloModel();
        $titulo->titulo = $request->titulo;
        $titulo->icono         = $request->icono;
        $titulo->profesion         = $request->profesion;
        $titulo->save();
        Session::flash('message', 'Se ha ingresado un nuevo título al Sistema');
        return redirect()->route('titulos.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\tituloModel  $tituloModel
     * @return \Illuminate\Http\Response
     */
    public function show(tituloModel $tituloModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tituloModel  $tituloModel
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $titulo  = tituloModel::find($id);
        
        return View('admin.titulos.edit', compact('titulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tituloModel  $tituloModel
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
     {
        $this->validate($request, [
          //  'nombre' => 'required|unique:rol,nombre,' . $id,
        ]);
        $titulo              = tituloModel::find($id);
        $titulo->titulo = $request->titulo;
        $titulo->icono         = $request->icono;
        $titulo->profesion         = $request->profesion;
        $titulo->save();
        Session::flash('message', 'Se ha actualizado el título al Sistema');
        return redirect()->route('titulos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tituloModel  $tituloModel
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        tituloModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente el título');
        return 1;
    }
}
