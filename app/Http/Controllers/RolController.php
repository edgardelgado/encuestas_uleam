<?php

namespace App\Http\Controllers;

use App\AsignarPermisosModel;
use App\Http\Controllers\Controller;
use App\MenuOpcionesModel;
use App\rolModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use View;

class rolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $roles = rolModel::where('id', '<>', 1)->get();

        return View::make('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|unique:rol',
        ]);
        $roles              = new rolModel();
        $roles->descripcion = $request->descripcion;
        //dd($roles);
        $roles->save();
        Session::flash('message', 'Se ha ingresado un nuevo rol de Usuario al Sistema');
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = rolModel::find($id);
        // dd($rol);
        return View('admin.roles.edit', compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'descripcion' => 'required|unique:rol,descripcion,' . $id,
        ]);
        $rol              = rolModel::find($id);
        $rol->descripcion = $request->descripcion;
        $rol->save();
        Session::flash('message', 'Se ha actualizado el rol de Usuario al Sistema');
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        rolModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente el rol');
        return 1;
    }

    public function administrar_permisos($id)
    {
        $rol = rolModel::find($id);

        $tabla = MenuOpcionesModel::where('nivel', 1)
        ->where('proyecto',1)->orderby("orden")->get();
        foreach ($tabla as $tab) {
            $tab->menus = MenuOpcionesModel::where('nivel', 2)
            ->where('proyecto',1)
            ->where('idmain', $tab->id)->orderby("orden")->get();
        }
        

        return view('admin.roles.permisos', compact('tabla', 'rol'));
    }
    public function guardar_permisos(Request $request)
    {
        $menus = $request->idmenumain;
        AsignarPermisosModel::where('idrol', $request->rol_id)->delete();
        $num = 0;
        if (count($menus)) {
            foreach ($menus as $key => $value) {
                $num++;
                //hijo
                $menu_hijo = AsignarPermisosModel::where('idrol', $request->rol_id)->where('idmenu', $value)->first();
                if (is_null($menu_hijo)) {
                    $menu_rol         = new AsignarPermisosModel();
                    $menu_rol->idrol  = $request->rol_id;
                    $menu_rol->idmenu = $value;
                    $menu_rol->save();
                }
                //padre
                $menu = MenuOpcionesModel::find($value);
                if ($menu->nivel == 2) {
                    $idpadre   = intval($menu->idmain);
                    $menupadre = AsignarPermisosModel::where('idrol', $request->rol_id)->where('idmenu', $idpadre)->first();
                    if (is_null($menupadre)) {
                        $menu_padre         = new AsignarPermisosModel();
                        $menu_padre->idrol  = $request->rol_id;
                        $menu_padre->idmenu = $idpadre;
                        $menu_padre->save();
                    }
                }
            }
        }
        $nombre = rolModel::find($request->rol_id)->nombre;
        Session::flash('message', 'Se ha actualizado correctamente los permisos para el rol ' . $nombre);
        return Redirect::to('configuracion/roles');
    }

}
