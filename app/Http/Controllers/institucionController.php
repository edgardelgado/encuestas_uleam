<?php

namespace App\Http\Controllers;

use App\institucionModel;
use App\paisModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;

class institucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        //
        return view('admin.instituciones.index');
    }


         public function instituciones_ajax()
    {
        $request=request();
        $limit = $request->input('length');
        $start = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = institucionModel::skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }else {
            $search = $request->input('search.value');
            $registros = institucionModel::where(function($query) use ($search) {
                $query->where("nombre" , 'LIKE' , '%'.$search.'%')
                ->orWhere("siglas", 'LIKE', '%' . $search . '%');                 
            })->orWhereHas('pais', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
            })
            ->skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                

                $acciones = "<a href=" . route('instituciones.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar Institución'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Institución'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']       = '<span>'.$value->id.'</span>'; 
                $usuario_data['nombre'] = '<span>'.$value->nombre.'</span>';  
                $usuario_data['siglas'] = '<span>'.$value->siglas.'</span>';  
                 $usuario_data['pais'] = '<span>'.$value->pais->nombre.'</span>';             
                $usuario_data['acciones']=$acciones;
                $data[] = $usuario_data;
            }
        }
        $json_data = [
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ];
        return json_encode($json_data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $pais = paisModel::pluck('nombre', 'id');
         return view('admin.instituciones.crear', compact('pais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            //'nombre' => 'required|unique:tipo_pagos',
        ]);
        $institucion            = new institucionModel();
        $institucion->nombre = $request->nombre;
        $institucion->siglas         = $request->siglas;
        $institucion->pais_id         = $request->pais_id;
        $institucion->save();
        Session::flash('message', 'Se ha ingresado una nueva Institución  al Sistema');
        return redirect()->route('instituciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\institucionModel  $institucionModel
     * @return \Illuminate\Http\Response
     */
    public function show(institucionModel $institucionModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\institucionModel  $institucionModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institucion  = institucionModel::find($id);
        $pais            = paisModel::pluck('nombre', 'id');
        return View('admin.instituciones.edit', compact('institucion','pais'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\institucionModel  $institucionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
     {
        $this->validate($request, [
          //  'nombre' => 'required|unique:rol,nombre,' . $id,
        ]);
        $institucion              = institucionModel::find($id);
         $institucion->nombre = $request->nombre;
        $institucion->siglas         = $request->siglas;
        $institucion->pais_id         = $request->pais_id;
        $institucion->save();
        Session::flash('message', 'Se ha actualizado la Institución en el Sistema');
        return redirect()->route('instituciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\institucionModel  $institucionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        institucionModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente la Institución del usuario');
        return 1;
    }
}
