<?php

namespace App\Http\Controllers;
use App\respuestaModel;
use App\respuesta_encuestaModel;
use App\direccion_carreraModel;
use App\carreraModel;
use App\encuestaModel;
use App\grupopreguntasModel;
use App\facultadModel;
use App\tituloModel;
use App\estudioModel;
use App\ciudadModel;
use App\opcionModel;
use App\preguntaModel;
use App\rolModel;
use App\usuarioModel;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use Hash;

class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.usuarios.index');
    }

    function iniciar_sesion_user($cod){
        if(Auth::user()->rol_id==1){
            $id=decrypt($cod);
            Auth::loginUsingId($id, true);
            return 1;
        }  
    }


    public function usuarios_ajax()
    {
        $request = request();
        $limit   = $request->input('length');
        $start   = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = usuarioModel::skip($start)->take($limit)->get();
            // $registros = rubroModel::with('tipo_impuesto')->skip($start)->take($limit)->get();
            $totalData     = usuarioModel::count();
            $totalFiltered = $totalData;
        } else {
            $search    = $request->input('search.value');
            $registros = usuarioModel::where(function ($query) use ($search) {
                $query->where("nombres", 'LIKE', '%' . $search . '%')
                    ->orWhere("apellidos", 'LIKE', '%' . $search . '%')
                    ->orWhere("identificacion", 'LIKE', '%' . $search . '%')
                    ->orWhere("email", 'LIKE', '%' . $search . '%')
                    ->orWhere("telefono", 'LIKE', '%' . $search . '%')
                    ->orWhere("estado", 'LIKE', '%' . $search . '%');
            })->orWhereHas('carrera.titulo', function ($query) use ($search) {
                $query->where("titulo.titulo", 'LIKE', '%' . $search . '%');
            })->orWhereHas('rol', function ($query) use ($search) {
                $query->where("descripcion", 'LIKE', '%' . $search . '%');
            })->skip($start)->take($limit)->get();
            $totalData     = usuarioModel::where(function ($query) use ($search) {
                $query->where("nombres", 'LIKE', '%' . $search . '%')
                    ->orWhere("apellidos", 'LIKE', '%' . $search . '%')
                    ->orWhere("identificacion", 'LIKE', '%' . $search . '%')
                    ->orWhere("email", 'LIKE', '%' . $search . '%')
                    ->orWhere("telefono", 'LIKE', '%' . $search . '%')
                    ->orWhere("estado", 'LIKE', '%' . $search . '%');
            })->orWhereHas('carrera.titulo', function ($query) use ($search) {
                $query->where("titulo.titulo", 'LIKE', '%' . $search . '%');
            })->orWhereHas('rol', function ($query) use ($search) {
                $query->where("descripcion", 'LIKE', '%' . $search . '%');
            })->count();
            $totalFiltered = $totalData;
        }
        $data = array();

        if (!empty($registros)) {
            //dd($registros);
            foreach ($registros as $key => $value) {
                $length   = strlen($value->id);
                $acciones = "<a href=" . route('usuarios.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar Usuario'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Usuario'><i class='fa fa-trash-o text-danger'></i></a>";

                if(Auth::user()->rol_id==1){
                    $acciones.=" <a  class='btn btn-sm btn-default js-sweetalert' onClick='iniciarsesion(`" . encrypt($value->id) . "`)' data-toggle='tooltip' data-placement='top' title='Iniciar Sesión'><i class='fa fa-sign-in text-primary'></i></a>";
                }
                $usuario_data['id']             = ' <span>' . $value->id . '</span>';
                $usuario_data['identificacion'] = ' <span>' . $value->identificacion . '</span>';
                $usuario_data['nombres']        = '<span>' . $value->nombres . '</span>';
                $usuario_data['apellidos']      = '<span>' . $value->apellidos . '</span>';
                $usuario_data['email']          = '<span>' . $value->email . '</span>';
                $usuario_data['telefono']       = '<span>' . $value->telefono . '</span>';
                $usuario_data['estado']         = '<span>' . $value->estado . '</span>';
                $usuario_data['ti']             = '<span>' . $value->nombre . '</span>';
                // dd($usuario_data['ti']);
                $usuario_data['roles']    = '<span>' . $value->rol->descripcion . '</span>';
                $usuario_data['acciones'] = $acciones;
                $data[]                   = $usuario_data;

                //  dd($usuario_data);
            }
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        ];
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carreras = carreraModel::join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->join('institucion','institucion.id','carrera.institucion_id')
        ->select(DB::raw("CONCAT(institucion.nombre,' - ',ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->pluck('descripcion', 'id');
        $roles    = rolModel::pluck('descripcion', 'id');
        return view('admin.usuarios.create', compact('carreras', 'roles'));
    }

    public function subir_encuestas()
    {
        return view('admin.encuestas.subir_encuestas');
    }

    public function subir_encuesta_post(Request $request)
    {
        ini_set('max_execution_time', 3000);
        $this->validate($request, [
            'identificacion' => 'required',
            //'anios'          => 'required',
            'periodos'       => 'required',
        ]);
        if ($request->hasFile('encuesta')) {
            $path      = $request->file('encuesta')->getRealPath();
            $preguntas = \Excel::load($path)->toArray();
            if (count($preguntas)) {

                DB::beginTransaction();
                try {

                    $periodos=null;
                    if(count($request->periodos)){
                        $periodos=join("|",$request->periodos); 
                    }
                    

                    $encuesta                     = new encuestaModel();
                    $encuesta->nombre             = $request->identificacion;
                    $encuesta->anios = $request->meses;
                    $encuesta->usuario_id         = Auth::id();
                    $encuesta->periodos=$periodos;
                    $encuesta->save();
                    foreach ($preguntas as $key => $value) {
                        if ($value['titulo_pregunta'] != '') {
                            $grupo_pregunta_txt = mb_strtoupper($value['titulo']);
                            $grupo_preguntas    = grupopreguntasModel::where('nombre', $grupo_pregunta_txt)
                                ->where('encuesta_id', $encuesta->id)
                                ->first();
                            if (is_null($grupo_preguntas)) {
                                $grupo_preguntas              = new grupopreguntasModel();
                                $grupo_preguntas->nombre      = $grupo_pregunta_txt;
                                $grupo_preguntas->encuesta_id = $encuesta->id;
                                $grupo_preguntas->save();
                            }

                            $pregunta                     = new preguntaModel();
                            $pregunta->grupo_preguntas_id = $grupo_preguntas->id;
                            $pregunta->pregunta           = mb_strtoupper($value['titulo_pregunta']);
                            $pregunta->order              = intval($value['numero']);
                            if($value['carrera_id']!=''){
                                $pregunta->carrera_id              = intval($value['carrera_id']);
                            }                            
                            $pregunta->titulo_respuesta   = mb_strtoupper($value['titulo_respuesta']);
                            // $pregunta->opciones_respuesta=mb_strtoupper($value['opcion_respuesta']);
                            $pregunta->opciones_respuesta = 0;
                            $pregunta->save();

                            if ($value['opcion_respuesta'] != '') {
                                $pregunta->opciones_respuesta = 1;
                                $pregunta->save();
                                $opciones_resp = explode('|', $value['opcion_respuesta']);
                                foreach ($opciones_resp as $keyx => $opx) {
                                    $opcionx                        = new opcionModel();
                                    $opcionx->opcion                = mb_strtoupper($opx);
                                    $opcionx->pregunta_principal_id = $pregunta->id;
                                    $opcionx->save();
                                }
                            }
                            //en encuesta debe ir asi como en bolsa de empleo 
                            //por ahora es la prioridad que empiece a funcar bolsa de empelo
                            
                            $opciones = explode('|', $value['respuestas']);
                            foreach ($opciones as $key => $op) {
                                $opcion              = new opcionModel();
                                $opcion->opcion      = mb_strtoupper($op);
                                $opcion->pregunta_id = $pregunta->id;
                                $opcion->save();
                            }

                        }
                    }

                    DB::commit();
                    Session::flash('message', 'Empresas ingresadas de manera masiva correctamente, los gerentes pueden iniciar con el ruc de la empresa y la contraseña es el mismo dato');
                    return Redirect('/inicio');

                } catch (Exception $e) {
                    DB::rollBack();
                    Session::flash('message-error', $e->getMessage());
                    return Redirect::back();
                }

            } else {
                Session::flash('message-error', 'Noo se ha recibido ninguno archivo');
                return Redirect::back();
            }
        }
        Session::flash('message-error', 'No se ha recibido ninguno archivo');
        return Redirect::back();
    }


function subir_encargados_post(Request $request){
         ini_set('max_execution_time', 3000);
        $this->validate($request, [
            'profesionales'       => 'required',
        ]);
        if ($request->hasFile('profesionales')) {
            $path      = $request->file('profesionales')->getRealPath();
            $profesionales = \Excel::load($path)->toArray();
            if (count($profesionales)) {
                DB::beginTransaction();
                try {
                    foreach ($profesionales as $key => $value) {
                        if ($value['cedula'] != '') {
                            $cedula=strval($value['cedula']);
                            $nombres_compuestos=mb_strtoupper($value['nombres']);
                            $email=$value['correo_institucional'];
                            $telefono=$value['telefono'];                   
                            $email=$value['correo_institucional'];
                            $resultado_nombres=obtenerNombres_Apellidos($nombres_compuestos);

                            $profesional=usuarioModel::where('identificacion',$cedula)->first();    
                            if(is_null($profesional)){
                              $profesional= new usuarioModel();  
                            }
                            $profesional->rol_id=2;
                            $profesional->nombres=$resultado_nombres['nombres'];
                            $profesional->apellidos=$resultado_nombres['apellidos'];
                            $profesional->identificacion=$cedula;
                            $profesional->password=bcrypt($cedula);
                            $profesional->email=$email;
                            $profesional->estado= 'ACT';
                            $profesional->email_institucional=$email;
                            $profesional->save();

                            if ($value['carrera_id'] != '') {
                                $carrera_id=intval($value['carrera_id']);
                                $direccion=direccion_carreraModel::where('usuario_id',$profesional->id)
                                ->where('carrera_id',$carrera_id)
                                ->first();
                                if(is_null($direccion)){
                                    $direccion=new direccion_carreraModel();
                                    $direccion->carrera_id=$carrera_id;
                                    $direccion->usuario_id=$profesional->id;
                                    $direccion->save();
                                }
                            }


                        }
                    }

                    DB::commit();
                    Session::flash('message', 'Encargados ingresados de manera masiva correctamente, los gerentes pueden iniciar con el ruc de la empresa y la contraseña es el mismo dato');
                    return Redirect('/inicio');

                } catch (Exception $e) {
                    DB::rollBack();
                    Session::flash('message-error', $e->getMessage());
                    return Redirect::back();
                }

            } else {
                Session::flash('message-error', 'No se ha recibido ninguno archivo');
                return Redirect::back();
            }
        }
        Session::flash('message-error', 'No se ha recibido ninguno archivo');
        return Redirect::back();
    }
    function subir_profesionales_post(Request $request){
         ini_set('max_execution_time', 3000);
        $this->validate($request, [
            'profesionales'       => 'required',
        ]);
        if ($request->hasFile('profesionales')) {
            $path      = $request->file('profesionales')->getRealPath();
            $profesionales = \Excel::load($path)->toArray();
            if (count($profesionales)) {
                DB::beginTransaction();
                //uleam
                $institucion_id=1;
                $provincia_id=1;
                try {
                    foreach ($profesionales as $key => $value) {
                       // dd($value);
                        if ($value['cedula'] != '') {

                            

                            $cedula=strval($value['cedula']);
                            $nombres_compuestos=mb_strtoupper($value['graduado']);
                            $email=$value['email'];
                            $telefono=$value['telefono'];
                            $anio=intval($value['ano_de_graduacion']);                            
                            $ciudadx=mb_strtoupper(trim($value['ciudad']));
                            $facultadx=mb_strtoupper(trim($value['facultad_de_graduado']));
                            $titulox=mb_strtoupper(trim($value['carrera_de_graduado']));
                            $email_institucional=$value['emailinstitucional'];
                            $periodo_academico=$value['periodo_academico'];
                            $fecha_de_graduacion=$value['fecha_de_graduacion'];
                            $resultado_nombres=obtenerNombres_Apellidos($nombres_compuestos);


                            //CIUDAD
                            $ciudad=ciudadModel::where('nombre',$ciudadx)->first();
                            if(is_null($ciudad)){
                                $ciudad=new ciudadModel();
                                $ciudad->nombre=$ciudadx;
                                $ciudad->provincia_id=$provincia_id;
                                $ciudad->save();
                            }

                            //FACULTAD
                            $facultad=facultadModel::where('nombre',$facultadx)
                            ->where('ciudad_id',$ciudad->id)
                            ->first();
                            if(is_null($facultad)){
                                $facultad=new facultadModel();
                                $facultad->nombre=$facultadx;
                                $facultad->ciudad_id=$ciudad->id;
                                $facultad->save();
                            }

                            //TITULO
                            $titulo=tituloModel::where('titulo',$titulox)->first();
                            if(is_null($titulo)){
                                $titulo=new tituloModel();
                                $titulo->titulo=$titulox;
                                $titulo->save();
                            }

                            //CARRERA
                            $carrera=carreraModel::where('institucion_id',$institucion_id)
                            ->where('facultad_id',$facultad->id)
                            ->where('titulo_id',$titulo->id)
                            ->first();

                            if(is_null($carrera)){
                                $carrera=new carreraModel();
                                $carrera->institucion_id=$institucion_id;
                                $carrera->facultad_id=$facultad->id;
                                $carrera->titulo_id=$titulo->id;
                                $carrera->save();
                            }

                            $profesional=usuarioModel::where('identificacion',$cedula)->first();    
                            if(is_null($profesional)){
                                $profesional= new usuarioModel();
                                $profesional->rol_id=3;
                            }
                            $profesional->nombres=$resultado_nombres['nombres'];
                            $profesional->apellidos=$resultado_nombres['apellidos'];
                            $profesional->identificacion=$cedula;
                            $profesional->carrera_id=$carrera->id;
                            $profesional->password=bcrypt($cedula);
                            $profesional->email=$email;
                            $profesional->telefono=$telefono;
                            $profesional->estado= 'ACT';
                            $profesional->email_institucional=$email_institucional;
                            $profesional->anio_graduacion=$anio;
                            $profesional->periodo_academico=$periodo_academico;
                            $profesional->fecha_graduacion=$fecha_de_graduacion;
                            $profesional->save();
                            
                        }
                    }

                    DB::commit();
                    Session::flash('message', 'Profesionales ingresados de manera masiva correctamente, los gerentes pueden iniciar con el ruc de la empresa y la contraseña es el mismo dato');
                    return Redirect('/inicio');

                } catch (Exception $e) {
                    DB::rollBack();
                    Session::flash('message-error', $e->getMessage());
                    return Redirect::back();
                }

            } else {
                Session::flash('message-error', 'No se ha recibido ninguno archivo');
                return Redirect::back();
            }
        }
        Session::flash('message-error', 'No se ha recibido ninguno archivo');
        return Redirect::back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'identificacion' => 'required|unique:usuario',
            'email'          => 'required|unique:usuario',
            'rol_id'         => 'required',
        ]);

        if ($request->rol_id == 2 || $request->rol_id == 4) {
            $this->validate($request, [
                'carrera_id' => 'required',
            ]);
        }
        $nuevo_usuario                 = new usuarioModel();
        $nuevo_usuario->apellidos      = $request->apellidos;
        $nuevo_usuario->nombres        = $request->nombres;
        $nuevo_usuario->identificacion = $request->identificacion;
        $nuevo_usuario->email          = $request->email;
        $nuevo_usuario->telefono       = $request->telefono;
        $nuevo_usuario->rol_id         = $request->rol_id;
        $nuevo_usuario->estado         = $request->estado;
        $nuevo_usuario->carrera_id     = $request->carrera_id;
        $nuevo_usuario->estadox        = 1;
        if ($request->rol_id == 2 || $request->rol_id == 4) {
            $nuevo_usuario->carrera_id = $request->carrera_id;
        }
        $nuevo_usuario->password = bcrypt($request->password);
        $nuevo_usuario->save();
        if (Input::hasFile('foto')) {
            $dir      = public_path() . '/usuarios/';
            $foto     = $request->file('foto');
            $fileName = $nuevo_usuario->id . '.' . $foto->getClientOriginalExtension();
            $img      = \Image::make($foto)->resize(80, 80);
            $img->save($dir . $fileName);
            $nuevo_usuario->foto = $fileName;
            $nuevo_usuario->save();
        }

        if($request->rol_id==2&&count($request->carreras)){
            foreach ($request->carreras as $key => $value) {
                    $dir_car=new direccion_carreraModel();
                    $dir_car->carrera_id=$value;
                    $dir_car->usuario_id=$nuevo_usuario->id;
                    $dir_car->save();
            }
        }
        Session::flash('message', 'Se ha ingresado un nuevo usuario');
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function show(usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario          = usuarioModel::find($id);
        $carreras = carreraModel::join('titulo','titulo.id','carrera.titulo_id')
        ->join('facultad','facultad.id','carrera.facultad_id')
        ->join('ciudad','ciudad.id','facultad.ciudad_id')
        ->join('institucion','institucion.id','carrera.institucion_id')
        ->select(DB::raw("CONCAT(institucion.nombre,' - ',ciudad.nombre,' - ',titulo.titulo) as descripcion"),'carrera.id')
        ->pluck('descripcion', 'id');
        $carreras_selected=direccion_carreraModel::where('usuario_id',$usuario->id)->pluck('carrera_id');
        $roles    = rolModel::pluck('descripcion', 'id');
        return view('admin.usuarios.edit', compact('carreras', 'roles', 'usuario','carreras_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'identificacion' => 'required|unique:usuario,identificacion,' . $id,
            // 'email' => 'required|unique:usuario,email,'.$id,
            'rol_id'         => 'required',
        ]);
        if ($request->rol_id == 2 || $request->rol_id == 4) {
            $this->validate($request, [
                'carrera_id' => 'required',
            ]);
        }

        $nuevo_usuario                 = usuarioModel::find($id);
        $nuevo_usuario->apellidos      = $request->apellidos;
        $nuevo_usuario->nombres        = $request->nombres;
        $nuevo_usuario->identificacion = $request->identificacion;
        $nuevo_usuario->email          = $request->email;
        if ($request->rol_id == 2 || $request->rol_id == 4) {
            $nuevo_usuario->carrera_id = $request->carrera_id;
        }
        $nuevo_usuario->rol_id     = $request->rol_id;
        $nuevo_usuario->estado     = $request->estado;
        $nuevo_usuario->carrera_id = $request->carrera_id;
        $nuevo_usuario->telefono   = $request->telefono;

        if (!is_null($request->password)) {
            $nuevo_usuario->password = bcrypt($request->password);
        }
        $nuevo_usuario->save();
        if (Input::hasFile('foto')) {
            $dir = public_path() . '/usuarios/';
            if (!is_null($nuevo_usuario->foto)) {
                File::delete($dir . $nuevo_usuario->foto);
                $nuevo_usuario->foto = null;
            }
            $foto     = $request->file('foto');
            $fileName = $nuevo_usuario->id . '.' . $foto->getClientOriginalExtension();
            $img      = \Image::make($foto)->resize(80, 80);
            $img->save($dir . $fileName);
            $nuevo_usuario->foto = $fileName;
            $nuevo_usuario->save();
        }
        direccion_carreraModel::where('usuario_id',$nuevo_usuario->id)->delete();
        if($request->rol_id==2&&count($request->carreras)){
            foreach ($request->carreras as $key => $value) {
                    $dir_car=new direccion_carreraModel();
                    $dir_car->carrera_id=$value;
                    $dir_car->usuario_id=$nuevo_usuario->id;
                    $dir_car->save();
            }
        }
        Session::flash('message', 'Se ha actualizado correctamente el usuario');
        return redirect()->route('usuarios.index');
    }

      public function actualizar_perfil(Request $request, $id)
    {
        $this->validate($request, [
            'apellidos'      => 'required',
            'nombres'        => 'required',
            'email' => 'required|unique:usuario,email,' . $id,
        ]);
      

        $nuevo_usuario                 = usuarioModel::find($id);
        $nuevo_usuario->apellidos      = $request->apellidos;
        $nuevo_usuario->nombres        = $request->nombres;
        $nuevo_usuario->email          = $request->email;
        $nuevo_usuario->descripcion    = $request->descripcion;
        $nuevo_usuario->save();
        Session::flash('message', 'Se ha actualizado correctamente el perfil');
        return redirect::Back();
    }

    public function actualizar_clave(Request $request, $id)
    {
         $this->validate($request, [
            'password' => 'required',
            'password2' => 'required',
            'password3' => 'required|same:password2',               
            ],
            [
              'password3.same'=>'Confirme correctamente su nueva contraseña',
            ]            
          );
      

            $usuario = usuarioModel::find($id);
            if (Hash::check($request->password, $usuario->password))
            {
                  if ($request->password == $request->password2) 
                    {
                        Session::flash('message-error', 'Las contraseñas nueva Contraseña es igual a la Actual,Por lo Tanto no se Actualizo');   
                    }else{
                     $usuario->password=bcrypt($request->password2);
                     $usuario->save();
                     Session::flash('message','Ha Actualizado su Contraseña correctamente');
                    }
            }else
            {
                Session::flash('message-error','Las Contraseña actual no es la correcta'); 
            }
        return redirect::Back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuario=usuarioModel::find($id);
        if(!is_null($usuario)){
            $rp_encuesta=respuesta_encuestaModel::where('usuario_id',$id)->get();
            if(count($rp_encuesta)){
                foreach ($rp_encuesta as $rpe) {
                    respuestaModel::where('respuesta_encuesta_id',$rpe->respuesta_encuesta_id)->delete();
                    $rpe->delete();
                }
            }
            $usuario->delete();
            Session::flash('message', 'Se ha eliminado correctamente el usuario');
            return 1;
        }
        
   
    }

    public function consultar_usuarios($id)
    {
        return usuarioModel::where('identificacion', $id)
            ->orwhere('apellidos', 'like', '%' . $id . '%')
            ->orwhere('nombres', 'like', '%' . $id . '%')
            ->select(DB::raw("CONCAT(identificacion,' - ',apellidos,' ', nombres) as value"), 'id')
            ->get();
    }


    function subir_profesionales(){
        return view('admin.profesionales.subir_profesionales');
    }
    function subir_encargados(){
        return view('admin.usuarios.subir_encargados');
    }

    

    //método para confirmar cuenta

/*

SET FOREIGN_KEY_CHECKS = 0;
truncate encuesta;
truncate respuesta;
truncate opcion;
truncate respuesta_encuesta;
truncate grupo_preguntas;
truncate pregunta;

SET FOREIGN_KEY_CHECKS = 1;

 */
}

        /*
        SET FOREIGN_KEY_CHECKS = 0;
        TRUNCATE pregunta;
        TRUNCATE grupo_preguntas;
        TRUNCATE opcion;
        SET FOREIGN_KEY_CHECKS = 1;

         */
