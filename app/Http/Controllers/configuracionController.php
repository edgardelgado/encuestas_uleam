<?php

namespace App\Http\Controllers;

use App\configuracionModel;
use Illuminate\Http\Request;
use Session;
use Redirect;
use DB;

class configuracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    } 

    public function index()
    {
        
        return View('admin.configuracion.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.configuracion.crear');
    }
    public function store(Request $request)
    {
          $this->validate($request, [               
           // 'nombre' => 'required|unique:nombre',
            ] );  
        $configuracion = new configuracionModel();
        $configuracion->nombre=$request->nombre;
         $configuracion->valor=$request->valor;
        $configuracion->extra=$request->extra;
      //  dd($nuevo_configuracion);
         $configuracion->save();
        Session::flash('message','Se ha ingresado un nuevo configuracion al Sistema');
        return redirect()->route('configuraciones.index');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $configuracion=configuracionModel::find($id);
        return View('admin.configuracion.edit',compact('configuracion'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carreraModel  $carreraModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [               
         //   'nombre' => 'required',
        ]); 
        $configuracion=configuracionModel::find($id);
         $configuracion->nombre=$request->nombre;
         $configuracion->valor=$request->valor;
        $configuracion->extra=$request->extra;
        //dd($configuracion);
            $configuracion->save();
        Session::flash('message','Se ha actualizado correctamente el configuracion'); 
       return redirect()->route('configuraciones.index');
    }
    public function destroy($id)
    {
        configuracionModel::find($id)->delete();
        Session::flash('message','Se ha eliminado correctamente el configuracion');
        return 1;
    }



    public function configuraciones_ajax()
    {
        $request = request();
        $limit   = $request->input('length');
        $start   = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = configuracionModel::skip($start)->take($limit)->get();
            // $registros = rubroModel::with('tipo_impuesto')->skip($start)->take($limit)->get();
            $totalData     = configuracionModel::count();
            $totalFiltered = $totalData;
        } else {
            $search    = $request->input('search.value');
            $registros = configuracionModel::where(function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%')
                    ->orWhere("valor", 'LIKE', '%' . $search . '%')
                     ->orWhere("extra", 'LIKE', '%' . $search . '%');
            })->skip($start)->take($limit)->get();
            $totalData     = configuracionModel::where(function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%')
                    ->orWhere("valor", 'LIKE', '%' . $search . '%')
                     ->orWhere("extra", 'LIKE', '%' . $search . '%');
            })->count();
            $totalFiltered = $totalData;
        }
        $data = array();

        if (!empty($registros)) {

            foreach ($registros as $key => $value) {

                    $acciones = "<a href=" . route('configuraciones.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar Configuración'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar Configuración'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']     = ' <span>' . $value->id . '</span>';
                $usuario_data['nombre'] = ' <span>' . $value->nombre . '</span>';
                $usuario_data['valor'] = '<span>' . $value->valor . '</span>';
                 $usuario_data['extra'] = '<span>' . $value->extra . '</span>';                
                $usuario_data['accion'] = $acciones;
                $data[]                 = $usuario_data;

                // dd($usuario_data);
            }
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
        ];
        return json_encode($json_data);
    }
}
