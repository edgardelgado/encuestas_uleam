<?php

namespace App\Http\Controllers;

use App\paisModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use View;

class paisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
         return view('admin.pais.index');
    }




     public function paises_ajax()
    {
        $request=request();
        $limit = $request->input('length');
        $start = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = paisModel::skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }else {
            $search = $request->input('search.value');
            $registros = paisModel::where(function($query) use ($search) {
                $query->where("nombre" , 'LIKE' , '%'.$search.'%');                
            })
            ->skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                

                $acciones = "<a href=" . route('paises.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar País'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar País'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']       = '<span>'.$value->id.'</span>'; 
                $usuario_data['nombre'] = '<span>'.$value->nombre.'</span>';             
                $usuario_data['acciones']=$acciones;
                $data[] = $usuario_data;
            }
        }
        $json_data = [
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ];
        return json_encode($json_data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return View::make('admin.pais.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            //'nombre' => 'required|unique:tipo_pagos',
        ]);
        $pais            = new paisModel();
        $pais->nombre = $request->nombre;
        $pais->save();
        Session::flash('message', 'Se ha ingresado un nuevo País al Sistema');
        return redirect()->route('paises.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paisModel  $paisModel
     * @return \Illuminate\Http\Response
     */
    public function show(paisModel $paisModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paisModel  $paisModel
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
    {
        //
       $pais = paisModel::find($id);
        return View('admin.pais.edit', compact('pais'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paisModel  $paisModel
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
        $this->validate($request, [
          //  'nombre' => 'required|unique:rol,nombre,' . $id,
        ]);
        $pais              = paisModel::find($id);
        $pais->nombre = $request->nombre;
      
        $pais->save();
        Session::flash('message', 'Se ha actualizado el País en el Sistema');
        return redirect()->route('paises.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paisModel  $paisModel
     * @return \Illuminate\Http\Response
     */
      public function destroy($id)
    {
        paisModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente el País');
        return 1;
    }
}
