<?php

namespace App\Http\Controllers;

use App\ciudadModel;
use App\provinciaModel;
use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;

class ciudadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


       public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return view('admin.ciudades.index');
    }

         public function ciudades_ajax()
    {
        $request=request();
        $limit = $request->input('length');
        $start = $request->input('start');
        if (empty($request->input('search.value'))) {
            $registros = ciudadModel::skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }else {
            $search = $request->input('search.value');
            $registros = ciudadModel::where(function($query) use ($search) {
                $query->where("nombre" , 'LIKE' , '%'.$search.'%');                
            })->orWhereHas('provincia', function ($query) use ($search) {
                $query->where("nombre", 'LIKE', '%' . $search . '%');
            })
            ->skip($start)->take($limit)->get();
            $totalData =count($registros);
            $totalFiltered = $totalData;
        }
        $data = array();
        if (!empty($registros)) {
            foreach ($registros as $key => $value) {
                

                $acciones = "<a href=" . route('ciudades.edit',$value->id) . " class='btn btn-sm btn-default' data-toggle='tooltip' data-placement='top' title='Editar ciudad'><i class='fa fa-edit'></i></a>
                <a  class='btn btn-sm btn-default js-sweetalert' onClick='SeguroEliminar(" . $value->id . ")' data-toggle='tooltip' data-placement='top' title='Eliminar ciudad'><i class='fa fa-trash-o text-danger'></i></a>";
                $usuario_data['id']       = '<span>'.$value->id.'</span>'; 
                $usuario_data['ciudad'] = '<span>'.$value->nombre.'</span>';  

                 $usuario_data['provincia'] = '<span>'.$value->provincia->nombre.'</span>';             
                $usuario_data['acciones']=$acciones;
                $data[] = $usuario_data;
            }
        }
        $json_data = [
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ];
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $provincia = provinciaModel::pluck('nombre', 'id');
         return view('admin.ciudades.crear', compact('provincia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            //'nombre' => 'required|unique:tipo_pagos',
        ]);
        $ciudad            = new ciudadModel();
        $ciudad->nombre = $request->nombre;
        $ciudad->provincia_id         = $request->provincia_id;
        $ciudad->save();
        Session::flash('message', 'Se ha ingresado un nuevo ciudad  al Sistema');
        return redirect()->route('ciudades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ciudadModel  $ciudadModel
     * @return \Illuminate\Http\Response
     */
    public function show(ciudadModel $ciudadModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ciudadModel  $ciudadModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ciudad  = ciudadModel::find($id);
        $provincia            = provinciaModel::pluck('nombre', 'id');
        return View('admin.ciudades.edit', compact('provincia','ciudad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ciudadModel  $ciudadModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
     {
        $this->validate($request, [
          //  'nombre' => 'required|unique:rol,nombre,' . $id,
        ]);
        $ciudad              = ciudadModel::find($id);
        $ciudad->nombre = $request->nombre;
        $ciudad->provincia_id         = $request->provincia_id;
        $ciudad->save();
        Session::flash('message', 'Se ha actualizado el ciudad al Sistema');
        return redirect()->route('ciudades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ciudadModel  $ciudadModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ciudadModel::find($id)->delete();
        Session::flash('message', 'Se ha eliminado correctamente la ciudad del usuario');
        return 1;
    }
}
