<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rolModel extends Model
{
    protected $table = 'rol';
 	protected $hidden = [];

 	public function usuarioss()
    {
        return $this->hasMany('App\usuarioModel','rol_id');
    }
}
