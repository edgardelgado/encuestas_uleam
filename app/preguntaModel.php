<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class preguntaModel extends Model
{
    protected $table='pregunta';

    //aqui me trae todas las opciones de la pregunta

    public function opciones()
    {
        return $this->hasMany('App\opcionModel','pregunta_id')->orderBy('orden');
    }

    public function carrera()
    {
        return $this->belongsTo('App\carreraModel', 'carrera_id');
    }

}
