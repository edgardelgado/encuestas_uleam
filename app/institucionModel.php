<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class institucionModel extends Model
{


	use SoftDeletes;
    protected $table = 'institucion';


     public function pais()
    {
        return $this->belongsTo('App\paisModel', 'pais_id');
    }
}

