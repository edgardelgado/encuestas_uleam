<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class direccion_carreraModel extends Model
{
       protected $table = 'direccion_carrera';



     public function usuario()
    {
        return $this->belongsTo('App\usuarioModel', 'usuario_id');
    }

    public function carrera()
    {
        return $this->belongsTo('App\carreraModel', 'carrera_id');
    }
}
