<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ciudadModel extends Model
{
   use SoftDeletes;
       protected $table = 'ciudad';



     public function provincia()
    {
        return $this->belongsTo('App\provinciaModel', 'provincia_id');
    }
}
