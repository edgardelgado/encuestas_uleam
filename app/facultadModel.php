<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class facultadModel extends Model
{
	use SoftDeletes;
	
    protected $table='facultad';

    public function ciudad()
    {
        return $this->belongsTo('App\ciudadModel', 'ciudad_id');
    }

}
