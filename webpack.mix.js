const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const vendors = 'resources/assets/vendors/';
const npmvendors='node_modules/'
const resourcesAssets = 'resources/assets/';
const srcCss = resourcesAssets + 'css/';
const srcJs = resourcesAssets + 'js/';

//destination path configuration
const dest = 'public/assets/';
const destFonts = dest + 'fonts/';
const destCss = dest + 'css/';
const destJs = dest + 'js/';
const destImg = dest + 'img/';
const destVendors= dest + 'vendors/';

const paths = {
    'animate': npmvendors + 'animate.css/',
    'morrisjs':npmvendors+ 'morris.js/',
    'raphael':npmvendors+'raphael/',
    'bootstrap': npmvendors + 'bootstrap/dist/',
    'popper': npmvendors + 'popper.js/dist/umd/',
    'jquery' : npmvendors + 'jquery/dist/',
    'fontawesome': npmvendors + 'font-awesome/',
    'chartjs': npmvendors + 'chart.js/dist/',
    'jvectormap': vendors + 'bower-jvectormap/',
    'wenk': npmvendors + 'wenk/dist/',
    'icheck': npmvendors + 'icheck/',
    'bootstrapValidator': npmvendors + 'bootstrapvalidator/dist/',
    'jqueryrepeater': npmvendors + 'jquery.repeater/',
    'select2': npmvendors + 'select2/dist/',
    'daterangepicker': npmvendors + 'bootstrap-daterangepicker/',
    'clockpicker': npmvendors + 'clockpicker/dist/',
    'airdatepicker': npmvendors + 'air-datepicker/dist/',
    'switch': npmvendors + 'bootstrap-switch/dist/',
    "bootstapfile": npmvendors + 'bootstrap-filestyle/',
    'select2BootstrapTheme': npmvendors + 'select2-bootstrap-theme/dist/',
    'moment': npmvendors + 'moment/',
    'colorpicker': npmvendors + 'bootstrap-colorpicker/dist/',
    'notific': npmvendors + 'notific8/dist/',
    'sweetalert2': npmvendors + 'sweetalert2/dist/',
    'fullcalendar': npmvendors + 'fullcalendar/dist/',
    'jqueryui': vendors + 'jquery-ui/',
    'gmaps': npmvendors + 'gmaps/',
    'jasnyBootstrap': npmvendors + 'jasny-bootstrap/dist/',
    'twtrBootstrapWizard': npmvendors + 'twitter-bootstrap-wizard/',
    'datetimepicker': npmvendors + 'eonasdan-bootstrap-datetimepicker/build/',
    'datatables': npmvendors + 'datatables.net/',
    'datatablesbs': npmvendors + 'datatables.net-bs4/',
    'datatablesdt': npmvendors + 'datatables.net-dt/',
    'datatables_buttons': npmvendors + 'datatables.net-buttons/',
    'datatables_buttons_bs': npmvendors + 'datatables.net-buttons-bs4/',
    'datatablescolreorder' : npmvendors + 'datatables.net-colreorder/',
    'datatablescolreorderbs' : npmvendors + 'datatables.net-colreorder-bs4/',
    'datatablesrowreorder' : npmvendors + 'datatables.net-rowreorder/',
    'datatablesrowreorderbs' : npmvendors + 'datatables.net-rowreorder-bs4/',
    'datatablesscrollbs' : npmvendors + 'datatables.net-scroller-bs4/',
    'datatablesscroll' : npmvendors + 'datatables.net-scroller/',
    'datatablesbutton' : npmvendors + 'datatables.net-buttons/',
    'datatablesresponsive': npmvendors + 'datatables.net-responsive/',
    'datatables_fixed_columns': npmvendors + 'datatables.net-fixedcolumns/',
    'bootstraptable': npmvendors + 'bootstrap-table/dist/',
    'datatablesmarkjs': npmvendors + 'datatables.mark.js/dist/',
    'markjs': npmvendors + 'mark.js/dist/',
    'tableExportjqueryplugin': npmvendors + 'tableexport.jquery.plugin/',
    'flotchart': npmvendors + 'flot/',
    'flottooltip': npmvendors + 'jquery.flot.tooltip/js/',
    'flotspline': npmvendors + 'flot-spline/js/',
    'knob': npmvendors + 'jquery-knob/js/',
    'chartist': npmvendors + 'chartist/dist/',
    'pdfmake' : npmvendors + 'pdfmake/build/',
    'sweetalert' : npmvendors + 'sweetalert/dist/',
    'jeditable': npmvendors + 'jquery-jeditable/dist/',
    'bootstrapSlider': npmvendors +'bootstrap-slider/dist/',
    'lobibox': npmvendors +'lobibox/dist/',
    'sortablejs' : npmvendors + 'sortablejs/',
    'lobipanel' : npmvendors + 'lobipanel/',
    'jscrollpane' : npmvendors + 'jscrollpane/',
    'bootstrapSweetalert' : npmvendors + 'bootstrap-sweetalert/dist/',
    'summernote': npmvendors + 'summernote/dist/',
    'datatables_fixed_headers': npmvendors +'datatables.net-fixedheader/',
    'datatables_fixed_headersdt': npmvendors +'datatables.net-fixedheader-dt/',
    'jqueryslimscroll':npmvendors + 'jquery-slimscroll/',
    'toastr': npmvendors + 'toastr/build/',
    'bootstrapfileinput': npmvendors + 'bootstrap-fileinput/',
    'emojionearea': npmvendors + 'emojionearea/dist/'

};

// Copy fonts straight to public
// mix.copy(paths.bootstrap + 'fonts', destFonts);
// mix.copy(paths.bootstrap + 'fonts/glyphicons-halflings-regular.ttf', destFonts + 'bootstrap');
// mix.copy(paths.bootstrap + 'fonts/glyphicons-halflings-regular.woff', destFonts + 'bootstrap');
// mix.copy(paths.bootstrap + 'fonts/glyphicons-halflings-regular.woff2', destFonts + 'bootstrap');
mix.copy(paths.bootstrap + 'js/bootstrap.min.js', srcJs);

//popper js

mix.copy(paths.popper + 'popper.min.js', srcJs);

//popper js

mix.copy(paths.jquery + 'jquery.min.js', srcJs);


// Copy fonts straight to public
mix.copy(paths.fontawesome + 'fonts', destFonts);

// ------------------resource js folders into public js folder--------------
mix.copy(srcJs, destJs);

// ------------------resource css folders into public css folder--------------
mix.copy(srcCss, destCss);


//fontawesome
mix.copy(paths.fontawesome + 'css/font-awesome.min.css', destCss);

// animate
mix.copy(paths.animate + 'animate.min.css',  destVendors + 'animate');


//morris
mix.copy(paths.morrisjs + 'morris.css',  destVendors + 'morrisjs');
mix.copy(paths.morrisjs + 'morris.min.js',  destVendors + 'morrisjs');


//raphael
mix.copy(paths.raphael + 'raphael.min.js',  destVendors + 'raphael');


//  chartjs plugin

mix.copy(paths.chartjs + 'Chart.js',  destVendors + 'chartjs/js');

//  jvector plugin

mix.copy(paths.jvectormap + 'jquery-jvectormap-1.2.2.min.js', destVendors + 'bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js');
mix.copy(paths.jvectormap + 'jquery-jvectormap-world-mill-en.js', destVendors + 'bower-jvectormap/js/jquery-jvectormap-world-mill-en.js');
mix.copy(paths.jvectormap + 'jquery-jvectormap-1.2.2.css', destVendors + 'bower-jvectormap/css/jquery-jvectormap-1.2.2.css');

//   wenk plugin

mix.copy(paths.wenk + 'wenk.min.css',  destVendors + 'wenk');

//   icheck plugin

mix.copy(paths.icheck + 'skins/',  destVendors + 'iCheck/css');
mix.copy(paths.icheck + 'icheck.js',  destVendors + 'iCheck/js');


// bootstrap validator plugin

mix.copy(paths.bootstrapValidator + 'css/bootstrapValidator.min.css',  destVendors + 'bootstrapvalidator/css/');
mix.copy(paths.bootstrapValidator + 'js/bootstrapValidator.min.js',  destVendors + 'bootstrapvalidator/js/');


// seiyria-bootstrap-slider
mix.copy(paths.bootstrapSlider + 'css/bootstrap-slider.min.css',  destVendors + 'bootstrap-slider/css');
mix.copy(paths.bootstrapSlider + 'bootstrap-slider.js',  destVendors + 'bootstrap-slider/js');

//      jquery repeater
mix.copy(paths.jqueryrepeater + 'jquery.repeater.js', destVendors + 'jqueryrepeater/js');


//          select2
mix.copy(paths.select2 + 'css/select2.min.css', destVendors + 'select2/css');
mix.copy(paths.select2 + 'js/select2.js', destVendors + 'select2/js');
mix.copy(paths.select2BootstrapTheme + 'select2-bootstrap.css', destVendors + 'select2/css');

//      daterange picker
mix.copy(paths.daterangepicker + 'daterangepicker.js', destVendors + 'daterangepicker/js');
mix.copy(paths.daterangepicker + 'daterangepicker.css', destVendors + 'daterangepicker/css');

//                   clockpicker
mix.copy(paths.clockpicker + 'bootstrap-clockpicker.min.css', destVendors + 'clockpicker/css');
mix.copy(paths.clockpicker + 'bootstrap-clockpicker.min.js', destVendors + 'clockpicker/js');

//              air datepicker
mix.copy(paths.airdatepicker + 'css/datepicker.min.css', destVendors + 'airdatepicker/css');
mix.copy(paths.airdatepicker + 'js/datepicker.min.js', destVendors + 'airdatepicker/js');
mix.copy(paths.airdatepicker + 'js/i18n/datepicker.en.js', destVendors + 'airdatepicker/js');

//           bootstrap switch
mix.copy(paths.switch+'css/bootstrap3/bootstrap-switch.css', destVendors + 'bootstrap-switch/css');
mix.copy(paths.switch+'js/bootstrap-switch.js', destVendors + 'bootstrap-switch/js');

//           bootstrap file style
mix.copy(paths.bootstapfile + 'src/bootstrap-filestyle.min.js', destVendors + 'bootstapfile/js');

//             moment
mix.copy(paths.moment + 'min/moment.min.js', destVendors + 'moment/js');

// Sweet Alert
// mix.copy(paths.sweetalert + 'sweetalert.css', destVendors + 'sweetalert/css');
mix.copy(paths.sweetalert + 'sweetalert.min.js', destVendors + 'sweetalert/js');
// mix.copy(paths.sweetalert + 'sweetalert-dev.js', destVendors + 'sweetalert/js');

//           bootstrap color picker
mix.copy(paths.colorpicker + 'css/bootstrap-colorpicker.min.css', destVendors + 'colorpicker/css');
mix.copy(paths.colorpicker + 'js/bootstrap-colorpicker.min.js', destVendors + 'colorpicker/js');
mix.copy(paths.colorpicker + 'img/bootstrap-colorpicker', destVendors + 'colorpicker/img/bootstrap-colorpicker');

// notifications
mix.copy(paths.notific + 'notific8.min.css', destVendors + 'notific/css');
mix.copy(paths.notific + 'jquery.notific8.min.js', destVendors + 'notific/js');

// Sweet Alert
mix.copy(paths.sweetalert2 + 'sweetalert2.min.css', destVendors + 'sweetalert2/css');
mix.copy(paths.sweetalert2 + 'sweetalert2.min.js', destVendors + 'sweetalert2/js');

//fullcalendar
mix.copy(paths.fullcalendar + 'fullcalendar.min.css', destVendors + 'fullcalendar/css');
mix.copy(paths.fullcalendar + 'fullcalendar.min.js', destVendors + 'fullcalendar/js');

//  jQuery UI
mix.copy(paths.jqueryui + 'jquery-ui.min.js', destVendors + 'jquery-UI');
mix.copy(paths.jqueryui + 'themes/base/jquery-ui.css', destVendors + 'jquery-UI');
mix.copy(paths.jqueryui + 'themes/base', destVendors + 'jquery-UI');



// gmaps
mix.copy(paths.gmaps + 'gmaps.min.js', destVendors + 'gmaps/js');

//jasny-bootstrap
mix.copy(paths.jasnyBootstrap + 'css/jasny-bootstrap.css', destVendors + 'jasny-bootstrap/css');
mix.copy(paths.jasnyBootstrap + 'js/jasny-bootstrap.js', destVendors + 'jasny-bootstrap/js');

//form wizard page
mix.copy(paths.twtrBootstrapWizard + 'jquery.bootstrap.wizard.js', destVendors + 'bootstrapwizard/js');
mix.copy(paths.twtrBootstrapWizard + 'bootstrap/js/bootstrap.min.js', destVendors + 'bootstrapwizard/js');


// bootstrap-datetimepicker
mix.copy(paths.datetimepicker + 'css/bootstrap-datetimepicker.min.css', destVendors + 'datetimepicker/css');
mix.copy(paths.datetimepicker + 'js/bootstrap-datetimepicker.min.js', destVendors + 'datetimepicker/js');


//datatables
mix.copy(paths.datatablesbs + 'css/dataTables.bootstrap4.css', destVendors + 'datatables/css');
mix.copy(paths.datatablesdt + 'css/jquery.dataTables.css', destVendors + 'datatables/css');
mix.copy(paths.datatables_buttons_bs + 'css/buttons.bootstrap4.min.css', destVendors + 'datatables/css');
// mix.copy(paths.datatables_fixed_columns + 'css/fixedColumns.dataTables.scss', destVendors + 'datatables/css');
mix.copy(paths.datatables_fixed_headersdt + 'css/fixedHeader.dataTables.min.css', destVendors + 'datatables/css');
mix.copy(paths.datatables_fixed_columns + 'js/dataTables.fixedColumns.js', destVendors + 'datatables/js');
mix.copy(paths.datatables_fixed_headers + 'js/dataTables.fixedHeader.js', destVendors + 'datatables/js');
mix.copy(paths.datatablesdt + 'images', destVendors + 'datatables/images');
mix.copy(paths.datatables + 'js/jquery.dataTables.js', destVendors + 'datatables/js');
mix.copy(paths.datatablesbs + 'js/dataTables.bootstrap4.js', destVendors + 'datatables/js');
mix.copy(paths.datatables_buttons + 'js/dataTables.buttons.min.js', destVendors + 'datatables/js');
mix.copy(paths.datatables_buttons + 'js/buttons.print.min.js', destVendors + 'datatables/js');
mix.copy(paths.datatables_buttons + 'js/buttons.html5.min.js', destVendors + 'datatables/js');
mix.copy(paths.datatables_buttons_bs + 'js/buttons.bootstrap4.min.js', destVendors + 'datatables/js');
mix.copy(paths.datatablescolreorder + 'js/dataTables.colReorder.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablescolreorderbs + 'css/colReorder.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesrowreorder + 'js/dataTables.rowReorder.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesrowreorderbs + 'css/rowReorder.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesscrollbs + 'css/scroller.bootstrap4.css',  destVendors + 'datatables/css');
mix.copy(paths.datatablesscroll + 'js/dataTables.scroller.js',  destVendors + 'datatables/js');
mix.copy(paths.datatablesbutton + 'js/buttons.colVis.js',  destVendors + 'datatables/js');
mix.copy(paths.pdfmake + 'pdfmake.js',  destVendors + 'datatables/js');
mix.copy(paths.pdfmake + 'vfs_fonts.js',  destVendors + 'datatables/js');
mix.copy(paths.jeditable + 'jquery.jeditable.min.js', destVendors + 'jeditable/js');





// datatables responsive
// mix.copy(paths.datatablesresponsive + 'css/responsive.dataTables.scss', destVendors + 'datatables/css');
mix.copy(paths.datatablesresponsive + 'js/dataTables.responsive.js', destVendors + 'datatables/js');


//bootstrap-table
mix.copy(paths.bootstraptable + 'bootstrap-table.min.css', destVendors + 'bootstrap-table/css');
mix.copy(paths.bootstraptable + 'bootstrap-table.min.js', destVendors + 'bootstrap-table/js');

//datatables.mark.js
mix.copy(paths.datatablesmarkjs + 'datatables.mark.min.js', destVendors + 'datatablesmark.js/js');
mix.copy(paths.datatablesmarkjs + 'datatables.mark.min.css', destVendors + 'datatablesmark.js/css');

//tableExport.jquery.plugin
mix.copy(paths.tableExportjqueryplugin + 'tableExport.min.js', destVendors + 'tableExportPlugin/');

//mark.js
mix.copy(paths.markjs + 'jquery.mark.js', destVendors + 'markjs/');

// flot charts
mix.copy(paths.flotchart + 'jquery.flot.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.stack.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.crosshair.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.time.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.selection.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.symbol.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.resize.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.categories.js', destVendors + 'flotchart/js');
mix.copy(paths.flotchart + 'jquery.flot.pie.js', destVendors + 'flotchart/js');
mix.copy(paths.flottooltip + 'jquery.flot.tooltip.js', destVendors + 'flot.tooltip/js');
mix.copy(paths.flotspline + 'jquery.flot.spline.min.js', destVendors + 'flotspline/js');

// knob
mix.copy(paths.knob + 'jquery.knob.js', destVendors + 'jquery-knob/js');

//chartist
mix.copy(paths.chartist + 'chartist.min.css', destVendors + 'chartist/css');
mix.copy(paths.chartist + 'chartist.min.js', destVendors + 'chartist/js');

//toastr
mix.copy(paths.lobibox + 'css/lobibox.min.css', destVendors + 'lobibox/css');
mix.copy(paths.lobibox + 'js/lobibox.min.js', destVendors + 'lobibox/js');

// html5sortable
mix.copy(paths.sortablejs + 'Sortable.min.js', destVendors + 'sortablejs/js');

// lobipanel
mix.copy(paths.lobipanel + 'dist/js/lobipanel.min.js', destVendors + 'lobipanel/js');
mix.copy(paths.lobipanel + 'dist/css/lobipanel.min.css', destVendors + 'lobipanel/css');
mix.copy(paths.lobipanel + 'lib/jquery-ui.min.css', destVendors + 'lobipanel/css');
mix.copy(paths.lobipanel + 'lib/jquery-ui.min.js', destVendors + 'lobipanel/js');
mix.copy(paths.lobipanel + 'lib/images', destVendors + 'lobipanel/css/images');

// sweetalert
mix.copy(paths.bootstrapSweetalert + 'sweetalert.min.js', destVendors + 'bootstrapSweetalert/js');
mix.copy(paths.bootstrapSweetalert + 'sweetalert.css', destVendors + 'bootstrapSweetalert/css');

// jscrollpane
mix.copy(paths.jscrollpane + '/script/jquery.jscrollpane.min.js', destVendors + 'jscrollpane/js');
mix.copy(paths.jscrollpane + '/script/jquery.mousewheel.js', destVendors + 'jscrollpane/js');
mix.copy(paths.jscrollpane  + '/style/jquery.jscrollpane.css', destVendors + 'jscrollpane/css');

// summer note
mix.copy(paths.summernote + 'summernote-bs4.css',  destVendors + 'summernote');
mix.copy(paths.summernote + 'summernote-lite.css',  destVendors + 'summernote');
mix.copy(paths.summernote + 'summernote-bs4.js',  destVendors + 'summernote');
mix.copy(paths.summernote + 'summernote-lite.js',  destVendors + 'summernote');
mix.copy(paths.summernote + 'font',  destVendors + 'summernote/font');

// toastr
mix.copy(paths.toastr + 'toastr.css',  destVendors + 'toastr/css');
mix.copy(paths.toastr + 'toastr.min.js',  destVendors + 'toastr/js');

// emojionearea
mix.copy(paths.emojionearea + 'emojionearea.min.css',  destVendors + 'emojionearea/css');
mix.copy(paths.emojionearea + 'emojionearea.min.js',  destVendors + 'emojionearea/js');

// jquery.nicescroll
mix.copy(paths.jqueryslimscroll + 'jquery.slimscroll.min.js', destVendors + 'jqueryslimscroll');


// bootstrap-fileinput
mix.copy(paths.bootstrapfileinput + 'css/fileinput.min.css', destVendors + 'bootstrap-fileinput/css/');
mix.copy(paths.bootstrapfileinput + 'js/fileinput.min.js', destVendors + 'bootstrap-fileinput/js/');
mix.copy(paths.bootstrapfileinput + 'themes/fa/theme.js', destVendors + 'bootstrap-fileinput/js/');
mix.copy(paths.bootstrapfileinput + 'img/loading.gif', destVendors + 'bootstrap-fileinput/img');


// images

// mix.copy(resourcesAssets + 'img', destImg, false);
// mix.copy(resourcesAssets + 'img/authors', destImg + '/authors');




//-------------------mix-styles---------------------------
mix.sass(resourcesAssets + 'sass/bootstrap/bootstrap.scss',destCss +'bootstrap.min.css').options({
    processCssUrls: false
});

mix.styles(
    [

        destCss +  'bootstrap.min.css',
        npmvendors + 'metismenu/dist/metisMenu.css',
        destCss +  'font-awesome.min.css'
    ], destCss + 'app.css');


//-----------------------mix-scripts--------------------


mix.scripts(
    [
        srcJs + 'jquery.min.js',
        srcJs + 'popper.min.js',
        srcJs + 'bootstrap.min.js',
        npmvendors + 'metismenu/dist/metisMenu.js',
        srcJs + 'leftmenu.js'
    ], destJs + 'app.js');
