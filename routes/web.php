<?php
use App\Mail\pruebaMail;
//INICIO
Route::get('/inicio', 'backendController@inicio');
//PANTALLA PRINCIPAL
Route::get('/', function () {
	return view('welcome');
});

Route::get('clave/{id}',function($clave){
	return bcrypt($clave);
});

Route::get('regular_pregunta','encuestaController@regular_pregunta');

// regist

//CERRAR SESION
Route::get('logout', 'LoginController@logout');
//INICIO DE SESION
Route::resource('login', 'LoginController');
Route::post('login', ['as' => 'login', 'uses' => 'LoginController@store']);
Route::post('loginx', ['as' => 'loginx', 'uses' => 'LoginController@login_profesional']);
// registo publico

	Route::get('registro', 'registroController@registro');
	Route::post('registro', 'registroController@registro_publico');


//PRUEBA DE EMAIL
Route::get('prueba_correo', function () {
    return Mail::to("stalin.molina96@hotmail.es", "Pruebas")->send(new pruebaMail());
});

//PARA REENVIAR CORREO DE CONFIRMACION
Route::get('recuperar-cuenta', 'usuarioController@recuperarcuenta');
//POST ENVIA CORREO PARA RECUPERAR CLAVE
Route::post('recuperar_clave', 'usuarioController@recuperar_clave');
//AL DAR CLICK EN EL MAIL DE RECUPERAR CLAVE
Route::get('recuperar-clave/{codigo}', 'LoginController@recuperarcontrasenia');
Route::post('enviar_clave', 'LoginController@enviar_clave');
Route::post('recuperar_clave', 'LoginController@recuperar_clave');
//AL DAR CLICK EN EL MAIL DE RECUPERAR CLAVE
Route::get('recuperar-clave/{codigo}', 'usuarioController@recuperarcontrasenia');
// AL INGRESAR NUEVA CONTRASEÑA (RECUPERAR CONTRASEÑA FORM)
Route::post('actualizar_clave_rec', 'usuarioController@actualizar_clave_rec');

// SUBIR FOTO
Route::post('subirfoto', ['as' => 'subirfoto', 'uses' => 'usuarioController@subirfoto']);
// ELIMINAR FOTO
Route::post('borrarfoto', ['as' => 'borrarfoto', 'uses' => 'usuarioController@borrarfoto']);

//SUBIR ENCUESTAS
Route::get('subir_encuestas', 'usuarioController@subir_encuestas');
Route::post('subir_encuesta_post', 'usuarioController@subir_encuesta_post');

//SUBIR PROFESIONALES
Route::get('subir_profesionales', 'usuarioController@subir_profesionales');
Route::post('subir_profesionales_post', 'usuarioController@subir_profesionales_post');

Route::get('subir_encargados', 'usuarioController@subir_encargados');
Route::post('subir_encargados_post', 'usuarioController@subir_encargados_post');









//RESPUESTA A ENCUESTAS
Route::get('responder_encuesta/{codigo}', 'encuestaController@responder_encuesta');
Route::post('responder_encuesta_post', 'encuestaController@responder_encuesta_post');

//RESULTADOS A ENCUESTAS
Route::get('resultados', 'encuestaController@vista_resultados');
Route::get('resultados/{id}', 'encuestaController@resultados');
Route::get('resultados_general/{id}', 'encuestaController@resultados_general');
Route::get('consultar_resultados_encuestas', 'encuestaController@consultar_resultados');
Route::get('obtener_pdf', 'encuestaController@obtener_pdf');


Route::delete('iniciar_sesion_user/{codigo}','usuarioController@iniciar_sesion_user');






Route::prefix('configuracion')->group(function () {
	//MENUS
	Route::resource('menus', 'menuController');
	//CARRERAS
	Route::resource('carreras', 'carreraController');
	//ROLES Y PERMISOS
	Route::resource('roles', 'rolController');
	//USUARIOS
	Route::resource('usuarios', 'usuarioController');
	//oute::post('registrarse', 'usuarioController@publico');






	//Route::post('registro', 'UsuarioController@registro_publico');
	//FACULTADES
	Route::resource('facultades', 'facultadController');
	//ENCUESTAS
	Route::resource('encuestas', 'encuestaController');
	//GESTIÓN DE PROFESIONALES
	Route::resource('profesionales', 'profesionalController');
	//CONFIGURACION DE ROLES Y PERMISOS
	Route::get('roles/administrar_permisos/{id}', 'rolController@administrar_permisos');
	//ciudades
	Route::resource('ciudades', 'ciudadController');
	//provincias
	Route::resource('provincias', 'provinciaController');
	//pais
	Route::resource('paises', 'paisController');
	//instituciones
	Route::resource('instituciones', 'institucionController');

	//tituloController
	Route::resource('titulos', 'tituloController');

	//configuracionController
	Route::resource('configuraciones','configuracionController');

	
});

	// GUARDAR PERMISOS DE ROLES
Route::post('guardar_permisos', 'rolController@guardar_permisos');


//AJAXS
Route::post('carrera_ajax', 'carreraController@carrera_ajax');
Route::post('usuarios/usuarios_ajax', 'usuarioController@usuarios_ajax');
Route::post('facultades/facultad_ajax', 'facultadController@facultad_ajax');
Route::get('profesional_ajax', 'profesionalController@profesional_ajax');
Route::post('ciudades_ajax', 'ciudadController@ciudades_ajax');
Route::post('provincias_ajax', 'provinciaController@provincias_ajax');
Route::post('paises/paises_ajax', 'paisController@paises_ajax');
Route::post('instituciones_ajax', 'institucionController@instituciones_ajax');

Route::post('titulos_ajax', 'tituloController@titulos_ajax');
Route::post('configuraciones_ajax', 'configuracionController@configuraciones_ajax');

//CONSULTAS

Route::get('consulta_usuario/{id}', 'LoginController@consulta_usuario');
Route::get('consultar_usuarios/{nombre}', 'usuarioController@consultar_usuarios');
Route::get('consultar_provincias/{nombre}', 'ubicacionController@consultar_provincias');
Route::get('consultar_ciudades/{nombre}', 'ubicacionController@consultar_ciudades');
Route::get('consultar_facultades/{nombre}', 'ubicacionController@consultar_facultades');
Route::get('consultar_carreras/{nombre}', 'ubicacionController@consultar_carreras');
Route::get('consultar_ciudad/{id}', 'ubicacionController@consultar_ciudad');
Route::get('consultar_provincia/{id}', 'ubicacionController@consultar_provincia');

Route::get('total_respuestas_ajax', 'encuestaController@total_respuestas_ajax');
Route::get('consultar_resultados_encuestas_cantidad', 'encuestaController@consultar_resultados_encuestas_cantidad');



Route::patch('actualizar_perfil/{id}','usuarioController@actualizar_perfil');
Route::patch('actualizar_clave/{id}','usuarioController@actualizar_clave');



//PERFIL DE USUARIO
Route::get('perfil',function(){
	if(Auth::user()->rol_id==3){
		return view('errors.403');
	}
	return view('perfil');
});


Route::patch('actualizar_preguntas/{id}','encuestaController@actualizar_preguntas');
Route::post('actualizar_pregunta','encuestaController@actualizar_pregunta');
Route::post('agregar_opcion','encuestaController@agregar_opcion');
Route::post('eliminar_dato_pregunta','encuestaController@eliminar_dato_pregunta');





