@extends('layouts.master')
@section('title')
    Página no Autorizada @parent
@stop
@section('content')
<section class="content-header">
    <h1>
        Página no Autorizada
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center animated fadeInDown">
                <i class="fa fa-lock fa-5x"></i>
                <h1>403</h1>
                <div class="error-desc">
                    No tienes permisos para acceder a esta página.<br><br>
                    <i>{{ Request::url() }}</i>
                </div>
            </div> 
        </div>
    </div>
</section>
@stop