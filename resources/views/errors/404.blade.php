<!DOCTYPE html>
<html>

<head>
    <title>404 | Sistema de encuestas ULEAM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
    <!-- global level css -->
    <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- end of global css-->
    <!-- page level styles-->
    <link href="{{ asset('assets/css/pages/error_pages.css')}}" rel="stylesheet">
    <!-- end of page level styles-->
    <style>

    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-10 m-auto">
            <div class="error_content">
                <div class="text-center">
                    <h2>
                        <img src="{{ asset('assets/img/logo_blue.png')}}" alt="Logo">
                    </h2>
                </div>
                <div class="text-center">
                    <div>
                        <div class="error">
                            <span class="folded-corner"></span>
                            <p class="type">404</p>
                            <p class="type-text">Página no encontrada</p>
                            <p class="message">La pagina que estas buscando no se encuentra, Por favor contactanos para reportar en caso de que sea un error. <a href="{{ URL::to('/') }}">Inicio</a><p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="{{ asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<!-- end of global js -->

</body>

</html>

