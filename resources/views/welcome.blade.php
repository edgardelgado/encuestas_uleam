<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	 <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
	<!-- Author Meta -->
	<meta name="author" content="QuoTech">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	 
	<title>Sistema de Encuestas</title>
	<link rel="stylesheet" href="{{ asset('raiz/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('raiz/css/main.css') }}">	
    <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('carga_ajax.css') }}" rel="stylesheet"></link>
    <link rel="stylesheet" href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}">
    
    <link type="text/css" href="{{asset('assets/css/app.css')}}" rel="stylesheet"/>
</head>

<body>
	<header id="header">
		<div class="container">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">					
						<li>
              <a href="{{ url('login') }}">Iniciar Sesión</a>

              <a href="registro">Registrarse</a>

              

            </li>
					</ul>

				</nav>
			</div>
		</div>
	</header>
	<section class="home-banner-area relative">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-center">
				<div class="banner-content col-lg-8 col-md-12">
					<!--
					<h1 class="wow fadeIn" data-wow-duration="4s">Sistemas de Encuestas <br> de la ULEAM</h1>
					<p class="text-white">
						Encuestas realizadas para los graduados de la Universidad Laica Eloy Alfaro de Manabí.
					</p>-->
					<div class="input-wrap" id="buscador">
						<div action="" class="form-box d-flex justify-content-between">
							<input type="text" placeholder="Ingrese Cédula" class="form-control" name="username" id="identificacion" onkeypress="return runScript1(event)">
							<button type="button" class="btn search-btn" onclick="traerdatos();">  <i class="fa fa-search">
                                                                Buscar
                                                            </i></button>
						</div>
					</div>
					<div class="col-md-12">
                            <div id="resultado">
                            </div>
                        </hr>
                    </div>
                    <div class="col-md-10" id="div_clave" style="display: none;">
                                                <div class="col-sm-6" style="width: 520% ! important; left:35% ! important; min-height :10% ! important;">
                                                    <label for="password" style="width: 50% ! important; padding-left: 35% ! important;">
                                                        <strong>
                                                            Clave
                                                        </strong>
                                                    </label>
                                                    <input class="form-control form-control-sm b" id="password" name="password" placeholder="Ingrese su Clave" required="" style="color: #1b1b1d! important;" type="password">
                                                    </input>
                                                </div>
                                                <div class="col-sm-8" style="width: 20% ! important; left: 50% ! important; min-height :10% ! important;     padding: 2% ! important;">
                                                    <button class="btn margin_top btn-primary pull-right btn-block btn-sm b" onclick="iniciar()">
                                                        Iniciar
                                                    </button>
                                                </div>
                                            </div>
				</div>
			</div>
		</div>
		<div class="rocket-img">
			<!--<img src="img/rocket.png" alt="">-->
		</div>

		<div class="modal carga_ajax" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm" role="document">
        <br>
            <br>
                <br>
                    <br>
                        <br>
                            <div class="modal-body">
                                <div id="floatingBarsG">
                                    <div class="blockG" id="rotateG_01">
                                    </div>
                                    <div class="blockG" id="rotateG_02">
                                    </div>
                                    <div class="blockG" id="rotateG_03">
                                    </div>
                                    <div class="blockG" id="rotateG_04">
                                    </div>
                                    <div class="blockG" id="rotateG_05">
                                    </div>
                                    <div class="blockG" id="rotateG_06">
                                    </div>
                                    <div class="blockG" id="rotateG_07">
                                    </div>
                                    <div class="blockG" id="rotateG_08">
                                    </div>
                                </div>
                            </div>
                            <br>
                                <div id="letrero_carga" style="color: white; font-size: 2em; text-align: center; font-style: italic; font-weight: bold;">
                                </div>
                            </br>
                        </br>
                    </br>
                </br>
            </br>
        </br>
    </div>
</div>
	</section>
	

	<script src="{{ asset('raiz/js/vendor/jquery-2.2.4.min.js') }}"></script>	
	<script src="{{ asset('raiz/js/vendor/bootstrap.min.js') }}"></script>	
	<script src="{{ asset('raiz/js/main.js') }}"></script>
	<script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>
	<script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript"></script>
	<script>


	function runScript1(e) {
    //See notes about 'which' and 'key'
    if (e.keyCode == 13) {
    	return traerdatos();
    }
}

	
	function traerdatos() {
          $('#password').val(null);
             var identificacion = $('#identificacion').val();
             if(identificacion==null||identificacion==""){
                return toastr['error']('No ha ingresado cédula'); 
             }
             //var resultado_validacion=cedula(identificacion);
             if(identificacion.length!=10){
              $('#resultado').html(null);
             	return toastr['error']("Cedula ingresada es incorrecta"); 
             }
             carga_ajax("Consultando graduado...");
            $.ajax({
                url: '{{url('consulta_usuario')}}/' +identificacion,
                type: 'GET',
                error: function(error) {
                    console.log(error);
                },
                success: function(res) {
                    $('#resultado').html(res);
                }
            }).always(function(){
              desaparecer_carga_ajax();
            });
    }
    //funcion para validar cedula
    function cedula(num_cedula) {
    var cedula = num_cedula;
    //Preguntamos si la cedula consta de 10 digitos
    if(cedula.length == 10){
        //Obtenemos el digito de la region que sonlos dos primeros digitos
        var digito_region = cedula.substring(0,2);
        //Pregunto si la region existe ecuador se divide en 24 regiones
        if( digito_region >= 1 && digito_region <=24 ){
          // Extraigo el ultimo digito
          var ultimo_digito   = cedula.substring(9,10);
          //Agrupo todos los pares y los sumo
          var pares = parseInt(cedula.substring(1,2)) + parseInt(cedula.substring(3,4)) + parseInt(cedula.substring(5,6)) + parseInt(cedula.substring(7,8));
          //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
          var numero1 = cedula.substring(0,1);
          var numero1 = (numero1 * 2);
          if( numero1 > 9 ){ var numero1 = (numero1 - 9); }
          var numero3 = cedula.substring(2,3);
          var numero3 = (numero3 * 2);
          if( numero3 > 9 ){ var numero3 = (numero3 - 9); }
          var numero5 = cedula.substring(4,5);
          var numero5 = (numero5 * 2);
          if( numero5 > 9 ){ var numero5 = (numero5 - 9); }
          var numero7 = cedula.substring(6,7);
          var numero7 = (numero7 * 2);
          if( numero7 > 9 ){ var numero7 = (numero7 - 9); }
          var numero9 = cedula.substring(8,9);
          var numero9 = (numero9 * 2);
          if( numero9 > 9 ){ var numero9 = (numero9 - 9); }
          var impares = numero1 + numero3 + numero5 + numero7 + numero9;
          //Suma total
          var suma_total = (pares + impares);
          //extraemos el primero digito
          var primer_digito_suma = String(suma_total).substring(0,1);
          //Obtenemos la decena inmediata
          var decena = (parseInt(primer_digito_suma) + 1)  * 10;
          //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
          var digito_validador = decena - suma_total;
          //Si el digito validador es = a 10 toma el valor de 0
          if(digito_validador == 10)
            var digito_validador = 0;
          //Validamos que el digito validador sea igual al de la cedula
          if(digito_validador == ultimo_digito){
           // console.log('la cedula es correcta');
            return 5;
          }else{
          //  console.log('la cedula:' + cedula + ' es incorrecta');
            return 4;
          }
        }else{
          // imprimimos en consola si la region no pertenece
         // console.log('Esta cédula no pertenece a ninguna region');
          return 6;
        }
    }else{
        //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
       // console.log('Esta cédula tiene menos de 10 Digitos');
        return 10;
    }
}


    function carga_ajax(letrero='Cargando ...') {
	 $('#letrero_carga').html(letrero);
	 $('.carga_ajax').modal({
	  show: true,
	  keyboard: false,
	  backdrop: 'static'
	 });
	}
	function desaparecer_carga_ajax() {
	 $('.carga_ajax').modal('hide');
	}

	function enviar_clave(id_usuario,cedula){
  carga_ajax("Solicitando clave de acceso...");

            $.ajax({
                url: '{{url('enviar_clave')}}',
                type: 'POST',
                data: {_token:"{{ csrf_token() }}",id_usuario},
                error: function(error) {
                  $('#password').val(null);
                  $('#div_clave').css('display','none');
                    console.log(error);
                },
                success: function(res) {
                	var email_enviado=$('#email_envio').val();
                    // toastr['success']('Su contraseña se ha enviado al su email');
                     swal({
			                    buttonsStyling: false,
			                    confirmButtonClass: 'btn btn-primary',
			                    type: 'success',
			                    title: 'Código de acceso enviado correctamente!',
			                    html: 'Enviado a : ' + email_enviado
			             }).then(function () {
			             	   swal({
				                title: 'Ingrese el código de acceso',
				                input: 'text',
				                showCancelButton: false,
				                confirmButtonText: 'Aceptar',
				                showLoaderOnConfirm: true,
				                buttonsStyling: false,
				                confirmButtonClass: 'btn btn-success',
				                cancelButtonClass: 'btn btn-danger m-l-10',
				                preConfirm: function (email) {
				                    return new Promise(function (resolve, reject) {
				                    	if(email===''|| email===null){
				                    		swal("Error de ingreso!", "No hay codigo ingresado", "error");
				                    	}else{
				                    		$.ajax({
											    url: '{{url('loginx')}}',
											    type: 'POST',
											    data: {_token:"{{ csrf_token() }}",cedula,password:email},
											    success: function(res) {
												    if(res==2){
												    	swal("Error de ingreso!", "Codigo de Acceso incorrecto", "error");
												    }

												    if(res==1){
												    	location.replace("{{ url('inicio') }}");
												    }
											    }
											});

				                    	}

				                   
				                       
				                        
				                    })
				                },
				               // allowOutsideClick: false
				            }).then(function (email) {
                      /*
				                swal({
				                    buttonsStyling: false,
				                    confirmButtonClass: 'btn btn-primary',
				                    type: 'success',
				                    title: 'Ajax request finished!',
				                    html: 'Submitted email: ' + email
				                })
                        */
				            });			               
			            });
                    

             
                }
            }).always(function(){
              desaparecer_carga_ajax();

            });
}

function iniciar(){
	var identificacion= $("identificacion").val();
	var password= $("password").val();
	    $.ajax({
	    url: '{{url('/')}}',
	    type: 'POST',
	    data: {_token:"{{ csrf_token() }}"},
	    error: function(error) {
	    
	        console.log(error);
	    },
	    success: function(res) {
	    toastr['success']('inico');
	    }
	});
}



    @if(Session::has('message'))
    toastr['success']('{{Session::get('message')}}'); 
    @endif
    @if(Session::has('message-error'))
    toastr['error']('{{Session::get('message-error')}}'); 
    @endif
    @if(count($errors) > 0)
    @php
    $resultado='<ul>';
    foreach ($errors->all() as $error) {
        $resultado.="<li>$error</li>";
    }
    $resultado.='</ul>';
    @endphp
    toastr['error']('<?php echo $resultado; ?>');               
    @endif
    
</script>

</body>

</html>