<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sistemas de Encuesta | Inicio de sesión</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Admin template that can be used to build dashboards for CRM, CMS, etc." />
    <meta name="author" content="Potenza Global Solutions" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- app favicon 
   -->
   <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
    <!-- google fonts -->
  <link rel="stylesheet" type="text/css" href="admin/assets/css/vendors.css" />
    <!-- app style -->
    <link rel="stylesheet" type="text/css" href="admin/assets/css/style.css" />
       
    <!-- plugin stylesheets -->
   <link rel="stylesheet" href="admin/login/bootstrap.4.1.1.min.css">

   <link rel="stylesheet" href="admin/login/bootstrap.3.0.0.min.css">



         <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
                     <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>


   <!-- app style -->
    <!-- app style -->

</head>
<style>
 .log  {
       
     margin-top: -50%;
     margin-left: -5%;
}
 
   body { 
  background: url('raiz/img/header-bg.png') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.panel-default {
opacity: 0.9;
margin-top:30px;
}
.form-group.last { margin-bottom:0px; } 

.tra{
  background: rgba(9, 9, 9, 0.17);
    background-image: initial;
    background-position-x: initial;
    background-position-y: initial;
    background-size: initial;
    background-repeat-x: initial;
    background-repeat-y: initial;
    background-attachment: initial;
    background-origin: initial;
    background-clip: initial;
    background-color: rgba(9, 9, 9, 0.17);
}
</style> 
<body class="bg-white">
     <div class="app">
<div class="app-contant">
    <div class="row  no-gutters ">
        <div class="col-md-4 col-md-offset-7  ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> Inicio de Sesión</div>
                <div class="panel-body">
                  <form action="login" class="smt-3 mt-sm-5" id="authentication" method="post">
                                    <input name="_token" type="hidden" value="<?php echo csrf_token(); ?>">

                                <div class="form-group">
                                     <label for="identificacion" class="col-sm-3 control-label">
                                         Cédula</label>
                                        <div class="col-sm-9">
                                      <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="Ingrese su Cédula" required>
                                       </div>
                                </div> 
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">
                                         Contraseña</label>
                                    <div class="col-sm-9">
                                      <input type="password" class="form-control" id="password" name="password" placeholder=" Contraseña" required>
                                   </div>
                               </div>
                           <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-9">
                                 <div class="checkbox">
                                   <label>
                                      <input type="checkbox"/>
                                      Recuérdame
                                   </label>

                                </div>
                              </div>
                           </div>
                           <div class="form-group last">
                                  <div class="col-sm-offset-3 col-sm-9">
                                  <button type="submit" class="btn btn-success btn-sm">
                                    Iniciar Sesión</button>
                                   <button type="reset" class="btn btn-default btn-sm">
                                Reiniciar</button>
                             </div>
                          </div>
                   </form>
                </div>
                <div class="panel-footer">
                  <!--  No estás registrado?  <a href="#">Registrar aquí</a></div>-->
            </div>
        </div>

         <div class="col-sm-6 col-xxl-9 col-lg-7 o-hidden order-1 order-sm-2">
                                <div class="row align-items-center h-100">
                                    <div class="col-7 mx-auto ">
                                   
                                    </div>
                                </div>
                            </div>

    </div>
</div>
</div>
</div>
<script src="admin/login/jquery-1.11.1.min.js"></script>
<script src="admin/login/jquery.3.2.1.min.js"></script>
<script src="admin/login/bootstrap.4.1.1.min.js"></script>

    <script src="admin/assets/js/vendors.js"></script>    <!-- custom app -->
   <script src="admin/assets/js/app.js"></script>


   

<script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/ui-toastr.js') }}"></script>



        <script>
            @if(Session::has('message'))
    //success
    toastr['success']('{{Session::get('message')}}'); 
    @endif

    @if(Session::has('message-error'))
    //error
    toastr['error']('{{Session::get('message-error')}}'); 
    @endif

    @if(count($errors) > 0)
    //errorees de fornmularios
    <?php
    $resultado='<ul>';
    foreach ($errors->all() as $error) {
        $resultado.="<li>$error</li>";
    }
    $resultado.='</ul>';
    ?>
    toastr['error']('<?php echo $resultado; ?>');               
    @endif
        </script>
</body>


</html>