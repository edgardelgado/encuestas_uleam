@extends('layouts.master')
{{-- Page title --}}
@section('title')
    INICIO
@stop
{{-- page level styles --}}
@section('header_styles')
<!--pagelevel css-->
<link href="{{ asset('assets/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/dashboard1.css')}}" rel="stylesheet">
        <!--end of pagelevel css-->
        @stop
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 align="center">
                SISTEMA DE ENCUESTAS A GRADUADOS DE LA ULEAM
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            @include('alerts.errors')
            @include('alerts.request')
            @include('alerts.success')
            <div class="row">
                @php
            $encuestas=App\encuestaModel::orderBy('orden','ASC')->where('activo',1)
            ->get();
            //dd(Auth::user()->periodo_academico);
            @endphp
                @foreach($encuestas as $encuesta)
                @php

                $direccion=url('responder_encuesta/'.encrypt($encuesta->id));
                $respondida=estado_respuesta_encuesta($encuesta->id);
                //1 ya fue respondida , 2 aun no esta habilitada, 3 habilitada y sin responder
                $color='danger';
                $icono='fa-edit';
                if($respondida==1){
                    $color='success';
                    $direccion="#";
                    $icono='fa-check-square-o';
                }

                if($respondida==2){
                    $direccion="#";
                    $icono='fa-clock-o';
                }

                @endphp

                @if($respondida!=-1)
                <div class="col-xl-4 col-md-4">
                    <a href="{{ $direccion }}" class="wenk-area" data-wenk="aaaaaa" data-wenk-pos="bottom">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon pull-left">
                                <i class="fa {{ $icono }} text-{{ $color }}">
                                </i>
                            </div>
                            <div class="text-right">
                                <h3>
                                    <b>
                                    </b>
                                </h3>
                                @php
                            $dato=explode('|', $encuesta->nombre)
                            @endphp
                                <p>
                                    @foreach($dato as $da)
                                {{ $da }}
                                    <br>
                                        @endforeach
                                    </br>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                @endif

                @endforeach
            </div>
        </section>
        <!-- /.content -->
        @stop
@section('footer_scripts')
        <!--morris chart-->
        <script src="{{ asset('assets/vendors/raphael/raphael.min.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/vendors/morrisjs/morris.min.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/plugins/jquery.flot.spline.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/pages/dashboard1.js')}}" type="text/javascript">
        </script>
        <!-- end of page level js -->
        @stop
    </link>
</link>