<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')

@endsection
@section('title')
    Editar Título @parent
@stop
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar Título
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar Título
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                                                @include('alerts.request')
                                                {!! Form::model($titulo, array('method' => 'PATCH', 'route' => array('titulos.update', $titulo->id))) !!}
                                                <div class="row">
                                                      <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            Nombre del Título
                                                        </label>
                                                        {!! Form::Text('titulo',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre del Título']) !!}
                                                    </div>

                                                    <div class="col-md-6 form-group mb-3">
                                                     <label for="descripcion">
                                                       Profesión
                                                    </label>
                                                     {!! Form::Text('profesion',null,['class'=>'form-control','placeholder'=>'Ingrese una Profesión']) !!}
                                                    </div>


                                                     <div class="col-md-6 form-group mb-3">
                                                     <label for="descripcion">
                                                       Ícono
                                                    </label>
                                                     {!! Form::Text('icono',null,['class'=>'form-control','placeholder'=>'Ingrese un Ícono']) !!}
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                            
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
@endsection
