@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Inicio
@stop
{{-- page level styles --}}
@section('header_styles')
<!--pagelevel css-->
<link href="{{ asset('assets/vendors/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages/dashboard1.css')}}" rel="stylesheet">
        <!--end of pagelevel css-->
        @stop
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-xl-4">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon pull-left">
                            <i class="fa fa-users text-warning">
                            </i>
                        </div>
                        <div class="text-right">

                            <h3><b>{{ $total_profesionales }}</b></h3>
                            <p>Total profesionales</p>

                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon pull-left">
                            <i class="fa fa-opencart text-success">
                            </i>
                        </div>
                        <div class="text-right">

                            <h3><b id="widget_count3">{{ $total_carreras }}</b></h3>
                            <p>Total de Carreras</p>

                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 ">
                    <div class="widget-bg-color-icon card-box">
                        <div class="bg-icon pull-left">

                            <i class="fa fa-thumbs-o-up text-info"></i>
                        </div>
                        <div class="text-right">
                            <h3><b>{{ $encuestas_respondida }}</b></h3>
                            <p>Encuestas respondidas</p>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card ">
                        <div class="card-header">
                            <h3 class="card-title">
                                Respuestas
                            </h3>
                        </div>
                        <div class="card-body">
                            <div id="sales_chart">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class=" card widget-timeline">
                        <div class="card-header">
                            <h3 class="card-title">
                                Ultimos profesionales respondiendo encuestas
                            </h3>
                        </div>
                        <div class="card-body">
                            <ul>
                                @php
                                $respondidos=App\respuesta_encuestaModel::join('usuario','usuario.id','respuesta_encuesta.usuario_id')
                                ->join('encuesta','encuesta.id','respuesta_encuesta.encuesta_id')
                                ->Orderby('respuesta_encuesta.id','DESC')
                                ->select('encuesta.nombre as encuesta','usuario.nombres as profesional','respuesta_encuesta.created_at')
                                ->take(5)->get();
                                @endphp

                                @foreach($respondidos as $respuesta)
                                <li>

                                    <img src="{{ asset('assets/img/authors/avatar.jpg')}}" alt="" class="pull-left">
                                    <div class="timeline">
                                        <span><strong>{{ $respuesta->profesional }}</strong> Respondió {{$respuesta->encuesta }}
                                            <br><a href="javascript:void(0)">{{ $respuesta->created_at }}</a>
                                        </span>
                                    </div>

                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="row">
                <div class="col-sm-12">
                    <div class="card product-details">
                        <div class="card-header">
                            <h3 class="card-title">
                                Detalles de encuestas
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped text-center" id="product-details">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        #
                                                    </th>
                                                    <th class="text-center">
                                                        <strong>
                                                            Id
                                                        </strong>
                                                    </th>
                                                    <th class="text-center">
                                                        <strong>
                                                            Encuesta
                                                        </strong>
                                                    </th>
                                                    <th class="text-center">
                                                        <strong>
                                                            Preguntas
                                                        </strong>
                                                    </th>
                                                    <th class="text-center">
                                                        <strong>
                                                            Profesionales que han respondido
                                                        </strong>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        7897898
                                                    </td>
                                                    <td>
                                                        Becky Barnes
                                                    </td>
                                                    <td>
                                                        $340
                                                    </td>
                                                    <td class="text-warning">
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-half-o">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-o">
                                                        </i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        7897898
                                                    </td>
                                                    <td>
                                                        Jayden Hunter
                                                    </td>
                                                    <td>
                                                        $340
                                                    </td>
                                                    <td class="text-warning">
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-o">
                                                        </i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        7897898
                                                    </td>
                                                    <td>
                                                        Wallace boyd
                                                    </td>
                                                    <td>
                                                        $340
                                                    </td>
                                                    <td class="text-warning">
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-half-o">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-o">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-o">
                                                        </i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>
                                                        7897898
                                                    </td>
                                                    <td>
                                                        Randy Spencer
                                                    </td>
                                                    <td>
                                                        $340
                                                    </td>
                                                    <td class="text-warning">
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star">
                                                        </i>
                                                        <i aria-hidden="true" class="fa fa-star-half-o">
                                                        </i>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-12 p-0">
                                    <span id="product_status">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </section>
        <!-- /.content -->
        @stop
@section('footer_scripts')
        <!--morris chart-->
        <script src="{{ asset('assets/vendors/raphael/raphael.min.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/vendors/morrisjs/morris.min.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/plugins/jquery.flot.spline.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/pages/dashboard1.js')}}" type="text/javascript">
        </script>
        <!-- end of page level js -->
        @stop
