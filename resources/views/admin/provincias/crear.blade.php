<?php Autorizar(Request::path()); ?> 

@extends('layouts.master')
@section('header_styles')        
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
@endsection
@section('titulo','Crear Provincia')
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
        <div class="breadcrumb">
            <h1>
                Crear Provincia
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo Provincia
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'provincias.store', 'method' => 'POST']) !!}
                        <div class="row">
                            
                         
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombre de la Provincia
                                </label>
                                {!! Form::Text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre']) !!}
                            </div>
                             <div class="col-md-6 form-group mb-3">
                             <label for="descripcion">
                               Descripción
                            </label>
                             {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una Descripción']) !!}
                            </div>
                     
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
@endsection
 