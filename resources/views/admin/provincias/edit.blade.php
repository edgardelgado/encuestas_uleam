<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')

@endsection
@section('title')
    Editar Provincia @parent
@stop
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar Provincia
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar Provincia
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                                                @include('alerts.request')
                                                {!! Form::model($provincia, array('method' => 'PATCH', 'route' => array('provincias.update', $provincia->id))) !!}
                                                <div class="row">
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            Nombre
                                                        </label>
                                                        {!! Form::Text('nombre',null,['class'=>'form-control']) !!}
                                                    </div>

                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="descripcion">
                                                            Descripción
                                                        </label>
                                                        {!! Form::Text('descripcion',null,['class'=>'form-control']) !!}
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                            
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
@endsection
