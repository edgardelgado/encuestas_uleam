<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
        @endsection
@section('titulo','Crear Facultad')
@section('content')
        <div class="breadcrumb">
            <h1>
                Crear Facultad
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'facultades.store', 'method' => 'POST']) !!}
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombre
                                </label>
                                {!! Form::Text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="Ciudad">
                                    Ciudad
                                </label>
                                <select class="search-field" id="select_ciudad" name="ciudad_id" placeholder='Ingrese el nombre de la ciudad' >
                                </select>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
        <script src="{{asset('assets/selective/selectize.js')}}">
        </script>
        <script>
            $('#select_ciudad').selectize({
    valueField: 'id', 
    labelField: 'value',  
    searchField: 'value', 
    options: [],
    //create: true,
    render: {
        option: function(item, escape) {
            return '<div>' +             
                '<span class="description">' + escape(item.value) + '</span>' +               
            '</div>';
        }
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item);
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{url('consultar_ciudades')}}/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.slice(0, 10));
            }
        });
    }
    });
        </script>
        @endsection
 