<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<!-- page vendors -->
<link href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
            <link href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet" type="text/css">
                <link href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
                    <link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
                    <!-- end of page vendors -->
                    <!--page level css -->
                    <link href="{{asset('assets/css/pages/addnew_user.css')}}" rel="stylesheet" type="text/css">
                        <link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
                            @endsection
@section('titulo','Editar Facultad')
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar Facultad
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar Facultad
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                            @include('alerts.request')
                            {!! Form::model($facultad, array('method' => 'PATCH', 'route' => array('facultades.update', $facultad->id))) !!}
                                                <div class="row">
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            nombres
                                                        </label>
                                                        {!! Form::Text('nombre',null,['class'=>'form-control']) !!}
                                                    </div>
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            Ciudad
                                                        </label>
                                                        <select class="search-field" id="select_ciudad" name="ciudad_id">
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="myModal" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal" type="button">
                                                                    ×
                                                                </button>
                                                                <h4 class="modal-title">
                                                                    User Edit
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Account Edited Successfully.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" data-dismiss="modal" type="button">
                                                                    Ok
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
                            <script src="{{asset('assets/selective/selectize.js')}}">
                            </script>
                            <!-- page vendors -->
                            <script src="{{asset('assets/vendors/moment/js/moment.min.js')}}">
                            </script>
                            <script src="{{asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript">
                            </script>
                            <!-- end of page vendors -->
                            <!-- begining of page level js -->
                            <script src="{{asset('assets/js/pages/addnew_user.js')}}" type="text/javascript">
                            </script>
                            <script>
                                $('#select_ciudad').selectize({
    valueField: 'id', 
    labelField: 'value',  
    searchField: 'value', 
    options: [],
    //create: true,
    render: {
        option: function(item, escape) {
            return '<div>' +             
                '<span class="description">' + escape(item.value) + '</span>' +               
            '</div>';
        }
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item);
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{url('consultar_ciudades')}}/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.slice(0, 10));
            }
        });
    } 
           ,
 onInitialize: function(){
      this.addOption([{"value":"{{$ciudad_selected->value }}","id":"{{$ciudad_selected->id }}"}]);
      this.setValue(["{{ $ciudad_selected->id }}"]);
    } 
});
                            </script>
                            @endsection
