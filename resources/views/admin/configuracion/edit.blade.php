<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')

@endsection
@section('title')
    Editar Configuración @parent
@stop
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar Configuración
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar Configuración
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                                                @include('alerts.request')
                                                {!! Form::model($configuracion, array('method' => 'PATCH', 'route' => array('configuraciones.update', $configuracion->id))) !!}
                                                <div class="row">
                                                        <div class="col-md-6 form-group mb-3">
                                                            <label for="nombre">
                                                                Nombre
                                                            </label>
                                                            {!! Form::Text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese nombres de la Configuración']) !!}
                                                        </div>

                                                        <div class="col-md-6 form-group mb-3">
                                                         <label for="Valor">
                                                           Valor
                                                        </label>
                                                         {!! Form::Text('valor',null,['class'=>'form-control','placeholder'=>'Ingrese valor de la Configuración']) !!}
                                                        </div>


                                                         <div class="col-md-6 form-group mb-3">
                                                         <label for="valor">
                                                           Extra
                                                        </label>
                                                         {!! Form::Text('extra',null,['class'=>'form-control','placeholder'=>'Ingrese extra de la Configuración']) !!}
                                                        </div>

                                                    
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                            
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
@endsection
