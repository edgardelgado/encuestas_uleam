<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<!-- page vendors -->
    <link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
                            @endsection
@section('title')
    Editar ciudad @parent
@stop
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar ciudad
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar ciudad
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                            @include('alerts.request')
                            {!! Form::model($ciudad, array('method' => 'PATCH', 'route' => array('ciudades.update', $ciudad->id))) !!}
                                                <div class="row">
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            Nombre
                                                        </label>
                                                        {!! Form::Text('nombre',null,['class'=>'form-control']) !!}
                                                    </div>
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="provincia_id">
                                                            Provincia
                                                        </label><!--
                                                        <select class="search-field" id="select_pais" name="pais_id">
                                                        </select>-->
                                                         {!! Form::select('provincia_id',$provincia,null,['class'=>'form-control select2']) !!}
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                            
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
                        <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function()  {
          $(".select2").select2({width: "100%"});
        });
    </script>
                           
                            @endsection
