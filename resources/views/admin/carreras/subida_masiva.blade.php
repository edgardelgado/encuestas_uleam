<?php Autorizar(Request::path()); ?>
@extends('layouts.principal')
@section('titulo')
Subir Carreras de Manera Masiva
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
<div class="single" id="titlebar">
    <div class="container">
        <div class="sixteen columns">
            <h2>
                Subir Carreras de Manera Masiva
            </h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>
                        Estás aquí:
                    </li>
                    <li>
                        <a href="#">
                            Master
                        </a>
                    </li>
                    <li>
                        Gestión de carreras
                    </li>
                    <li>
                        Subir Carreras de Manera Masiva
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="sixteen columns">
        {!! Form::open(['url'=>'subir_carreras', 'method' => 'POST','files'=>'true']) !!}
        <div class="submit-page">
            <div class="notification notice closeable margin-bottom-40">
                <p>
                    <span>
                        Aviso:
                    </span>
                    Debe seleccionar el excel
                </p>
            </div>
            <div class="form">
                <h5>
                    Carreras
                    <span>
                        (Excel)
                    </span>
                </h5>
                <input id="file-4" name="carreras" type="file">
                    <a href="{{ url('descargar_formato/Carreras.xlsx') }}">
                        DESCARGAR FORMATO CORRECTO
                    </a>
                </input>
            </div>
            <div class="divider margin-top-0 padding-reset">
            </div>
            <button class="button big margin-top-5" type="submit">
                SUBIR CARRERAS A LA PLATAFORMA
                <i class="fa fa-save">
                </i>
            </button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="margin-top-60">
</div>
@stop

@section('script')
<link href="{{ asset('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
<script src="{{ asset('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js') }}">
</script>
<script>
    $(document).ready(function(){
       $('#file-4').fileinput({
          language:"es",
          'theme': 'explorer',
            showUpload:false           
       });
 });
</script>
@stop
