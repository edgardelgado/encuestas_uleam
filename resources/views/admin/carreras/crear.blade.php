<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link href="{{asset('assets/vendors/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('title')
 Crear Carrera @parent
@stop
@section('content')
        <div class="breadcrumb">
            <h1>
                Crear Carrera
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'carreras.store', 'method' => 'POST']) !!}
                          <div class="row">
                                <div class="col-md-6 form-group mb-3">
                                    <label for="email">
                                        Institución
                                    </label>
                                    {!!Form::select('institucion_id', $instituciones,null, ['class' => 'select2','placeholder'=>'Seleccione...'])!!}
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="nombre">
                                        Facultad
                                    </label>
                                    <select class="search-field" id="select_facultad" name="facultad_id">
                                    </select>
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="apellidos">
                                        Titulo
                                    </label>
                                    {!!Form::select('titulo_id', $titulos,null, ['class' => 'select2','placeholder'=>'Seleccione...'])!!}
                                </div>
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="nombre">
                                        Encargados
                                    </label>
                                   {!!Form::select('encargados[]', $encargados,null, ['class' => 'select2','multiple'])!!}
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary" type="submit">
                                        Registrar
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
  <script src="{{asset('assets/selective/selectize.js')}}">
        </script>
        <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript">
        </script>
        <script type="text/javascript">
            $(".select2").select2({width: "100%"});
        </script>
        <script>
            $('#select_facultad').selectize({
    valueField: 'id', 
    labelField: 'value',  
    searchField: 'value', 
    options: [],
    //create: true,
    render: {
        option: function(item, escape) {
            return '<div>' +             
                '<span class="description">' + escape(item.value) + '</span>' +               
            '</div>';
        }
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item);
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{url('consultar_facultades')}}/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.slice(0, 10));
            }
        });
    }
 });
        </script>
        @endsection
