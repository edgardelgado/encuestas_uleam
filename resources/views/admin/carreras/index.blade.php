<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Carreras @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet">
            <link href="{{ asset('assets/css/pages/datatables.css')}}" rel="stylesheet">
                <link href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}" rel="stylesheet" type="text/css">
                    <link href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}" rel="stylesheet" type="text/css">
                        <script src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}" type="text/javascript">
                        </script>
                        <!--end of page vendors -->
                        @stop
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <h1>
                                Gestión de Carreras
                            </h1>
                        </section>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card filterable">
                                        <div class="card-header clearfix">
                                            <h3 class="card-title pull-left m-t-6">
                                                <i class="ti-control-shuffle">
                                                </i>
                                                Carreras
                                                <!--Add / Remove rows-->
                                            </h3>
                                            <div class="pull-right">
                                                <a class="btn btn-primary btn-sm" href="{{ URL::route('carreras.create') }}" type="button">
                                                    Nuevo
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table cellspacing="0" class="table" id="tabla" width="100%">
                                                    <thead>
                                                         <tr>
                                                            <th>
                                                                ID:
                                                            </th>
                                                            <th>
                                                                INSTITUCION:
                                                            </th>                                                         
                                                            <th>
                                                                FACULTAD:
                                                            </th>
                                                            <th>
                                                                TITULO DE PROFESIONAL:
                                                            </th>
                                                            <th width="10%">
                                                                Opciones
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- fourth datatable end -->
                            <!-- fifth row datatable -->
                            <!-- fifth row datatable end -->
                        </section>
                        <!-- /.content -->
                        @stop




@section('footer_scripts')
                        <!--   page level js -->
                        <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap4.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/js/pages/advanced_tables.js')}}" type="text/javascript">
                        </script>
                        <!-- end of page level js -->
                        <script>
                            /*
    $(function () {
         $("[rel='tooltip']").tooltip();
    })
    function SeguroEliminar(id)
   {
    $.colorbox({width:"auto",height:"auto", open:true, href:"seguro_eliminar/carreraarticulos/"+id});
   }
*/

 $(document).ready(function(){});


  function SeguroEliminar($id)
    {
      alertify.confirm('Confirma', '¿Desea eliminar esta carrera?', 

        function()
        {

            $.ajax({
               url: "{{ url(request()->route()->getPrefix().'/carreras') }}/"+$id,
               data: { "_token": "{{ csrf_token() }}" },
              type: 'DELETE',
            })
            .done(function() {
              location.reload();
              desaparecer_carga_ajax();
            })
             .fail(function() {
              desaparecer_carga_ajax();
            })
             .always(function() {
              desaparecer_carga_ajax();
            });
              },
          function(){});


        }

  


       var tabla = $('#tabla').DataTable({
            responsive : false,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todas"]],
            language: {
                "emptyTable":     "0 Carreras encontrados",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ Carreras",
                "infoEmpty":      "Mostrando 0 a 0 de 0 Carreras",
                "infoFiltered":   "(filtrado de _MAX_ total Carreras)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ Carreras",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar: ",
                "zeroRecords":    "No se encontraron Carreras coincidentes",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Atrás"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            },
            // "order": ([ 0, 'desc' ]),
            dom: 'flrBtip',
            buttons: [
                'print','copy', 'excel', 'pdf'
            ],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('carrera_ajax') }}",
                "dataType": "json",
                "type": "post",
                "data":{ _token: "{{ csrf_token() }}" }
            },
            "columns": [
                { "data": "id" },
                { "data": "institucion" },                  
                { "data": "facultad" },              
                { "data": "titulo" },
                 { "data": "accion","orderable":false }
               
            ]
        });
                        </script>
                        @stop
                  