<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
    <link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
        <link href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}" rel="stylesheet" type="text/css">
        @endsection
@section('title')
    Editar encuesta @parent
@stop
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
        <div class="breadcrumb">
            <h1>
                Editar encuesta
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header">
                            <h3>Datos principales</h3>
                    </div>
                    <div class="card-body">
                         {!! Form::model($encuesta, array('method' => 'PATCH', 'route' => array('encuestas.update', $encuesta->id))) !!}
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="nombre">
                                    Nombre de encuesta
                                </label>
                                {!! Form::Text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre para la encuesta']) !!}
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="nombre">
                                   Años despues de graduación para habilitar
                                </label>
                                {!! Form::Number('anios',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                @php
                                $periodos=App\usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
                                $periodos_selected=(is_null($encuesta->periodos))?[]:explode('|', $encuesta->periodos);
                                @endphp
                                     <label>Periodos Académico</label>
                                    {!!Form::select('periodos[]', $periodos,$periodos_selected, ['class' => 'select2','id'=>'periodos','multiple'])!!}
                            </div>
                            <div class="col-md-12 form-group mb-3">
                                <a href="{{ asset('graduados.xlsx') }}">
                                    Descargar formato
                                </a>
                                <br>
                                    <label for="nombre">
                                        Excel de encuesta a subir
                                    </label>
                                    <input name="encuesta" type="file">
                                    </input>
                                </br>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- DATOS DE PREGUNTAS -->
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header">
                        <h3>Datos de Preguntas</h3>
                        <a class="btn btn-primary" style="color: white;" onclick="agregar_nueva_pregunta();">Agregar Pregunta</a>
                    </div>
                    <div class="card-body">
                        {!! Form::open( array('method' => 'PATCH', 'url' => array('actualizar_preguntas', $encuesta->id))) !!}
                        <div class="card mb-4">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-4">
                                       <h3>#</h3>
                                    </div>
                                    <div class="col-md-8">                                    
                                        <h3>DETALLE</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($preguntas as $pregunta)
                        @if($pregunta->carrera!=null)
                        @php
                        if($pregunta->carrera->facultad){
                            $ciudad='';
                            if($pregunta->carrera->facultad->ciudad){
                                $ciudad=$pregunta->carrera->facultad->ciudad->nombre;
                            }
                          $qqq  = $pregunta->carrera->facultad->nombre .' - '.$ciudad;  
                        }
                        @endphp
                        {{ $pregunta->carrera->titulo->titulo }}
                        <br>
                         {{ $qqq }}
                        @endif
                        <div class="card mb-4" id="pregunta{{ $pregunta->id }}">
                            <div class="card-header" style="background: #acd41d;">
                                <div class="row">
                                    <div class="col-md-4">
                                       <input type="number" class="form-control" value="{{ intval($pregunta->order) }}" name="pregunta-order-{{ $pregunta->id }}"> 
                                    </div>
                                    <div class="col-md-7">                                    
                                        <input type="text" class="form-control" value="{{ $pregunta->pregunta }}" name="pregunta-pregunta-{{ $pregunta->id }}">
                                    </div>

                                    <div class="col-md-1">  
                                        <a class="btn btn-primary" style="color: white;" onclick="actualizar_texto_pregunta({{ $pregunta->id }})"><i class="fa fa-upload"></i></a>                                  
                                        <a class="btn btn-danger" style="color: white;" onclick="eliminar_dato(1,'pregunta{{ $pregunta->id }}',{{ $pregunta->id }})"><i class="fa fa-trash"></i></a>
                                         <a class="btn btn-success" style="color: white;" onclick="agregar_nueva_opcion({{ $pregunta->id }},'{{ intval($pregunta->order) }} {{ $pregunta->pregunta }}')"><i class="fa fa-plus-circle"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body"> 

                           
                                        @foreach($pregunta->opciones as $op)
                                        <div class="row" style="margin-top: 1%;" id="op{{ $op->id }}">
                                            <div class="col-md-4">
                                               <input type="number" class="form-control" value="{{ intval($op->orden) }}" name="opcion-orden-{{ $op->id }}"> 
                                            </div>
                                            <div class="col-md-6">                                    
                                                <input type="text" class="form-control" value="{{ $op->opcion }}" name="opcion-opcion-{{ $op->id }}">
                                            </div>
                                             <div class="col-md-2">                                    
                                                <a class="btn btn-danger" style="color: white;" onclick="eliminar_dato(2,'op{{ $op->id }}',{{ $op->id }})"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </div>
                                        @endforeach
                                             
                            </div>
                        </div>
                        @endforeach  
                        <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                   <i class="fa fa-save"></i> Actualizar Datos de Preguntas
                                </button>
                            </div>
                        {!! Form::close() !!}                     
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade launch-pricing-modal" id="modal_nueva_opcion" tabindex="-1" role="dialog"  aria-hidden="true">
               <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="verifyModalContent2_title"></h5>
                        </div>                               
                        <div class="modal-body">                          
                        <div class="form-group" id="texto_pregunta_div">
                            <p id="texto_pregunta"></p>                         
                        </div>

                 
                        <input type="hidden" id="pregunta_id">
                        <div class="form-group">
                            <label>#</label>
                            <input type="number" id="order_x" value="0" class="form-control">                            
                        </div>
                         <div class="form-group">
                            <label id="ooo"></label>
                            <input type="text" id="opcion_x" class="form-control">                            
                        </div>
                        <div class="form-group" id="grupo_pregunta_div">
                            <label>GRUPO DE PREGUNTA</label>
                            {!! Form::select('grupo_pregunta',$grupo_preguntas,null,['class'=>'form-control select2','id'=>'grupo_pregunta_id']) !!}
                            <br>
                            <label>TITULO DE RESPUESTA</label>
                            <input type="text" class="form-control" id="titulo_respuesta" placeholder="Ingrese un titulo de la respuesta">
                        </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                            <button type="button" class="btn btn-primary" onclick="agregar_opcion()">Agregar Opción</button>
                        </div>
                    </div>
                </div>
        </div>


        @endsection
@section('footer_scripts')
        <script src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}">
        </script>
        <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/pages/ui-toastr.js') }}">
        </script>
        <script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript">
        </script>

        <script type="text/javascript">
        var tipo_ingreso=0;
               $(document).ready(function() {
                    $(".select2").select2({width: "100%"});
                });
            function agregar_opcion(){
                var encuesta_id={{ $encuesta->id }};
                var pregunta_id=$('#pregunta_id').val();
                var titulo_respuesta=$('#titulo_respuesta').val();
                var grupo_pregunta_id=$('#grupo_pregunta_id').val();
                var orden=$('#order_x').val();
                var opcion=$('#opcion_x').val();

                if(tipo_ingreso==1&pregunta_id==''||pregunta_id==null){
                     return toastr['error']('No ha seleccionado la pregunta adonde pertenecerá la opción');
                }

                if(orden==''||orden==null||orden==0){
                     return toastr['error']('No ha ingresado un orden válido');
                }

                if(opcion==''||opcion==null){
                     return toastr['error']('No ha ingresado la opción');
                }

                 if(tipo_ingreso==2&titulo_respuesta==''||titulo_respuesta==null){
                     return toastr['error']('No ha ingresado un titulo a la respuesta de la pregunta a registrar');
                }

                carga_ajax('Registrando Opción...');

                $.ajax({
                url: '{{url('agregar_opcion')}}',
                type: 'POST',
                data:{'_token':'{{ csrf_token() }}',pregunta_id,orden,opcion,tipo_ingreso,encuesta_id,grupo_pregunta_id,titulo_respuesta}
                }).done(function(resultado){ 
                    location.reload();
                }).always(function(){
                desaparecer_carga_ajax();
                });              
            }


    function eliminar_dato(tipo,div,id){
        var tipos=[];
        tipos[1]='pregunta';
        tipos[2]='opcion';
        alertify.confirm('Confirmar', '¿Desea eliminar esta '+tipos[tipo], 
        function()
        {
            carga_ajax('Eliminando...');
                $.ajax({
                url: '{{url('eliminar_dato_pregunta')}}',
                type: 'POST',
                data:{'_token':'{{ csrf_token() }}',id,tipo}
                }).done(function(resultado){ 
                    if(resultado==1){
                        $('#'+div).hide();
                        return toastr['success']('Eliminado correctamente');
                    }else{
                        return toastr['error']('La '+tipos[tipo]+' ya esta respondida en alguna encuesta');
                    }                    
                }).always(function(){
                desaparecer_carga_ajax();
                }); 
        },
          function(){}).set({labels:{ok:'Eliminar', cancel: 'Cancelar'}});  
    }


    function agregar_nueva_opcion(pregunta_id,texto){
        tipo_ingreso=1;        
        $('#grupo_pregunta_div').css('display','none');
        $('#texto_pregunta_div').css('display','block');
        $('#verifyModalContent2_title').html('NUEVA OPCIÓN');
        $('#ooo').html('Opción');        
        $('#texto_pregunta').html(texto);
        $('#pregunta_id').val(pregunta_id);
        $('#modal_nueva_opcion').modal('show');
    }

       function agregar_nueva_pregunta(){
        tipo_ingreso=2;
         $('#grupo_pregunta_div').css('display','block');
        $('#texto_pregunta_div').css('display','none');
        $('#verifyModalContent2_title').html('NUEVA PREGUNTA');
        $('#ooo').html('Pregunta');       
        $('#texto_pregunta').html(null);
        $('#pregunta_id').val(null);
        $('#modal_nueva_opcion').modal('show');
    }
    function actualizar_texto_pregunta(id){
        var orden=$('input[name ="pregunta-order-'+id+'"]').val();
        var texto=$('input[name ="pregunta-pregunta-'+id+'"]').val();
         carga_ajax('Eliminando...');
                $.ajax({
                url: '{{url('actualizar_pregunta')}}',
                type: 'POST',
                data:{'_token':'{{ csrf_token() }}',id,orden,texto}
                }).done(function(resultado){ 
                    return toastr['success']('Nombre y numero de pregunta actualizado exitosamente');                  
                }).always(function(){
                desaparecer_carga_ajax();
                }); 
    }

        </script>
@endsection
