<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
         <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
        @endsection
@section('titulo','Crear usuario')
@section('content')
        <div class="breadcrumb">
            <h1>
                Subir Profesionales
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nueva Encuesta
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['url'=>'subir_encuesta_post', 'method' => 'POST','files'=>'true']) !!}
                        <div class="row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="nombre">
                                    Nombre de encuesta
                                </label>
                                {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre para la encuesta']) !!}
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="nombre">
                                   Años despues de graduación para habilitar
                                </label>
                                {!! Form::Number('anios',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                @php
                                $periodos=App\usuarioModel::whereNotNull('periodo_academico')->distinct('periodo_academico')->pluck('periodo_academico','periodo_academico');
                                @endphp
                                     <label>Periodos Académico</label>
                                    {!!Form::select('periodos[]', $periodos,null, ['class' => 'select2','id'=>'periodos','multiple'])!!}
                            </div>
                            <div class="col-md-12 form-group mb-3">
                                <a href="graduados.xlsx">
                                    Descargar formato
                                </a>
                                <br>
                                    <label for="nombre">
                                        Excel de encuesta a subir
                                    </label>
                                    <input name="encuesta" type="file">
                                    </input>
                                </br>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
<script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/ui-toastr.js') }}"></script>
<script src="{{asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
             $(".select2").select2({width: "100%"});
        </script>
        @endsection
