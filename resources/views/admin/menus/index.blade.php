<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Menus del Sistema @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet">
            <link href="{{ asset('assets/css/pages/datatables.css')}}" rel="stylesheet">
                <!--end of page vendors -->
                <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
                <link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
                    @stop
@section('content')
                    <section class="content-header">
                        <h1>
                            Menus del Sistema
                        </h1>
                    </section>
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card filterable">
                                    <div class="card-header clearfix">
                                        <h3 class="card-title pull-left m-t-6">
                                            <i class="ti-control-shuffle">
                                            </i>
                                            Menus del Sistema
                                        </h3>
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" onclick="agregarrow();" type="button">
                                                Nuevo
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table cellspacing="0" class="table table-striped table-bordered table-hover" id="tablesa" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            ID
                                                        </th>
                                                        <th scope="col">
                                                            MENU
                                                        </th>
                                                        <th scope="col">
                                                            RUTA
                                                        </th>
                                                        <th scope="col">
                                                            ICONO
                                                        </th>
                                                        <th scope="col">
                                                            NIVEL
                                                        </th>
                                                        <th scope="col">
                                                            MENU PADRE
                                                        </th>
                                                        <th scope="col">
                                                            ORDEN
                                                        </th>
                                                        <th scope="col">
                                                            VISIBLE
                                                        </th>
                                                        <th scope="col">
                                                            ACCIONES
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($menus))
                                                    <?php $cont=0;?>
                                                    @foreach ($menus as $menu)
                                                    <?php $cont++;?>
                                                    <tr id="tr{{ $cont }}">
                                                        <td class="edit-disabled">
                                                            <input $menu-="" id="idx{{ $cont }}" name="idx{{ $cont }}" type="hidden" value="{{  $menu->id  }}">
                                                            </input>
                                                        </td>
                                                        <td class="editable-enable">
                                                            {{ $menu->menu }}
                                                        </td>
                                                        <td class="editable-enable">
                                                            {{ $menu->ruta }}
                                                        </td>
                                                        <td class="editable-enable">
                                                            {{ $menu->iconfaw }}
                                                        </td>
                                                        <td>
                                                            <select class="form-control" id="nivel{{ $cont }}" name="nivel{{ $cont }}">
                                                                <?php 
                                                               $selected1='';if($menu->nivel==1){$selected1='selected';}
                                                               $selected2='';if($menu->nivel==2){$selected2='selected';}
                                                               ?>
                                                                <option  value="1" {{ $selected1}}>
                                                                    1
                                                                </option>
                                                                <option  value="2" {{ $selected2}}>
                                                                    2
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" id="idmain{{ $cont }}" name="idmain{{ $cont }}">
                                                                @foreach($menus as $men)
                                                                <?php $selected='';if($menu->idmain==$men->id){$selected='selected';}?>
                                                                <option  value="{{$men->id }}" {{ $selected }}>
                                                                    {{ $men->menu }}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td class="editable-enable" >
                                                            {{ $menu->orden }}
                                                        </td>
                                                        <?php
                                                        $checked='';
                                                        if($menu->visible=="SI"){
                                                            $checked='checked';
                                                        }
                                                        ?>
                                                        <td>
                                                            <label class="switch switch-primary">
                                                                <input $checked="" id="digital{{ $cont }}" name="digital{{ $cont }}" type="checkbox" {{ $checked }}>
                                                                    <span class="slider">
                                                                    </span>
                                                                </input>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <a class="botonactualizar btn btn-sm btn-default" data-placement="top" data-toggle="tooltip" id="{{$cont}}" title="Guardar menu">
                                                                <i class="fa fa-pencil">
                                                                </i>
                                                            </a>
                                                            <a class="text-success mr-2" data-placement="top" data-toggle="tooltip" onclick="SeguroEliminar({{ $cont }})" title="Eliminar menu">
                                                                <i class="fa fa-fw fa-times">
                                                                </i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                        @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    @stop
@section('footer_scripts')
                    <!--   page level js -->
                    <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap4.min.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/js/pages/advanced_tables.js')}}" type="text/javascript">
                    </script>
                    <!-- end of page level js -->
                    <script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}">
                    </script>
                    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/js/pages/ui-toastr.js') }}">
                    </script>
                    <script src="{{ asset('assets/js/funciones.js') }}" type="text/javascript">
                    </script>
                    <script src="{{ asset('assets/vendors/editable-table/mindmup-editabletable.js') }}">
                    </script>
                   


                    <script>

    'use strict';
  var table = $('#tablesa').DataTable({
   responsive: true,
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
  
});
 

                        $(function () {
    $('#tablesa').editableTableWidget();    
    });
    function agregarrow()
    {
        var idx=CountRows();
        $('#cant').val(idx); 
       
        var documento='<tr id="tr'+idx+'" style="background: lightgoldenrodyellow;"><td class="edit-disabled"><input type="hidden" name="idx'+idx+'" id="idx'+idx+'">#</td><td tabindex=1></td><td tabindex=1></td><td tabindex=1></td><td class="edit-disabled"><select name="nivel'+idx+'" id="nivel'+idx+'" class="form-control">  <option value="1">1</option><option value="2">2</option></select></td><td class="edit-disabled"><select name="idmain'+idx+'" id="idmain'+idx+'" class="form-control">@foreach($menus as $men)<option value="{{ $men->id }}">{{ $men->menu }}</option>@endforeach</select></td><td tabindex=1></td><td class="edit-disabled"><label class="switch switch-primary"><input type="checkbox" name="digital'+idx+'" id="digital'+idx+'"><span class="slider"></span></label></td><td class="edit-disabled"><a class="botonactualizar btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title="Guardar menu" id="'+idx+'"><i class="fa fa-edit"></i></a><a class="btn btn-sm btn-default js-sweetalert" onClick="SeguroEliminar('+idx+')" data-toggle="tooltip" data-placement="top" title="Eliminar menu"><i class="fa fa-trash-o text-danger"></i></a></td></tr>';
         $('#tablesa tr:first').after(documento);
           // $('#multicolumn_ordering_table tbody').append(documento); 
    }
  function CountRows() {
        var totalRowCount = 0;
        var rowCount = 0;
        var table = document.getElementById("tablesa");
        var rows = table.getElementsByTagName("tr")
        for (var i = 0; i < rows.length; i++) {
            totalRowCount++;
            if (rows[i].getElementsByTagName("td").length > 0) {
                rowCount++;
            }
        }
       return totalRowCount;
    }

      $(document).on('click','.botonactualizar', function(){      
          actualizardatos($(this).attr('id'));     
     });
    function actualizardatos(id)
    { 

         
        var menu= $('#tr'+id+' td:nth-child(2)').text();
        var ruta= $('#tr'+id+' td:nth-child(3)').text();
        var icono= $('#tr'+id+' td:nth-child(4)').text();
        var nivel= $('#nivel'+id).val();
        var idmain= $('#idmain'+id).val(); 
        var orden= $('#tr'+id+' td:nth-child(7)').text();

        var visible='NO';
        if($('#digital'+id).is(':checked')){
        visible='SI';            
        }

        var id_menu=$('#idx'+id).val();
         if(menu==''){
             return mensaje('warning','Usted no ha ingresado el titulo del menu');
        }

        carga_ajax('Espere, se guardando datos ...');
         $.ajax({
                url: '{{ url('configuracion/menus') }}',
                type: 'POST',
                data:{ _token:"{{ csrf_token() }}",menu,icono,nivel,idmain,orden,visible,id:id_menu,ruta }              
            })
            .done(function(datax) {
                $('#idx'+id).val(datax);
                toastr['success']('Exito al guardar');
                desaparecer_carga_ajax();
            })
            .fail(function() {
                desaparecer_carga_ajax();
            })
            .always(function() {
                desaparecer_carga_ajax();
            });

}





function SeguroEliminar(id){

    var id_doc=$('#idx'+id).val();
    if(id_doc==''){        
        $('#tr'+id).remove();
        $('#cant').val(CountRows()-1);            
        }else{

                 
            swal({
                title: "Eliminar Menú",
                text: "Seguro de eliminar Menú?",
                type: "info",
                showCancelButton: true,
                confirmButtonText: 'Eliminar',
                showLoaderOnConfirm: true,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                preConfirm: function (email) {
                    /*return new Promise(function (resolve, reject) {
                        setTimeout(function () {
                            if (email === 'taken@example.com') {
                                reject('This email is already taken.')
                            } else {
                                resolve()
                            }
                        }, 2000)
                    })*/

                      $.ajax({
                type: "DELETE",
                url: "{{url('/')}}/configuracion/menus/"+id_doc,
                data: { "_token": "{{ csrf_token() }}" },
                success: function (data) {
                    if(data==1){
                        $('#tr'+id).remove();
                        swal(
                        'Eliminado!',
                        'EL Menú fue eliminado correctamente.',
                        'success'
                        )
                    }else{
                         swal(
                            'Contiene Menú Padre',
                            'El Menú esta ligado a alguna acción',
                            'error'
                      )
                 }
            }                   
    });


                },
                allowOutsideClick: false
            });
      

   }
}
                    </script>



@stop