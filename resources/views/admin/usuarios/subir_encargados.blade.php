<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('title')
    Subir Encargados @parent
@stop
@section('header_styles')
<link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/selective/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
        @endsection
@section('titulo','Crear usuario')
@section('content')
        <div class="breadcrumb">
            <h1>
                Subir Encargados
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Subir Encargados
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['url'=>'subir_encargados_post', 'method' => 'POST','files'=>'true']) !!}
                        <div class="row">
                            <div class="col-md-12 form-group mb-3">
                                <a href="{{ asset('formato encargados.xlsx') }}">
                                    Descargar formato
                                </a>
                                <br>
                                    <label for="nombre">
                                        Excel de profesionales a subir
                                    </label>
                                    <input name="profesionales" type="file">
                                    </input>
                                </br>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
        <script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}">
        </script>
        <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript">
        </script>
        <script src="{{ asset('assets/js/pages/ui-toastr.js') }}">
        </script>
        @endsection
