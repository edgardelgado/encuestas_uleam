<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')        
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
@endsection
@section('titulo','Crear usuario')
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
        <div class="breadcrumb">
            <h1>
                Crear usuario
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo usuario
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'usuarios.store', 'method' => 'POST']) !!}
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Identificación
                                </label>
                                {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese N° de Identificación']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombres
                                </label>
                                {!! Form::Text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese los nombres']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="apellidos">
                                    Apellidos
                                </label>
                                {!! Form::Text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese los apellidos']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="email">
                                    Correo electrónico
                                </label>
                                {!! Form::Text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el correo electrónico']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="telefono">
                                    Teléfono
                                </label>
                                {!! Form::Text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese número de teléfono']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="rol_id">
                                    Rol
                                </label>
                                {!! Form::select('rol_id',$roles,null,['class'=>'form-control select2']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="area_id">
                                    Carrera
                                </label>
                                {!! Form::select('carrera_id',$carreras,null,['class'=>'form-control select2']) !!}
                            </div>
                              <div class="col-md-6 form-group mb-3">
                                    {!! Form::label('estado', 'ESTADO:') !!}            
                                     {!! Form::select('estado',  array(
                                                                    'ACT' => 'ACT',
                                                                    'DES' => 'DES'), null ,['class'=>'select2']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="password">
                                    Password
                                </label>
                                {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
                            </div>
                               <div class="col-md-6 form-group mb-3">
                                    <label for="nombre">
                                        Carreras a encargarse (Solo se guarda en caso de que se guarde como rol encargado)
                                    </label>
                                   {!!Form::select('carreras[]', $carreras,null, ['class' => 'select2','multiple'])!!}
                                </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function()  {
          $(".select2").select2({width: "100%"});
        });
    </script>
@endsection
 