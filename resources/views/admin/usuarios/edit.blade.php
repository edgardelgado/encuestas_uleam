@extends('layouts.master')
@section('header_styles')     
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
@endsection
@section('title')
    Editar Usuario @parent
@stop
@section('content')

@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
                        <section class="content-header">
                            <h1>
                                Editar usuario
                            </h1>
                        </section>
                        <section class="content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                Editar usuario
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            @include('alerts.errors')
                            @include('alerts.request')
                            {!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id))) !!}
                                  <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Identificación
                                </label>
                                {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese N° de Identificación']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombres
                                </label>
                                {!! Form::Text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese los nombres']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="apellidos">
                                    Apellidos
                                </label>
                                {!! Form::Text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese los apellidos']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="email">
                                   Correo electrónico
                                </label>
                                {!! Form::Text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el correo electrónico']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="telefono">
                                    Teléfono
                                </label>
                                {!! Form::Text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese número de teléfono']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="rol_id">
                                    Rol
                                </label>
                                {!! Form::select('rol_id',$roles,null,['class'=>'form-control select2']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="area_id">
                                    Carrera
                                </label>
                                  {!! Form::select('carrera_id',$carreras,null,['class'=>'form-control select2']) !!}
                            </div>
                              <div class="col-md-6 form-group mb-3">
                                    {!! Form::label('estado', 'ESTADO:') !!}            
                                     {!! Form::select('estado',  array(
                                                                    'ACT' => 'ACT',
                                                                    'DES' => 'DES'), null ,['class'=>'select2']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="password">
                                    Password
                                </label>
                                {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña']) !!}
                            </div>
                             <div class="col-md-6 form-group mb-3">
                                    <label for="nombre">
                                        Carreras a encargarse (Solo se guarda en caso de que se guarde como rol encargado)
                                    </label>
                                   {!!Form::select('carreras[]', $carreras,$carreras_selected, ['class' => 'select2','multiple'])!!}
                                </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--row end-->
                        </section>
                        @endsection
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function()  {
          $(".select2").select2({width: "100%"});
        });
    </script>
@endsection