<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
@endsection
@section('titulo','Crear usuario')
@section('content')
        <div class="breadcrumb">
            <h1>
                Crear Profesional
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo  Profesional
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'profesionales.store', 'method' => 'POST']) !!}
                        <div class="row">

                          <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Identificación
                                </label>
                                {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese N° de Identificación']) !!}
                            </div>


                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Carrera Profesional
                                </label>
                                {!!Form::select('carrera_id', $carreras,null, ['class' => 'select2'])!!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombres
                                </label>
                                {!! Form::Text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese los nombres']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="apellidos">
                                    Apellidos
                                </label>
                                {!! Form::Text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese los apellidos']) !!}
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="email">
                                    Correo electrónico
                                </label>
                                {!! Form::Text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el correo electrónico']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="telefono">
                                    Teléfono
                                </label>
                                {!! Form::Text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese número de teléfono']) !!}
                           </div>
                    

                                 <div class="col-md-6 form-group mb-3">
                                     <label>Periodos Académico</label>
                                    {!!Form::select('periodo_academico', $periodos,null, ['class' => 'select2','id'=>'periodos'])!!}
                                 </div>

                            <div class="col-md-6 form-group mb-3">
                                     <label>Año de graduación</label>
                                    {!!Form::select('anio_graduacion', $anios,null, ['class' => 'select2','id'=>'anios'])!!}
                                 </div>
                              <div class="col-md-6 form-group mb-3">
                                     <label>Fecha de graduación</label>
                                     <br>   
                                    {!!Form::Date('fecha_graduacion', $anios, ['class' => 'form-control','id'=>'fecha_graduacion'])!!}
                                 </div>


                          
                         
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
<script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>

<script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/ui-toastr.js') }}"></script>

<script type="text/javascript">
$(".select2").select2({width: "100%"});



   @if(Session::has('message'))
    //success
    toastr['success']('{{Session::get('message')}}'); 
    @endif

    @if(Session::has('message-error'))
    //error
    toastr['error']('{{Session::get('message-error')}}'); 
    @endif

    @if(count($errors) > 0)
    //errorees de fornmularios
    <?php
    $resultado='<ul>';
    foreach ($errors->all() as $error) {
        $resultado.="<li>$error</li>";
    }
    $resultado.='</ul>';
    ?>
    toastr['error']('<?php echo $resultado; ?>');               
    @endif



</script>
@endsection
 