<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
@endsection
@section('titulo','Editar usuario')
@section('content')
        <div class="breadcrumb">
            <h1>
                Editar Profesional
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo  Profesional
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('profesionales.update', $usuario->id))) !!}
                        <div class="row">

                          <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Identificación
                                </label>
                                {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese el identificacion']) !!}
                            </div>


                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Carrera Profesional
                                </label>
                                {!!Form::select('carrera_id', $carreras,null, ['class' => 'select2'])!!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    Nombres
                                </label>
                                {!! Form::Text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese el nombres']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="apellidos">
                                    Apellidos
                                </label>
                                {!! Form::Text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese el apellidos']) !!}
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="email">
                                    Correo eletrónico
                                </label>
                                {!! Form::Text('email',null,['class'=>'form-control','placeholder'=>'Ingrese el email']) !!}
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="telefono">
                                    Telefono
                                </label>
                                {!! Form::Text('telefono',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono']) !!}
                           </div>
                             <div class="col-md-6 form-group mb-3">
                                     <label>Periodos Académico</label>
                                    {!!Form::select('periodo_academico', $periodos,null, ['class' => 'select2','id'=>'periodos'])!!}
                                 </div>
                                  <div class="col-md-6 form-group mb-3">
                                     <label>Año de graduacion</label>
                                    {!!Form::select('anio_graduacion', $anios,null, ['class' => 'select2','id'=>'anios'])!!}
                                 </div>

                                       <div class="col-md-6 form-group mb-3">
                                     <label>Fecha de graduacion</label>
                                     <br>   
                                  
                                    <input type="date" name="fecha_graduacion" value="{{ date('Y-m-d',strtotime($usuario->fecha_graduacion)) }}" />
                                 </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
<script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(".select2").select2({width: "100%"});
</script>
@endsection
 
