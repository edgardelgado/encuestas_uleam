<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Profesionales @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet">
            <link href="{{ asset('assets/css/pages/datatables.css')}}" rel="stylesheet">
                <link href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}" rel="stylesheet" type="text/css">
                    <link href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}" rel="stylesheet" type="text/css">
                        <script src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}" type="text/javascript">
                        </script>
                        <!--end of page vendors -->
                        @stop
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <h1>
                                Gestión de Profesionales
                            </h1>
                        </section>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card filterable">
                                        <div class="card-header clearfix">
                                            <h3 class="card-title pull-left m-t-6">
                                                <i class="ti-control-shuffle">
                                                </i>
                                                Profesionales
                                                <!--Add / Remove rows-->
                                            </h3>
                                            <div class="pull-right">
                                                <a class="btn btn-primary btn-sm" href="{{ URL::route('profesionales.create') }}" type="button">
                                                    Nuevo
                                                </a>
                                                @if(Auth::user()->rol_id==1)
                                                 <a class="btn btn-primary btn-sm" href="{{ url('subir_profesionales') }}" type="button">
                                                    Subida Masiva
                                                </a>
                                                @endif

                                               <!-- <a class="button" href="{{ URL::route('profesionales.create') }}"><i class="ln ln-icon-Add-User"></i>Nuevo</a><a class="button" href="{{ url('subir_profesionales') }}">Subir de Manera Masiva</a>-->
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table cellspacing="0" class="table" id="tabla" width="120%">
                                                    <thead>
                                                        <tr>

                                                        <th width="5%">Opciones</th> 
                                                        <th>Identificación</th>
                                                        <th>Apellidos</th>  
                                                        <th>Nombres</th>  
                                                        <th>Correo electrónico</th> 
                                                        <th>Teléfono</th>  
                                                        <th>Carrera</th>
                                                        <th>Año</th> 
                                                        <th>Período</th>          
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- fourth datatable end -->
                            <!-- fifth row datatable -->
                            <!-- fifth row datatable end -->
                        </section>
                        <!-- /.content -->
                        @stop




@section('footer_scripts')
                        <!--   page level js -->
                        <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap4.min.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js')}}" type="text/javascript">
                        </script>
                        <script src="{{ asset('assets/js/pages/advanced_tables.js')}}" type="text/javascript">
                        </script>
                        <!-- end of page level js -->
                        <script>


 $(document).ready(function(){});


  function SeguroEliminar($id)
    {
      alertify.confirm('Confirmar eliminación', '¿Desea eliminar este profesional?', 

        function()
        {

            carga_ajax();

            $.ajax({
               url: "{{ url(request()->route()->getPrefix().'/profesionales') }}/"+$id,
               data: { "_token": "{{ csrf_token() }}" },
              type: 'DELETE',
            })
            .done(function() {
             // location.reload();
              desaparecer_carga_ajax();
            })
             .fail(function() {
              desaparecer_carga_ajax();
            })
             .always(function() {
              desaparecer_carga_ajax();
            });
              },
          function(){});


        }

  

      var tabla = $('#tabla').DataTable({
                responsive : true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                language: {
                    "emptyTable":     "No hay Profesionales  aún",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ Profesionales",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 Profesionales",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
              
                    "order": ([ 0, 'desc' ]),
        dom: 'flrBtip',//Bflipt
        buttons: [
            'copy', 'csv','excel', 'print'
        ],
        "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('profesional_ajax') }}",

                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [

                { "data": "acciones","orderable":false },
                { "data": "identificacion","orderable":false  },
                { "data": "apellidos","orderable":false  },
                { "data": "nombres","orderable":false  },
                { "data": "email","orderable":false  },
                { "data": "telefono","orderable":false  },                
                { "data": "carrera" ,"orderable":false },    
                { "data": "anio_graduacion","orderable":false  },    
                { "data": "periodo_academico","orderable":false  }
                ]                             
            });  

        




 </script>
@stop
                  
