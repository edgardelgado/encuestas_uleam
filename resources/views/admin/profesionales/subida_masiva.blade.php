<?php Autorizar(Request::path()); ?>
@extends('layouts.master')
@section('titulo')
Subir Profesionales de Manera Masiva
@stop
@section('contenido')
@include('alerts.errors')
@include('alerts.request')
<div id="titlebar" class="single">
    <div class="container">
        
        <div class="sixteen columns">
            <h2>Subir Profesionales de Manera Masiva</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>Estás aquí:</li>
                    <li><a href="#">Master</a></li>
                    <li>Gestión de profesionales</li>
                    <li>Subir Profesionales de Manera Masiva</li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="sixteen columns">
      {!! Form::open(['url'=>'subir_profesionales', 'method' => 'POST','files'=>'true']) !!}    
        <div class="submit-page">
            <div class="notification notice closeable margin-bottom-40">
                <p><span>Aviso: </span> Debe seleccionar el excel</p>
            </div>
            <div class="form">
                <h5>Carreras Profesionales</h5>
                <select id="select-carreras" name="carrera_id"></select>
            </div>
                <div class="form">
                <h5>Carreras  <span>(Excel)</span></h5>
                <input id="file-4" type="file"   name="profesionales">
                 <a href="{{ url('descargar_formato/Desemepleados.xlsx') }}">DESCARGAR FORMATO CORRECTO</a>
            </div>
        <div class="divider margin-top-0 padding-reset"></div>
         <button type="submit" class="button big margin-top-5">SUBIR PROFESIONALES  A LA PLATAFORMA<i class="fa fa-save"></i></button>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<div class="margin-top-60"></div>
@stop

@section('script')
 {!! Html::script('admin/selective/selectize.js') !!}
{!! HTML::style('admin/selective/selectize.bootstrap3.css'); !!}
<link href="{{ asset('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script src="{{ asset('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js') }}"></script>
<script>    
 $(document).ready(function(){
       $('#file-4').fileinput({
          language:"es",
          'theme': 'explorer',
            showUpload:false           
       });
 });
     $('#select-carreras').selectize({
    valueField: 'id', 
    labelField: 'value',  
    searchField: 'value', 
    options: [],
    //create: true,
    render: {
        option: function(item, escape) {
            return '<div>' +             
                '<span class="description">' + escape(item.value) + '</span>' +               
            '</div>';
        }
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item);
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{url('consultar_carreras')}}/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res.slice(0, 10));
            }
        });
    }
});
 </script>
 @stop