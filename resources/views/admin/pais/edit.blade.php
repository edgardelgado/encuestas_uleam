<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')

@endsection

@section('title')
    Editar País @parent
@stop
@section('content')
                            <section class="content-header">
                                <h1>
                                    Editar País
                                </h1>
                            </section>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                                    Editar País
                                                </h3>
                                            </div>
                                            <div class="card-body">
                                                @include('alerts.errors')
                                            @include('alerts.request')
                                             {!! Form::model($pais, array('method' => 'PATCH', 'route' => array('paises.update', $pais->id))) !!}
                                                <div class="row">
                                                    <div class="col-md-6 form-group mb-3">
                                                        <label for="nombre">
                                                            nombre
                                                        </label>
                                                        {!! Form::Text('nombre',null,['class'=>'form-control']) !!}
                                                    </div>
                                                  
                                                    <div class="col-md-12">
                                                        <button class="btn btn-primary" type="submit">
                                                            Actualizar
                                                        </button>
                                                    </div>
                                                </div>
                                               
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--row end-->
                            </section>
                            @endsection
@section('footer_scripts')
 @endsection
