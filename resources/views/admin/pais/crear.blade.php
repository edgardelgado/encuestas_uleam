<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
        @endsection

@section('title')
    Crear País @parent
@stop
@section('content')
        <div class="breadcrumb">
            <h1>
                Crear País
            </h1>
        </div>
        <div class="separator-breadcrumb border-top">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title mb-3">
                            Nuevo
                        </div>
                        @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['route'=>'paises.store', 'method' => 'POST']) !!}
                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="nombre">
                                    nombre
                                </label>
                                {!! Form::Text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre']) !!}
                            </div>
                          
                            <div class="col-md-12">
                                <button class="btn btn-primary" type="submit">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript">
    </script>
        </script>
        @endsection
 