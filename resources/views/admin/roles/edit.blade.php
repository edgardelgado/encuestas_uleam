<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')

@endsection
@section('titulo','Editar rol')
@section('content')
<div class="breadcrumb">
    <h1>
        Editar rol
    </h1>
</div>
<div class="separator-breadcrumb border-top">
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title mb-3">
                    Editar rol
                </div>
                @include('alerts.errors')
                            @include('alerts.request')
                             {!! Form::model($rol, array('method' => 'PATCH', 'url' => array('configuracion/roles', $rol->id))) !!}
                <div class="row">
                    <div class="col-md-6 form-group mb-3">
                        <label for="descripcion">
                            Descripción
                        </label>
                        {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una descripción (Opcional)']) !!}
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary" type="submit">
                            Actualizar
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
@endsection
