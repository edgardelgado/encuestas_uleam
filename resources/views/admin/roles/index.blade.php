<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Roles del sistema @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet">
            <link href="{{ asset('assets/css/pages/datatables.css')}}" rel="stylesheet">
                <!--end of page vendors -->
                <link href="{{asset('assets/plugins/jquery-colorbox/example1/colorbox.css')}}" rel="stylesheet"/>
                <link href="{{asset('assets/plugins/jsTree/style.css')}}" rel="stylesheet">
                    <link href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}" rel="stylesheet" type="text/css">
                        <link href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}" rel="stylesheet" type="text/css">
                            <script src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}" type="text/javascript">
                            </script>
                            @stop
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')
                            <!-- Content Header (Page header) -->
                            <section class="content-header">
                                <h1>
                                    Roles del sistema
                                </h1>
                            </section>
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card filterable">
                                            <div class="card-header clearfix">
                                                <h3 class="card-title pull-left m-t-6">
                                                    <i class="ti-control-shuffle">
                                                    </i>
                                                    Roles del sistema
                                                </h3>
                                                <div class="pull-right">
                                                    <a class="btn btn-primary btn-sm" href="{{ URL::route('roles.create') }}">
                                                        Nuevo
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table cellspacing="0" class="table" id="table" width="100%">
                                                        <thead>
                                                            <tr class="filters">
                                                                <th scope="col">
                                                                    ID
                                                                </th>
                                                                <!-- <th scope="col">ROL</th>-->
                                                                <th scope="col">
                                                                    DETALLE
                                                                </th>
                                                                <th scope="col">
                                                                    ACCIONES
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($roles))
                                        @foreach ($roles as $rol)
                                                            <tr id="tr{{ $rol->id }}">
                                                                <td>
                                                                    {{ $rol->id }}
                                                                </td>
                                                                <!--<td>{{ $rol->nombre }}</td>-->
                                                                <td>
                                                                    {{ $rol->descripcion }}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-default" data-placement="top" data-toggle="tooltip" href="{{ route('roles.edit',$rol->id) }}" title="Editar Rol">
                                                                        <i class="fa fa-edit">
                                                                        </i>
                                                                    </a>
                                                                    <a class="btn btn-sm btn-default js-sweetalert" onclick="SeguroEliminar({{$rol->id}})" title="Eliminar Rol">
                                                                        <i class="fa fa-trash-o text-danger">
                                                                        </i>
                                                                        <a class="btn btn-sm btn-default js-sweetalert" data-placement="top" data-toggle="tooltip" onclick="administrar_permisos({{$rol->id}})" title="Configurar permisos">
                                                                            <i class="fa fa-key">
                                                                            </i>
                                                                        </a>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fourth datatable end -->
                                <!-- fifth row datatable -->
                                <!-- fifth row datatable end -->
                            </section>
                            <!-- /.content -->
                            @stop
@section('footer_scripts')
                            <!--   page level js -->
                            <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap4.min.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js')}}" type="text/javascript">
                            </script>
                            <script src="{{ asset('assets/js/pages/advanced_tables.js')}}" type="text/javascript">
                            </script>
                            <!-- end of page level js -->
                            <script src="{{asset('assets/plugins/jquery-colorbox/jquery.colorbox.js')}}" type="text/javascript">
                            </script>
                            <script src="{{asset('assets/plugins/jsTree/jstree.min.js')}}">
                            </script>
                            <script type="text/javascript">



    'use strict';
  var table = $('#table').DataTable({
   responsive: true,
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
  
});
 



    $(document).ready(function(){
        $(".dataTables_length .form-control").removeClass('form-control-sm');
    })

    function administrar_permisos(id){
        var direccion="{{ url('configuracion/roles/administrar_permisos') }}/"+id;
        $.colorbox({href : direccion, width:"100%", height:"100%"});
    }

      function SeguroEliminar($id)
    {
      alertify.confirm('Confirmar', '¿Desea eliminar este usuario?', 
        function()
        {
            $.ajax({
               url: "{{ url(request()->route()->getPrefix().'/roles') }}/"+$id,
               data: { "_token": "{{ csrf_token() }}" },
              type: 'DELETE',
            })
            .done(function() {
              location.reload();
              desaparecer_carga_ajax();
            })
             .fail(function() {
              desaparecer_carga_ajax();
            })
             .always(function() {
              desaparecer_carga_ajax();
            });
              },
          function(){});


        }
                            </script>
                            @stop
