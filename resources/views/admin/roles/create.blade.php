<?php Autorizar(Request::path()); ?> 
@extends('layouts.master')
@section('header_styles')
<link href="{{ asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}" rel="stylesheet">
    @endsection
@section('titulo','Crear rol')
@section('content')
    <div class="breadcrumb">
        <h1>
            Crear rol
        </h1>
    </div>
    <div class="separator-breadcrumb border-top">
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">
                        Nuevo rol
                    </div>
                    @include('alerts.errors')
                            @include('alerts.request')
                           {!! Form::open(['url'=>'configuracion/roles', 'method' => 'POST']) !!}
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="descripcion">
                                Descripción
                            </label>
                            {!! Form::Text('descripcion',null,['class'=>'form-control','placeholder'=>'Ingrese una descripción (Opcional)']) !!}
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary" type="submit">
                                Registrar
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}" type="text/javascript">
    </script>
    @endsection
