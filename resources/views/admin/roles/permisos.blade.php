<script>
$(document).ready(function () {
    $('#jstree').on('changed.jstree', function (e, data) {
        var i, j, r = [];
        for(i = 0, j = data.selected.length; i < j; i++) {
            r.push(data.instance.get_node(data.selected[i]).text);
            //r.push(data.instance.get_node(data.selected[i]).text.)
        }
        //$('#event_result').html('Selected: ' + r.join(', '));
        var valor = r.join(', ');
        valor= valor.replace(/idmenu/g,'idmenumain[]');
        //valor= valor.replace(/hidden/g,'text');
        $('#divselect').html(valor);
        //            alert (valor);
    }).jstree({
        'state': {
            'opened': true
            },
        'core' : {
            'check_callback' : true
        },
        "checkbox" : {
            "keep_selected_style" : false
        },
        "plugins" : [ "checkbox" ]                                      
    }) ;             
});
</script>
<div class="breadcrumb">
    <center>
        <h1>
            Permisos para el rol
            <strong>
                {{ $rol->descripcion }}
            </strong>
        </h1>
    </center>
</div>
<div class="separator-breadcrumb border-top">
</div>
<div class="card mb-8">
    {!! Form::open(['url'=>'guardar_permisos', 'method' => 'POST']) !!}
    <input name="rol_id" type="hidden" value="{{ $rol->id }}">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 form-group mb-3" id="jstree">
                    <ul>
                        <li class="jstree-open">
                            Todos los Menus
                            <ul>
                                <?php
                                $rol_id=$rol->
                                id;
                                $menuselec="";
                                $idmesel=0;
                                $menu= $tabla;
                                ?>
                                @foreach ($tabla as $tab)
                                <?php 
                                        $versela=App\AsignarPermisosModel::where('idmenu',$tab->id)
                                        ->where('idrol',$rol_id)->first(); 
                                        ?>
                               
                                       
                                        
                                         @if(!count($tab->menus))
                                                @if($versela)
                                                <li class='jstree-open' data-jstree='{"selected":true}'>
                                                <input type="hidden" name="idmenu{{ $tab->id }}" value="{{ $tab->id }}"> {{ $tab->menu }}  
                                                @else
                                                <li class="jstree-open">
                                                <input name="idmenu{{ $tab->id }}" type="hidden" value="{{ $tab->id }}">
                                                {{ $tab->menu }} 
                                                @endif                                   
                                        @else 
                                        <li class='jstree-open'>
                                            <input type="hidden" name="idmenu{{ $tab->id }}" value="{{ $tab->id }}"> {{ $tab->menu }} 
                                        <ul>
                                            @foreach($tab->menus as $menux)
                                            <?php
                                        $verselx=App\AsignarPermisosModel::where('idmenu',$menux->
                                            id)
                                        ->where('idrol',$rol_id)->first();                              
                                        ?>
                                        @if($verselx)
                                            <li class="jstree-open" data-jstree='{"selected":true}'>
                                                <input name="idmenu{{ $menux->id }}" type="hidden" value="{{ $menux->id }}">
                                                    {{ $menux->menu }}
                                                </input>
                                            </li>
                                            @else
                                            <li class="jstree-open">
                                                <input name="idmenu{{ $menux->id }}" type="hidden" value="{{ $menux->id }}">
                                                    {{ $menux->menu }}
                                                </input>
                                            </li>
                                            @endif
                                        @endforeach
                                    </li>
                                        </ul>
                                        @endif
                                    </input>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="car-footer">
            <div class="col-md-12 form-group mb-3">
                <center>
                    <button class="btn btn-primary" type="submit">
                       <i class="fa fa-save">  </i> Guardar Permisos
                    </button>
                </center>
            </div>
        </div>
        <div id="divselect" style="display:none">
            {!! Form::close() !!}
        </div>
    </input>
</div>