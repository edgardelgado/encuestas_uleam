@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Perfil @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!-- page vendors -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/iCheck/css/all.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/user_profile.css')}}">
    <!--end of page vendors -->
@stop
{{-- Page content --}}
@section('content')
@include('alerts.errors')
@include('alerts.request')
@include('alerts.success')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Perfil de Usuario</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                @php
                $us=Auth::user();
                  if (Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) {
                        
                    }

                @endphp

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile text-center">
                        <img class="profile-user-img img-fluid rounded-circle" src="{{asset('assets/img/authors/user.jpg')}}"
                             alt="User profile picture">
                        <h3 class="profile-username text-center">{{ $us->full_name }}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Información</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <strong><i class="fa fa-book margin-r-5"></i> 
                            @if(Auth::user()->rol_id != 1&&Auth::user()->rol_id != 5) 
                            @php
                            $carreras=App\direccion_carreraModel::where('usuario_id',Auth::id())->get();
                            @endphp
                                @if(count($carreras))
                                <ul>
                                @foreach($carreras as $ca)
                                <li>{{ $ca->carrera->titulo->titulo }}</li>
                                @endforeach
                                </ul>
                                @else
                                Sin Carreras Asignadas                           
                                @endif
                            @endif
                        </strong>
                        <p>
                          {{ $us->rol->descripcion }}
                        </p>
                        <hr>
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                        <p>ULEAM</p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a href="#activity" class="nav-link active" data-toggle="tab" aria-expanded="true">Actualizar Datos</a>
                        </li>
                        <li class="nav-item"><a href="#settings" data-toggle="tab" class="nav-link" aria-expanded="false">Cambiar Contraseña</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                                {!! Form::model($us, array('class'=>'horizontal','method' => 'PATCH', 'url' => array('actualizar_perfil', $us->id))) !!}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Cédula</label>
                                    <div class="col-sm-10">
                                        {!! Form::Text('identificacion',null,['class'=>'form-control','readonly']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Apellidos</label>
                                    <div class="col-sm-10">
                                        {!! Form::Text('apellidos',null,['class'=>'form-control','placeholder'=>'Apellidos']) !!}
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Nombres</label>
                                    <div class="col-sm-10">
                                        {!! Form::Text('nombres',null,['class'=>'form-control','placeholder'=>'Nombres']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        {!! Form::Text('email',null,['class'=>'form-control','placeholder'=>'Correo electrónico']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputExperience" class="col-sm-2 control-label">Descripción</label>

                                    <div class="col-sm-10">
                                        {!! Form::Textarea('descripcion',null,['class'=>'form-control resize_vertical','rows'=>4,'placeholder'=>'Descripción']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Actualizar</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="settings">
                           {!! Form::model($us, array('class'=>'horizontal','method' => 'PATCH', 'url' => array('actualizar_clave', $us->id))) !!}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-6 control-label">Contraseña Actual</label>
                                    <div class="col-sm-10">
                                         {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="inputName" class="col-sm-6 control-label">Nueva Contraseña</label>
                                    <div class="col-sm-10">
                                         {!! Form::password('password2',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="inputName" class="col-sm-6 control-label">Confirmar nueva contraseña</label>
                                    <div class="col-sm-10">
                                         {!! Form::password('password3',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña','required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Actualizar contraseña</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- /.content -->

@stop
@section('footer_scripts')
    <!--   page level js ----------->
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/user_profile.js')}}" type="text/javascript"></script>
    <!-- end of page level js -->
@stop
