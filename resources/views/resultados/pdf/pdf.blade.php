<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.bubble.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.snow.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
    <title>Resultados_encuestas_graficos</title>
    <style type="text/css">
    header {
      position: fixed; top: -10px; left: 0px; right: 0px; height: 50px; }


    </style>



  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-lg navbar-light navbar-static-top" role="navigation" id="imprimir">
          <a class="logo navbar-brand mr-0" href="http://localhost/encuestas_uleam/public/inicio">
            <img alt="logo" style="position: absolute; left: 0;" src="http://localhost/encuestas_uleam/public/assets/img/logo_blue.png"/>
            <img alt="logo" style="position: absolute; left: 300;" src="http://localhost/encuestas_uleam/public/assets/img/logo_vic.png"/>
          </a>
    </header>
    <div class="card" id="imprimir">


      <div id="resultado_totales">
      @foreach($preguntas as $preg)

              @if($preg->opciones_respuesta==1)
                  @php
                      $opciones_prin=App\opcionModel::where('pregunta_principal_id',$preg->id)->get();
                  @endphp
                  <div class="col-md-12">
                      <div class="card">
                          <div class="card-header" >
                              <h3 class="card-title">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h3></div>
                          <div class="card-body container-fluid">
                              <div class="row">
                                      <!-- Opciones -->
                                          @php
                                          $opxx=0;
                                          @endphp
                                          @foreach($preg->opciones as $op)
                                            @php
                                          $opxx++;
                                          @endphp
                                          <div class="col-md-12">
                                              <div class="example-wrap">
                                                <a onclick="mostrar_info({{ $preg->id }},{{ $op->id }})">
                                                      <div class="card">
                                                          <div class="card-header">
                                                              <h6>{{ intval($preg->order) }}.{{ $opxx }} .-{{ $op->opcion }}</h6>
                                                          </div>
                                                          <div class="card-body">
                                                              <div>
                                                                  <div id="pie{{ $preg->id.$op->id }}" style="height: 300px;"></div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </a>
                                              </div>
                                          </div>
                                          @endforeach
                              </div>
                          </div>
                      </div>
                  </div>
              @endif
              @if($preg->opciones_respuesta==0)
                  <div class="col-md-12">
                      <div class="card">
                          <div class="card-header">
                          <h3 class="card-title">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h3></div>
                          <a onclick="mostrar_info({{ $preg->id }},{{ $preg->opciones_respuesta }})">
                          <div class="card-body container-fluid">
                              <div class="row row-lg">
                                  <div class="col-md-8">
                                      <!-- Opciones -->
                                      <div class="example-wrap">
                                           <div class="card-body">
                                              <div id="pie{{ $preg->id }}" style="height: 300px;"></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          </a>
                      </div>
                  </div>
              @endif
      @endforeach
      </div>
    </div>

  </body>
  <script src="{{asset('assets/vendors/echart/echarts.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}"></script>
  <script src="{{asset('js/underscore.min.js')}}"></script>
  <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
  <script>
  function pruebaDivAPdf()
   {
      var pdf = new jsPDF('p', 'pt', 'letter');
      pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 298);

      source = $('#imprimir')[0];


      specialElementHandlers = {
          '#bypassme': function (element, renderer) {
              return true
          }
      };

      margins = {
          top: 80,
          bottom: 60,
          left: 40,
          width: 522
      };

      pdf.fromHTML(
          source,
          margins.left, // x coord
          margins.top, { // y coord
              'width': margins.width,
              'elementHandlers': specialElementHandlers
          },

          function (dispose) {
              pdf.save('Prueba.pdf');
          }, margins
      );
      function headerFooterFormatting(doc)
        {
            var totalPages  = doc.internal.getNumberOfPages();

            for(var i = totalPages; i >= 1; i--)
            { //make this page, the current page we are currently working on.
                doc.setPage(i);

                header(doc);

                footer(doc, i, totalPages);

            }
        };
  };

  </script>

</html>
