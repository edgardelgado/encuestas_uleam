@extends('layouts.desnuda')
@section('header_styles')
<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.bubble.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.snow.css')}}">
@endsection
@section('title')
    {{ $encuesta->nombre}} @parent
@stop
@section('titulo',$encuesta->nombre)
@section('content')
<style type="text/css" media="print">
      div.siguiente_pagina
      {
       page-break-after:always;
      }
      div.inicio_pagina{
        margin-top: 5%;
      }
      div.inicio_paginax{
        page-break-before: always;
      }
</style>
<style type="text/css">
    #cabecera {
            margin-top: 0px;
            margin-left: 3px;
            margin-right: 3px;
            text-align: center;
    }
    .dot {
      height: 15px;
      width: 15px;
      border-radius: 100%;
     display: inline-block;
     margin-left: 1%;
     margin-top: 1%;
    }   
    #logo{
    width: 25%;
    position: absolute;
    right:72%;
}

.card-title2 {
    font-size: 15px;
    font-weight: bold;
    margin-top: 0;
    margin-bottom: 0;
    color: #777;
}
</style>
            <div class="col-md-12">           
                <div id="cabecera">
                    <img alt="Logo" src="{{ asset('assets/img/logo_white.png')}}" id="logo"></img>
                    <p>
                        <span style="font-weight: 500; font-size: 20px;">Vicerrectorado Académico</span>
                        <br>
                        <span style="font-weight: 500;">Área de Seguimiento de Graduados</span>
                    </p>
                    <p style="font-weight: 800; font-size: 20px;">RESULTADOS DE {{ $encuesta->nombre}}</p>
                    <p style="text-align: right;  margin-right: 18px; font-size: 14px; font-family: times;">
                        Fecha y Hora de impresión: {{ date('Y-m-d H:i A') }}
                        <br>    
                         <strong>Usuario:</strong> {{ Auth::user()->apellidos." ".Auth::user()->nombres }}
                        <br>    
                         <strong>Total Encuestados:</strong>{{ $total }}
                        @if(!is_null($carrerax))
                         <strong>Carrera(S):</strong>{{ $carrerax }}
                        @endif
                    </p>
                </div>
            </div>
            @php
            $total_item=0;
            @endphp
            @foreach($preguntas as $preg)
                    @if($preg->opciones_respuesta==1)
                        @php
                            $opciones_prin=App\opcionModel::where('pregunta_principal_id',$preg->id)->get();
                        @endphp
                        @if($total_item % 3 == 0)                       
                        <div class="inicio_paginax col-md-12">
                        @else
                        <div class="col-md-12">
                        @endif
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title2">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h4>
                                </div>
                                <div class="card-body container-fluid">
                                    
                                        
                                            <!-- Opciones -->
                                                @php
                                                $opxx=0;
                                                @endphp                                
                                                @foreach($preg->opciones as $op)
                                                @php
                                                $opxx++;

                                                $item_ant=$total_item;
                                                $total_item++;
                                                @endphp  
                                                @if($total_item % 3 == 0)                       
                                                <div class="siguiente_pagina col-md-12">
                                                @elseif($item_ant>0&&$item_ant % 3 == 0) 
                                                <div class="inicio_pagina col-md-12">
                                                @else 
                                                <div class="col-md-12">
                                                @endif                  
                                                    <div class="example-wrap">                                     
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h6>{{ intval($preg->order) }}.{{ $opxx }} .-{{ $op->opcion }}</h6>
                                                                </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                         <div id="pie{{ $preg->id.$op->id }}" style="height: 300px;"></div>
                                                                        </div> 
                                                                         <div class="col-md-5">                                                    
                                                                         <div id="tabla_resultado{{ $preg->id.$op->id }}">
                                                                        </div>                                          
                                                                         </div>                                                                     
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>                                       
                                                    </div>
                                                </div>                                                
                                                @endforeach    
                                    </div>
                            </div>
                        </div>
                    @endif



                    @if($preg->opciones_respuesta==0)
                        @php
                        $item_ant=$total_item;
                        $total_item++;
                        @endphp
                        @if($total_item % 3 == 0)                       
                        <div class="siguiente_pagina col-md-12">
                        @elseif($item_ant>0&&$item_ant % 3 == 0) 
                        <div class="inicio_pagina col-md-12"> 
                        @else
                        <div class="col-md-12">
                        @endif
                            <div class="card">
                                <div class="card-header">
                                <h5 class="card-title2">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h5></div>
                                <div class="card-body container-fluid">
                                    <div class="row row-lg">
                                        <div class="col-md-7">
                                            <!-- Opciones -->
                                            <div class="example-wrap">                                                      
                                                 <div class="card-body">
                                                    <div id="pie{{ $preg->id }}" style="height: 300px;"></div>
                                                </div>                            
                                            </div>
                                        </div>
                                        <div class="col-md-5">                                                    
                                                <div id="tabla_resultado{{ $preg->id }}">
                                                </div>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    @endif 
            @endforeach   
         
@endsection
@section('footer_scripts')
    <script src="{{asset('assets/vendors/echart/echarts.min.js')}}"></script>
<script>

var total_encuestados={{ $total }};
$(document).ready(function() {             
                    var datos='<?php echo $preguntas ?>';
                    datos=JSON.parse(datos);
                    for (var i = 0; i < datos.length; i++) {
                        if(datos[i].resultados){
                                var resultados=datos[i].resultados;   
                                if(datos[i].opciones_respuesta==0){ 
                                    var labelsx=[];
                                    var datasx=[];
                                    var colorsx=[];
                                    var columnas='';    
                                    var porcentaje_total=0;

                                    if(resultados.length>0){
                                          for (var j = 0; j < resultados.length; j++) {
                                                var col=randomColor();
                                                colorsx.push(col);
                                                datasx.push({value:parseInt(resultados[j].total),name:resultados[j].opcion});
                                                var porcentaje=(parseInt(resultados[j].total)/parseInt(datos[i].total_respuestas))*100;
                                                porcentaje=parseFloat(porcentaje.toFixed(2));
                                                porcentaje_total+=porcentaje;            
                                                columnas+=`
                                                <tr>
                                                <td style="font-size:9px;">${resultados[j].opcion}<span class="dot" style=" background-color:  ${col} ;"></span></td>
                                                <td align="right" style="font-size:10px;">${parseInt(resultados[j].total)}</td>
                                                <td align="right" style="font-size:10px;">${porcentaje} %</td>
                                                </tr>
                                                `;      
                                            }
                                    }else{
                                        datasx.push({value:0,name:"Sin datos"});
                                    }    
                                   
                       
                                    porcentaje_total=Math.round(porcentaje_total).toFixed(2);  
                                    var tabla=`<table class="table">
                                                 <thead>
                                                    <tr>
                                                    <th>Opción</th>
                                                    <th align="right">Valor</th>
                                                    <th align="right">%</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                ${columnas}
                                                </tbody>
                                                <tfooter>
                                                <tr>
                                                <td>TOTAL</td>
                                                <td align="right" style="font-size:10px;">${parseInt(datos[i].total_respuestas)}</td>
                                                <td align="right" style="font-size:10px;">${porcentaje_total} %</td>
                                                </tr>
                                                </table>
                                     `;
                                     $('#tabla_resultado'+datos[i].id).html(tabla);

                                    var D=document.getElementById("pie"+datos[i].id);
                                    if(D)
                                    {
                                        var T=echarts.init(D);
                                        T.setOption({
                                            color:colorsx,
                                            grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                                            xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                            yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                            tooltip:{
                                                trigger:"item",
                                                formatter:"{a} <br/>{b} : {c} ({d}%)"},
                                            series:[
                                            {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                                            data:datasx,
                                                itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                                            ]
                                        }),
                                        $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
                                    }
                                }else{                                    

                                    for (var j = 0; j < resultados.length; j++) {                           
                                        if(resultados[j].resultados){
                                                var labelsx=[];
                                                var datasx=[];
                                                var colorsx=[];
                                                var result=resultados[j].resultados;
                                                var resultados_opciones=[];

                                                var columnas='';    
                                                var porcentaje_total=0;  
                                                 if(result.length>0){

                                                    for (var k = result.length - 1; k >= 0; k--) {                                                  
                                                    var col=randomColor();
                                                    datasx.push({value:parseInt(result[k].total),name:result[k].opcion});
                                                    colorsx.push(col);
                                                    resultados_opciones.push({total:result[k].total,opcion:result[k].opcion,color:col,id:result[k].id});


                                                    var porcentaje=(parseInt(result[k].total)/parseInt(resultados[j].total_respuestas))*100;
                                                    porcentaje=parseFloat(porcentaje.toFixed(2));
                                                    porcentaje_total+=porcentaje;            
                                                    columnas+=`
                                                    <tr>
                                                    <td style="font-size:9px;">${result[k].opcion}<span class="dot" style=" background-color:  ${col} ;"></span></td>
                                                    <td align="right" style="font-size:10px;">${parseInt(result[k].total)}</td>
                                                    <td align="right" style="font-size:10px;">${porcentaje} %</td>
                                                    </tr>
                                                    `;  
                                                    }   
                                                 }else{
                                                    resultados_opciones.push({total:0,opcion:"Sin Datos",color:col});
                                                    datasx.push({value:0,name:"Sin datos"});
                                                 }
  
                                            
                                                var selector="pie"+datos[i].id+""+resultados[j].id;     

                                                porcentaje_total=Math.round(porcentaje_total).toFixed(2);  
                                                var tabla=`<table class="table">
                                                             <thead>
                                                                <tr>
                                                                <th>Opción</th>
                                                                <th align="right">Valor</th>
                                                                <th align="right">%</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            ${columnas}
                                                            </tbody>
                                                            <tfooter>
                                                            <tr>
                                                            <td>TOTAL</td>
                                                            <td align="right" style="font-size:10px;">${parseInt(resultados[j].total_respuestas)}</td>
                                                            <td align="right" style="font-size:10px;">${porcentaje_total} %</td>
                                                            </tr>
                                                            </table>
                                                 `;

                                                $('#tabla_resultado'+datos[i].id+""+resultados[j].id).html(tabla);


                                          
                                                var D=document.getElementById(selector);
                                                if(D)
                                                {
                                                    var T=echarts.init(D);
                                                    T.setOption({
                                                        color:colorsx,
                                                        grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                                                        xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                                        yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                                        tooltip:{
                                                            trigger:"item",
                                                            formatter:"{a} <br/>{b} : {c} ({d}%)"},
                                                        series:[
                                                        {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                                                        data:datasx,
                                                            itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                                                        ]
                                                    }),
                                                    $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
                                                }
                                        }
                                    }  
                                }                                             
                        }
                    }              

});
function randomColor()
{
  return 'rgba('+randomColorFactor()+','+randomColorFactor()+','+randomColorFactor()+',.7)';
}

function randomColorFactor()
{
  return Math.round(Math.random()*255);
}
</script>
@endsection