
@extends('layouts.master')
{{-- Page title --}}
@section('title')
    Resultados de encuestas @parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- page vendors -->
<link href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.dataTables.css')}}" rel="stylesheet">
            <link href="{{ asset('assets/css/pages/datatables.css')}}" rel="stylesheet">
                <!--end of page vendors -->
                <link href="{{asset('assets/plugins/jquery-colorbox/example1/colorbox.css')}}" rel="stylesheet"/>
                <link href="{{asset('assets/plugins/jsTree/style.css')}}" rel="stylesheet">
                    @stop
@section('content')
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Encuestas
                        </h1>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card filterable">
                                    <div class="card-header clearfix">
                                        <h3 class="card-title pull-left m-t-6">
                                            <i class="ti-control-shuffle">
                                            </i>
                                            Resultados de Encuestas
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table cellspacing="0" class="table" id="tables3" width="100%">
                                                <thead>
                                                    <tr class="filters">
                                                        <th scope="col">
                                                            NOMBRE
                                                        </th>
                                                        <th scope="col">
                                                            VER
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($encuestas))
                                        @foreach ($encuestas as $rol)
                                        <tr>
                                                    <td>
                                                        {{ $rol->nombre }}
                                                    </td>
                                                    <td align="center">
                                                        <a href="{{ url('resultados/'.encrypt($rol->id)) }}">
                                                            <i class="fa fa-search text-success actions_icon" title="Ver resultados" style="font-size: 200%;">
                                                            </i>
                                                        </a>
                                                        @if(Auth::user()->rol_id==1||Auth::user()->rol_id==5)
                                                         <a href="{{ url('resultados_general/'.encrypt($rol->id)) }}">
                                                            <i class="fa fa-search text-info actions_icon" title="Ver resultados Generales" style="font-size: 200%;">
                                                            </i>
                                                        </a>
                                                        @endif
                                                    </td>
                                                    </tr>
                                        @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

@endif
@stop
@section('footer_scripts')
<!--   page level js -->
<script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap4.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/advanced_tables.js')}}" type="text/javascript">
</script>
<!-- end of page level js -->
<script src="{{asset('assets/plugins/jquery-colorbox/jquery.colorbox.js')}}" type="text/javascript">
</script>
<script src="{{asset('assets/plugins/jsTree/jstree.min.js')}}">
</script>
<script type="text/javascript">
    'use strict';
  var table = $('#tables3').DataTable({
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
  
});
 
</script>
@stop
