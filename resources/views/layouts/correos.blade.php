<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
            <title>
                @yield('titulo')
            </title>
        </meta>
    </head>
    <body>
        <center>
            <div>
                <!--<div style="width: 600px; padding: 15px; border-left: 1px solid #B4B5B5;">
			
		</div>-->
                <div style="background-color: white; width: 600px; text-align : justify; padding: 15px; border-left: 1px solid #B4B5B5;  border-top: 1px solid #B4B5B5;  border-right: 1px solid #B4B5B5;   ">
                    <table border="0" cellpadding="0" cellspacing="0" style="margin:0px auto 0px auto; font-size:14px; font-family:Helvetica,Arial,Sans-serif; color:#333; padding:10px 20px 0 20px" width="600">
                        <tbody>
                            <tr style="vertical-align:bottom">
                                {{-- 
                                <th style="text-align:left; padding-bottom:10px; vertical-align:bottom; width:50%">
                                    <img data-connectorsauthtoken="1" data-imageproxyendpoint="/actions/ei" data-imageproxyid="" data-imagetype="External" originalsrc="http://assets.tuenti.net/2WQrc11IBclUAAPCg" src="{{$message->embed('assets/img/logo_white.png')}}"/>
                                </th>--}}
                                <th style="text-align:right; padding-bottom:10px; vertical-align:bottom; width:50%">
                                    <p style="margin:0px; padding:0px; font-size:12px; font-weight:normal; color:#666">
                                        <strong>
                                            {{ date('Y-m-d') }}
                                        </strong>
                                    </p>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <h2>
                        @yield('encabezado')
                    </h2>
                    @yield('contenido')
				@yield('pie')
                </div>
                <div style="width: 600px; padding: 15px; border-left: 1px solid #B4B5B5;   border-right: 1px solid #B4B5B5; border-bottom: 1px solid #B4B5B5;   ">
                </div>
            </div>
        </center>
    </body>
</html>