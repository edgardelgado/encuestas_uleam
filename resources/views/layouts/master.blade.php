<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
            <meta content="width=device-width,initial-scale=1" name="viewport">
                <meta content="ie=edge" http-equiv="X-UA-Compatible">
                    <!-- CSRF Token -->
                    <meta content="{{ csrf_token() }}" name="csrf-token">
                        <title>
                            @yield('title')
            | ENCUESTAS ULEAM
                        </title>
                        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                            <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
                            <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" type="text/css"/>
                            <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css">
                                @yield('header_styles')
                                <link href="{{ asset('carga_ajax.css') }}" rel="stylesheet">
                                </link>
                            </link>
                        </meta>
                    </meta>
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        @include('layouts.header-menu')
      @include('layouts.sidebar')
        <div class="modal carga_ajax" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-sm" role="document">
                <br>
                    <br>
                        <br>
                            <br>
                                <br>
                                    <div class="modal-body">
                                        <div id="floatingBarsG">
                                            <div class="blockG" id="rotateG_01">
                                            </div>
                                            <div class="blockG" id="rotateG_02">
                                            </div>
                                            <div class="blockG" id="rotateG_03">
                                            </div>
                                            <div class="blockG" id="rotateG_04">
                                            </div>
                                            <div class="blockG" id="rotateG_05">
                                            </div>
                                            <div class="blockG" id="rotateG_06">
                                            </div>
                                            <div class="blockG" id="rotateG_07">
                                            </div>
                                            <div class="blockG" id="rotateG_08">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                        <div id="letrero_carga" style="color: white; font-size: 2em; text-align: center; font-style: italic; font-weight: bold;">
                                        </div>
                                    </br>
                                </br>
                            </br>
                        </br>
                    </br>
                </br>
            </div>
        </div>
        <div id="reporte" style="display: none;"></div>
        <script src="{{asset('assets/js/app.js')}}" type="text/javascript">
        </script>
        @yield('footer_scripts')
        @yield('bottom-js')
        <script type="text/javascript">
            function carga_ajax(letrero='Cargando ...') {
            $('#letrero_carga').html(letrero);
           $('.carga_ajax').modal({
            show: true,
            keyboard: false,
            backdrop: 'static'
           });
          }




          function desaparecer_carga_ajax() {
           $('.carga_ajax').modal('hide');
          }
        </script>
    </body>
</html>
