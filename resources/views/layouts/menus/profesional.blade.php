
@php 
$rol_actual=Auth::user()->rol_id;
             if ($rol_actual==1)
              {   
                $tabla=App\MenuOpcionesModel::where("visible","SI")
                ->orderby("orden","Asc")->get();
           
            }
            else
            {   
                 $tabla=App\menuModel::join("menu_rol as b","menu.id","=","b.idmenu")

                      ->select("menu.*")
                      ->where("b.idrol",$rol_actual)
                     ->where("visible",'SI')
                      ->orderby("menu.orden")
                      ->get();
            }
            $nivel1=$tabla->where('nivel',1);
            $nivel2=$tabla->where('nivel',2);            
            @endphp
                <ul class="navigation">
                    <li {!! (Request::is('inicio')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('inicio') }} ">
                            <i class="menu-icon fa fa-fw fa-home"></i>
                            <span class="mm-text ">Inicio</span>
                        </a>
                    </li>
                    @foreach($nivel1 as $niv1)
                     @php
                            $submenus=$nivel2->where('idmain',$niv1->id);
                            $submenus_mostrar=null;
                            $clase_lista='';
                            $dire_men="#";
                            $menus_rutas="";
                            if (count($submenus)) {
                              foreach ($submenus as $submen) {
                                $menus_rutas.='"'.$submen->ruta.'",';
                              }

                              $menus_rutas=trim($menus_rutas, ',"');
                            }else{
                              $dire_men=url($niv1->ruta);
                            } 
                      @endphp

                            @if(count($submenus))
                            <li  {!! (Request::is($menus_rutas) ? 'class="active"' : '') !!}>
                                  <a href="javascript:;">
                                      <i class="menu-icon {{ $niv1->iconfaw }}"></i>
                                      <span>{{ $niv1->menu }}</span>
                                      <span class=""></span>
                                  </a>
                                  <ul class="sub-menu">
                                    @foreach($submenus as $submenu)
                                     <li {!! (Request::is($submenu->ruta) ? 'class="active"' : '') !!}>
                                          <a href="{{ URL::to($submenu->ruta) }}">
                                              <i class="fa-fw {{ $submenu->iconfaw }}"></i> {{ $submenu->menu }}
                                          </a>
                                      </li>
                                    @endforeach
                                  </ul>
                              </li>
                            @else
                            <li {!! (Request::is($dire_men)? 'class="active"':"") !!}>
                                <a href="{{ $dire_men }}">
                                    <i class="menu-icon fa fa-fw {{ $niv1->iconfaw }}"></i>
                                    <span class="mm-text ">{{ $niv1->menu }}</span>
                                </a>
                            </li>
                            @endif
                    @endforeach