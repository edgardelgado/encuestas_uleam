<header class="header">
    <nav class="navbar navbar-expand-lg navbar-light navbar-static-top" role="navigation">
        <a class="logo navbar-brand mr-0" href="{{ url('inicio') }}">
            <img alt="logo" src="{{asset('assets/img/logo_blue.png')}}"/>
        </a>
        <div>
            @if(Auth::user()->rol_id!=3)
            <a class="navbar-btn mr-sm-auto sidebar-toggle" data-toggle="offcanvas" href="javascript:void(0)" role="button">
                <i class="fa fa-fw fa-bars">
                </i>
            </a>
            @endif
        </div>
        <div class="navbar-right ml-auto">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu nav-item dropdown">
                    <a class="dropdown-toggle padding-user nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" id="userDropdown">
                        <img alt="User Image" class="rounded-circle img-fluid pull-left" src="{{asset('assets/img/authors/user.jpg')}}">
                            <div class="riot">
                                <span class="fa fa-sort-down caret">
                                </span>
                            </div>
                        </img>
                    </a>
                    <ul aria-labelledby="navbarDropdown" class="dropdown-menu">
                        <li class="user-name text-center">
                            <span>
                                {{ Auth::user()->nombres }}
                            </span>
                        </li>
                         @if(Auth::user()->rol_id!=3)
                        <li class="p-t-3">
                            <a class="dropdown-item" href="{{ url('perfil') }}">
                                Perfil
                                <i class="fa fa-fw fa-user pull-right mt-1">
                                </i>
                            </a>
                        </li>
                         @endif

                        <li>
                            <a class="dropdown-item" href="{{ url('logout') }}">
                                Cerrar sesión
                                <i class="fa fa-fw fa-sign-out pull-right mt-1">
                                </i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>