<div class="wrapper">
    @if(Auth::user()->rol_id!=3)
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-aside">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar metismenu">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="javascript:void(0)">
                            <img alt="User Image" class="rounded-circle" src="{{asset('assets/img/authors/user.jpg')}}">
                            </img>
                        </a>
                        <div class="content-profile">
                            <h4 class="media-heading">
                                {{ Auth::user()->nombres }}
                            </h4>
                            <p>
                                {{ Auth::user()->rol->descripcion }}
                            </p>
                        </div>
                    </div>
                </div>
                @include('layouts.menus.dinamico')
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    @endif
    <aside class="right-aside view-port-height">
        @yield('content')
    </aside>
</div>