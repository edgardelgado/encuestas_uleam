<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
       <title>
        @yield('title')
            | ENCUESTAS ULEAM
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="{{ asset('assets/img/logo_bl.png')}}"/>
    <link type="text/css" href="{{asset('assets/css/app.css')}}" rel="stylesheet"/>
     @yield('header_styles')
     <link rel="stylesheet" href="{{ asset('carga_ajax.css') }}">
</head>
<body>
@yield('content')
<script src="{{asset('assets/js/app.js')}}" type="text/javascript"></script>
@yield('footer_scripts')
@yield('bottom-js')
</body>
</html>
