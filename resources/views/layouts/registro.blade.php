<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro al Sistema | Encuesta</title>
            <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
    {!! Html::style('admin/plantilla/css/bootstrap.min.css') !!}
    
    
    {!! Html::style('admin/plantilla/css/style.css') !!}
    
      <link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">






         <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
                     <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
  </head>
    <style>
 .log  {
       
     margin-top: -50%;
     margin-left: -5%;
}
 
   body { 
  background: url('raiz/img/header-bg.png') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.panel-default {
opacity: 0.9;
margin-top:30px;
}
.form-group.last { margin-bottom:0px; } 

.login100-form-btn {
  
    font-size: 15px;
    line-height: 1.5;
    color: #fff;
    text-transform: uppercase;
    
    border-radius: 25px;
    background: #5cb65f;
    
    </style> 
  <body class="j">
    <div class="middle-box text-center loginscreen animated fadeInDown" style="     width: 60%;max-width: 45%;">
      <div>
       
        <div class="ibox-content2" style="margin-top:  3%;">
          @include('alerts.request')
          @include('alerts.success')
          @include('alerts.errors')



          {!! Form::open(['url'=>'registro', 'method' => 'POST']) !!}
           {!! csrf_field() !!}
            <p style="font-size: 22px;color: #000000;" >Estimado graduado/a, Eres nuevo? Registrate aquí</p>
           
                          <!-- <img  class="foto1" src="public/Fecha/Fecha.jpg" alt="">-->
            <br>
            <div class="row">
                 <div class="col-md-12">
                        <label for="ciudad" style="color: #000000 ! important;">
                            Carrera
                        </label>
                       {!! Form::select('carrera_id',$carreras,null,['class'=>'form-control select2','placeholder'=>'Seleccione la carrera...']) !!}
                    </div>
            </div>

           <br>
          <div class="row">
             <div class="col-md-6">
                <label for="txtapellido" style="color: #000000 ! important;">Apellidos: </label>
                {!! Form::text('apellidos',old('apellidos'),['title'=>'Escriba sus dos apellidos','class'=>'form-control','placeholder'=>'Apellidos', 'onkeypress'=>'return letras(event)', 'maxlength'=>'100', 'id'=>'txtapellido']) !!}
              </div> 

               <div class="col-md-6">
                <label for="txtnombre" style="color: #000000 ! important;">Nombres: </label>
                {!! Form::text('nombres',old('nombres'),['title'=>'Escriba sus dos nombres','class'=>'form-control','placeholder'=>'Nombres', 'onkeypress'=>'return letras(event)', 'maxlength'=>'100', 'id'=>'txtnombre']) !!}

              </div>
            </div>


            <div class="row">
              <div class="col-md-6">
                <label for="txttelefono" style="color: #000000 ! important;">   Cédula, Ruc o Pasaporte (Sin guión): </label>
                 {!! Form::Text('identificacion',null,['class'=>'form-control','placeholder'=>'Ingrese Cédula, Ruc o Pasaporte','onkeypress'=>'return letras_numeros(event)']) !!}
              </div>

              <div class="col-md-6">
                <label for="txtmail" style="color: #000000 ! important;">Correo electrónico: </label>
                {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Correo electrónico', 'onkeypress'=>'return mail(event)', 'maxlength'=>'50','id' => 'txtmail']) !!}
              </div>
            </div>




            <div class="row">
              <div class="col-md-6">
                <label for="txttelefono" style="color: #000000 ! important;" >Número de teléfono: </label>
                {!! Form::text('telefono',old('telefono'),['title'=>'Teléfono fijo o celular','class'=>'form-control','placeholder'=>'Número de teléfono', 'onkeypress'=>'return numeros(event)', 'maxlength'=>'10','id' => 'txttelefono']) !!}
              </div>

              <div class="col-md-6">
                <label for="txtmail" style="color: #000000 ! important;">Fecha de graduación: </label>
               
                 {!!Form::Date('fecha_graduacion', $anios, ['class' => 'form-control','id'=>'fecha_graduacion' ,'placeholder'=>'Seleccione'])!!}

              </div>
            </div>


             <div class="row">
              <div class="col-md-6">
                <label for="txttelefono" style="color: #000000 ! important;">Periodos Académico </label>
                {!!Form::select('periodo_academico', $periodos,null, ['class' => 'select2','id'=>'periodos' ,'placeholder'=>'Seleccione el periodo Académico...'])!!}
              </div>

              <div class="col-md-6">
                <label for="txtmail" style="color: #000000 ! important;">Año de graduación : </label>
                {!!Form::select('anio_graduacion', $anios,null, ['class' => 'select2','id'=>'anios' ,'placeholder'=>'Seleccione el año de graduación...'])!!}

              </div>
            </div>
            <br>
            <!--<div class="row">
              <div class="col-md-6">
                <label for="txtclave">Contraseña: </label>
                {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña', 'id' => 'txtclave','required'] ) !!}
              </div>

              <div class="col-md-6">
                <label for="txtclave2">Confirmar Contraseña: </label>
                {!! Form::password('password2',['class' => 'form-control awesome','placeholder'=>'Vuelva a escribir la contraseña','id' => 'txtclave2','required']) !!}
              </div>
            </div> -->   
            <br>
            <div class="form-group has-feedback">
              {!!Form::submit('Registrar',['class'=>'btn btn-primary login100-form-btn', 'id'=>'btnregistro'])!!}
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>    
  <script src="{{ asset('raiz/js/vendor/jquery-2.2.4.min.js') }}"></script> 
  <script src="{{ asset('raiz/js/vendor/bootstrap.min.js') }}"></script>  
    

  <script src="{{ asset('admin/js/funciones.js') }}"></script>  
    
       
     <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(".select2").select2({width: "100%"});

    function letras_numeros(e) {
              tecla = (document.all) ? e.keyCode : e.which;
              if (tecla==8) return true;
              var patron =/[0-9a-zA-Z]/;
              var te = String.fromCharCode(tecla);
              return patron.test(te);
            }
</script>

</body>

</html>
