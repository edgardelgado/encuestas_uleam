@if(!is_null($resultado))
<div class="table-responsive">
    <table class="table table-bordered table-hover" style="color:black !important;">
        <thead>
            <tr>
                <th>
                    CÉDULA
                </th>
                <th>
                    APELLIDOS
                </th>
                <th>
                    NOMBRES
                </th>
                <th>
                    EMAIL
                </th>
                <th>
                    SOLICITAR CLAVE
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{ $resultado->cedula }}
                </td>
                <td>
                    {{ $resultado->apellidos }}
                </td>
                <td>
                    {{ $resultado->nombres }}
                </td>
                <td>
                    <input type="hidden" id="email_envio" value="{{ $resultado->email }}">
                    {{ $resultado->email }}
                </td>
                <td>
                    <a class="btn  btn-xs b" onclick="enviar_clave({{ $resultado->id }},'{{ $resultado->cedula }}')" style="color: white;background-color: #6c9924;">
                        <i class="fa fa-envelope ">
                            Solicitar
                        </i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@else
<center>
    <h3 style="color: white;">
        GRADUADO NO ENCONTRADO
    </h3>
</center>
@endif
