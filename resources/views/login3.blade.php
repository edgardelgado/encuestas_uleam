<!DOCTYPE html>
<html>
    <head>
        <title>
            Sistemas de Encuesta | Inicio de Sesión

        </title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
            <link href="{{ asset('assets/img/logo_bl.png')}}" rel="shortcut icon"/>
            
                <!--page level css -->
                <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
                    <link href="{{ asset('carga_ajax.css') }}" rel="stylesheet">
                        <link href="{{ asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet">
                            <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet"/>
                            <link href="{{ asset('assets/css/pages/login_register.css')}}" rel="stylesheet">
                                <link href="{{ asset('assets/selective/selectize.css')}}" rel="stylesheet">
                                    <link href="{{ asset('carga_ajax.css') }}" rel="stylesheet">
                                        <link href="{{asset('assets/css/app.css')}}" rel="stylesheet" type="text/css"/>
                                        <!--debes agregar fontawesome icon para que se pueda llamar los iconos fa fa-print etc-->
                               <link href="{{ asset('assets/vendors/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
                                <link href="{{ asset('assets/css/pages/toastr.css') }}" rel="stylesheet"/>
                                
                                    </link>
                                </link>
                            </link>
                        </link>
                    </link>
                </link>
            </link>
        </meta>
    </head>
    <style type="text/css">
        .b{
    border-radius: 20px;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
    border-bottom-left-radius: 20px;
  }
    </style>
    <body id="sign-in">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-8 col-10 m-auto signin-form">
                    <div class="panel-header">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-center">
                                    <img alt="Logo" src="{{ asset('assets/img/logo_white.png')}}">
                                    </img>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 m-auto signin-form">
                                <form action="login" class="form-horizontal" method="post">
                                    <input name="_token" type="hidden" value="<?php echo csrf_token(); ?>">
                                        <div class="">
                                            <center>
                                                <div class="form-group row margin-bottom-none">
                                                    <div class="col-sm-5" style="width: 90% ! important; left: 15% ! important; min-height :10% ! important;">
                                                        <input class="form-control form-control-sm b" id="datos" name="identificacion" placeholder="Ingrese su cédula sin guiones">
                                                        </input>
                                                    </div>
                                                    <div class="col-sm-3" style="width: 90% ! important; left: 12% ! important; min-height :10% ! important;">
                                                        <button class="btn margin_top btn-primary pull-right btn-block btn-sm b" onclick="return traerdatos();" type="submit">
                                                            <i class="fa fa-search">
                                                                Consultar
                                                            </i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </center>
                                            <div class="col-md-12" style="color:#364653 ! important;">
                                                <hr class="separator">
                                                    <div class="table-responsive" id="resultado">
                                                    </div>
                                                </hr>
                                            </div>
                                            <div class="col-md-10" id="div_clave" style="display: none;">
                                                <div class="col-sm-6" style="width: 520% ! important; left:35% ! important; min-height :10% ! important;">
                                                    <label for="password" style="width: 50% ! important; padding-left: 35% ! important;">
                                                        <strong>
                                                            Clave
                                                        </strong>
                                                    </label>
                                                    <input class="form-control form-control-sm b" id="password" name="password" placeholder="Ingrese su Clave" required="" style="color: #1b1b1d! important;" type="password">
                                                    </input>
                                                </div>
                                                <div class="col-sm-8" style="width: 20% ! important; left: 50% ! important; min-height :10% ! important;     padding: 2% ! important;">
                                                    <button class="btn margin_top btn-primary pull-right btn-block btn-sm b" type="submit">
                                                        Iniciar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </input>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<div class="modal carga_ajax" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm" role="document">
        <br>
            <br>
                <br>
                    <br>
                        <br>
                            <div class="modal-body">
                                <div id="floatingBarsG">
                                    <div class="blockG" id="rotateG_01">
                                    </div>
                                    <div class="blockG" id="rotateG_02">
                                    </div>
                                    <div class="blockG" id="rotateG_03">
                                    </div>
                                    <div class="blockG" id="rotateG_04">
                                    </div>
                                    <div class="blockG" id="rotateG_05">
                                    </div>
                                    <div class="blockG" id="rotateG_06">
                                    </div>
                                    <div class="blockG" id="rotateG_07">
                                    </div>
                                    <div class="blockG" id="rotateG_08">
                                    </div>
                                </div>
                            </div>
                            <br>
                                <div id="letrero_carga" style="color: white; font-size: 2em; text-align: center; font-style: italic; font-weight: bold;">
                                </div>
                            </br>
                        </br>
                    </br>
                </br>
            </br>
        </br>
    </div>
</div>
<script src="{{asset('assets/js/app.js')}}" type="text/javascript">
</script>
<!-- begining of page level js -->
<script src="{{ asset('assets/js/jquery.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/bootstrap.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/vendors/iCheck/js/icheck.js')}}">
</script>
<script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript">
</script>
<script src="{{ asset('assets/js/pages/login_register.js')}}">
</script>
<script src="{{ asset('assets/selective/selectize.js')}}">
</script>

<script src="{{ asset('assets/vendors/toastr/js/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/ui-toastr.js') }}"></script>

<!-- end of page level js -->
<script type="text/javascript">
    datos= $('#identificacion').selectize({  

//    valueField: 'id', 
  //  labelField: 'value',  
   // searchField: 'value', 
    render: {
        option: function(item, escape) {
            return '<div>' +             
                '<span class="description">' + escape(item.value) + '</span>' +               
            '</div>';
        },
    },
    score: function(search) {
        var score = this.getScoreFunction(search);
        return function(item) {
            return score(item);
        };
    },
    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '{{url('consulta_usuario')}}/' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                console.log(res);
                callback(res.slice(0, 10));
            }
        });
    }
});




        function traerdatos() {


          $('#password').val(null);
                  $('#div_clave').css('display','none');

             var identificacion = $('#datos').val();
             //verifico si ha ingresado algo en cedula
             if(identificacion==null||identificacion==""){
                //si no ha ingresado nada no hara nada cuando presione click en buscar
                return 1;
             }

             carga_ajax('Consultando datos...');


             //en caso de que si haya ingresado correctamente


             var resultado_validacion=cedula(identificacion);
             if(resultado_validacion!=5){
                //cedula incorrecta
                return alert("Cedula ingresada es incorrecta");
             }

             carga_ajax("Consultando graduado...");


             //si ha ingresado una cedula correcta entonces se hace la consulta al server

            $.ajax({
                url: '{{url('consulta_usuario')}}/' +identificacion,
                type: 'GET',
                error: function(error) {
                    console.log(error);
                },
                success: function(res) {
                    $('#resultado').html(res);
                }
            }).always(function(){
              desaparecer_carga_ajax();
            });



        }

//funcion para validar cedula
    function cedula(num_cedula) {
    var cedula = num_cedula;

    //Preguntamos si la cedula consta de 10 digitos
    if(cedula.length == 10){

        //Obtenemos el digito de la region que sonlos dos primeros digitos
        var digito_region = cedula.substring(0,2);

        //Pregunto si la region existe ecuador se divide en 24 regiones
        if( digito_region >= 1 && digito_region <=24 ){

          // Extraigo el ultimo digito
          var ultimo_digito   = cedula.substring(9,10);

          //Agrupo todos los pares y los sumo
          var pares = parseInt(cedula.substring(1,2)) + parseInt(cedula.substring(3,4)) + parseInt(cedula.substring(5,6)) + parseInt(cedula.substring(7,8));

          //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
          var numero1 = cedula.substring(0,1);
          var numero1 = (numero1 * 2);
          if( numero1 > 9 ){ var numero1 = (numero1 - 9); }

          var numero3 = cedula.substring(2,3);
          var numero3 = (numero3 * 2);
          if( numero3 > 9 ){ var numero3 = (numero3 - 9); }

          var numero5 = cedula.substring(4,5);
          var numero5 = (numero5 * 2);
          if( numero5 > 9 ){ var numero5 = (numero5 - 9); }

          var numero7 = cedula.substring(6,7);
          var numero7 = (numero7 * 2);
          if( numero7 > 9 ){ var numero7 = (numero7 - 9); }

          var numero9 = cedula.substring(8,9);
          var numero9 = (numero9 * 2);
          if( numero9 > 9 ){ var numero9 = (numero9 - 9); }

          var impares = numero1 + numero3 + numero5 + numero7 + numero9;

          //Suma total
          var suma_total = (pares + impares);

          //extraemos el primero digito
          var primer_digito_suma = String(suma_total).substring(0,1);

          //Obtenemos la decena inmediata
          var decena = (parseInt(primer_digito_suma) + 1)  * 10;

          //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
          var digito_validador = decena - suma_total;

          //Si el digito validador es = a 10 toma el valor de 0
          if(digito_validador == 10)
            var digito_validador = 0;

          //Validamos que el digito validador sea igual al de la cedula
          if(digito_validador == ultimo_digito){
           // console.log('la cedula es correcta');
            return 5;
          }else{
          //  console.log('la cedula:' + cedula + ' es incorrecta');
            return 4;
          }
        }else{
          // imprimimos en consola si la region no pertenece
         // console.log('Esta cédula no pertenece a ninguna region');
          return 6;
        }
    }else{
        //imprimimos en consola si la cedula tiene mas o menos de 10 digitos

       // console.log('Esta cédula tiene menos de 10 Digitos');
        return 10;
    }
}


function enviar_clave(id_usuario){
  carga_ajax("Solicitando clave de acceso...");

            $.ajax({
                url: '{{url('enviar_clave')}}',
                type: 'POST',
                data: {_token:"{{ csrf_token() }}",id_usuario},
                error: function(error) {
                  $('#password').val(null);
                  $('#div_clave').css('display','none');
                    console.log(error);
                },
                success: function(res) {
                  $('#password').val(null);
                  $('#div_clave').css('display','block');

                toastr['success']('Su contraseña se ha enviado al su email');
                  console.log(res);
                }
            }).always(function(){
              desaparecer_carga_ajax();

            });
}



function iniciar(){
var identificacion= $("identificacion").val();
var password= $("password").val();
            $.ajax({
                url: '{{url('/')}}',
                type: 'POST',
                data: {_token:"{{ csrf_token() }}"},
                error: function(error) {
                
                    console.log(error);
                },
                success: function(res) {
                toastr['success']('inico');
                   console.log(res);
                }

            });
}

function carga_ajax(letrero='Cargando ...') {
  $('#letrero_carga').html(letrero);
 $('.carga_ajax').modal({
  show: true,
  keyboard: false,
  backdrop: 'static'
 });
}




function desaparecer_carga_ajax() {
 $('.carga_ajax').modal('hide');
}
</script>
<script>
    @if(Session::has('message'))
    //success
    toastr['success']('{{Session::get('message')}}'); 
    @endif

    @if(Session::has('message-error'))
    //error
    toastr['error']('{{Session::get('message-error')}}'); 
    @endif

    @if(count($errors) > 0)
    //errorees de fornmularios
    <?php
    $resultado='<ul>';
    foreach ($errors->all() as $error) {
        $resultado.="<li>$error</li>";
    }
    $resultado.='</ul>';
    ?>
    toastr['error']('<?php echo $resultado; ?>');               
    @endif
</script>
