<?php Autorizar(Request::path()); ?> 
@extends('layouts.desnuda')
@section('header_styles')
<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.bubble.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.snow.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-colorbox/example1/colorbox.css')}}" />
@endsection
@section('title')
    {{ $pregunta->pregunta}} @parent
@stop
@section('titulo',$pregunta->titulo_pregunta)
@section('content')
<div class="breadcrumb">
    <h1>{{$pregunta->pregunta}}</h1>        
</div>
<?php
$total=$resultados->sum('total');
?>
<div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ intval($pregunta->order) }}) {{ $pregunta->pregunta }}</h3>
                </div>
            </div> 
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">GRÁFICO DE PASTEL </h3></div>                                
                    <div class="card-body container-fluid">
                        <div class="row row-lg">
                            <div class="col-md-8 col-lg-8">
                                <div class="example-wrap">                                                      
                                     <div class="card-body">
                                        <div id="pie" style="height: 300px;"></div>
                                    </div>                            
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="example-wrap">                                                      
                                     <div class="card-body" id="tabla_resultado">
                                        
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
                        
    </div>  
    <style type="text/css">
        .ct-chart .ct-label.ct-horizontal {
  text-align: left;

}

 .dot {
  height: 15px;
  width: 15px;
  border-radius: 100%;
 display: inline-block;
 margin-left: 1%;
 margin-top: 1%;
}
    </style>          
@endsection
@section('footer_scripts')
    <script src="{{asset('assets/vendors/echart/echarts.min.js')}}"></script>
    <script  src="{{asset('assets/plugins/jquery-colorbox/jquery.colorbox.js')}}" type="text/javascript"></script>
<script>


    $(document).ready(function() {
        var datos='<?php echo $resultados; ?>';
        var total_encuestados='<?php echo $total; ?>';
        datos=JSON.parse(datos);
        total_encuestados=parseInt(total_encuestados);

        if(datos.length>0){

            var tabla='<table class="table">';
            tabla+='<thead>';
            tabla+='<tr>';
            tabla+='<th>Opción</th>';
            tabla+='<th align="right">Valor</th>';
            tabla+='<th align="right">%</th>';
            tabla+='</tr>';
            tabla+='</thead>';
            tabla+='<tbody>';                                           
                                            
            var labelsx=[];
            var datasx=[];
            var colorsx=[];
            var porcentaje_total=0;
            for (var i = 0; i < datos.length; i++) {
                var color=randomColor();
                var porcentaje=(parseInt(datos[i].total)/total_encuestados)*100;
                porcentaje=parseFloat(porcentaje.toFixed(2));
                porcentaje_total+=porcentaje;
                tabla+='<tr>';
                tabla+='<td style="font-size:9px;">'+datos[i].opcion+'<span class="dot" style=" background-color:  '+color+' ;"></span></td>';
                tabla+='<td align="right" style="font-size:9px;">'+parseInt(datos[i].total)+'</td>';
                tabla+='<td align="right" style="font-size:9px;">'+porcentaje+' %</td>';
                colorsx.push(color);
                datasx.push({value:parseInt(datos[i].total),name:datos[i].opcion});
                tabla+='</tr>';
            }            
            tabla+='</tbody>';
            tabla+='</tbody>';
            tabla+='<tfooter>';
            tabla+='<tr><td>TOTAL</td><td align="right">'+total_encuestados+'</td><td align="right">'+porcentaje_total+'%</td>';
            tabla+='</tfooter>';

            $('#tabla_resultado').html(tabla);
            var D=document.getElementById("pie");
            if(D)
            {
                var T=echarts.init(D);
                T.setOption({
                    color:colorsx,
                    grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                    xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                    yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                    tooltip:{
                        trigger:"item",
                        formatter:"{a} <br/>{b} : {c} ({d}%)"},
                    series:[
                    {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                    data:datasx,
                        itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                    ]
                }),
                $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
            }
        }
    });

function randomColor()
{
  return 'rgba('+randomColorFactor()+','+randomColorFactor()+','+randomColorFactor()+',.7)';
}

function randomColorFactor()
{
  return Math.round(Math.random()*255);
}
</script>
@endsection