@extends('layouts.master')
@section('header_styles')
@endsection
@section('title')
    {{ $encuesta->nombre}} @parent
@stop
@section('titulo',$encuesta->nombre)
@section('content')
    <div class="breadcrumb" style="margin-top:1%;">
        <h1>
            {{ $encuesta->nombre }}
        </h1>
    </div>
    <div class="separator-breadcrumb border-top">
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url'=>'responder_encuesta_post', 'method' => 'POST','id'=>'formulario_responder']) !!}
            <input name="id" type="hidden" value="{{ $encuesta->id }}">
                @foreach($preguntas as $preg)
                    @php
                    $opciones_prin=null;
                    if($preg->opciones_respuesta==1){
                        $opciones_prin=App\opcionModel::where('pregunta_principal_id',$preg->id)->get();
                    }
                    @endphp
                <div class="card" id="p-{{ $preg->order }}">
                    <div class="card-header">
                        <h3 class="card-title">
                            {{ intval($preg->order) }}) {{ $preg->pregunta }}
                        </h3>
                    </div>
                    <div class="card-body container-fluid">
                        <div class="row row-lg">
                            <div class="col-md-12">
                                <!-- Opciones -->
                                <div class="example-wrap">
                                    <h4 class="example-title">
                                        {{ $preg->titulo_respuesta }}
                                    </h4>
                                    @if($preg->opciones_respuesta==0)
                                    @php
                                    $opa=0;
                                    @endphp
                                        @foreach($preg->opciones as $op)
                                        @php
                                        $opa++;
                                        @endphp
                                    <div class="radio-custom radio-primary p-t-3">
                                        @if(is_null($op->preguntas_alterar))
                                        <input id="{{ $preg->id }}" name="p{{ $preg->id }}" class="p{{ $preg->order }}" type="radio" value="{{ $op->id }}" required>
                                        @else
                                        <input id="{{ $preg->id }}" name="p{{ $preg->id }}" class="p{{ $preg->order }}" type="radio" value="{{ $op->id }}" onclick="accion_pregunta('{{ $op->preguntas_alterar }}','{{ $op->display }}');" required>
                                        @endif
                                            <label for="{{ $preg->id }}">
                                                {{ $opa }}) {{ $op->opcion }}
                                            </label>
                                        </input>
                                    </div>
                                    @endforeach  
                                    @endif    
                                    @if($preg->opciones_respuesta==1)
                                    <div class="table-responsive">
                                    <table border="1" class="table">
                                        <thead>
                                            <tr>
                                                <th width="40%">
                                                    DETALLE
                                                </th>
                                                @foreach($opciones_prin as $oprin)
                                                <th style="font-size: 10px; text-align: center;" width="12%">
                                                    {{ $oprin->opcion }}
                                                </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $opa=0;
                                            @endphp
                                            @foreach($preg->opciones as $op)
                                            @php
                                            $opa++;
                                            @endphp
                                            <tr>
                                                <td width="40%" style="font-size: 90%;">
                                                    {{ $opa }}. {{ $op->opcion }}
                                                </td>                                            
                                                @foreach($opciones_prin as $oprin)
                                                <td class="td_radios">
                                                <input style="margin-top: 10%  ! important;" id="{{ $op->id }}" name="o{{ $op->id }}" type="radio" value="{{ $oprin->id }}" required>                                    
                                                </div>                                                    
                                                </td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="form-group p-12">
                    <div class="ml-auto col-md-12">
                        <center>
                            <button class="btn btn-primary" type="submit" onclick="responder_encuesta()" id="btn_resp">
                                <i class="fa fa-save">
                                </i>
                                Responder Encuesta
                            </button>
                        </center>
                    </div>
                </div>
                {!! Form::close() !!}
            </input>
        </div>
    </div>
    <style type="text/css">

.td_radios {
    justify-content: center;
    align-items: center;
    text-align: center;
    vertical-align: center;
}
    </style>
    @endsection
@section('footer_scripts')
<script src="{{asset('js/underscore.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(setup);

        function setup () {
            $("#formulario_responder").on("submit", function () {
                $('#btn_resp').attr('disabled', true);
            });
        }
        preguntas_todas=JSON.parse(preguntas_todas);
        function responder_encuesta(){  
            $("#formulario_responder").validate({
              submitHandler: function(form) {
                form.submit();
              }
            });         
        }

        function accion_pregunta(preguntas,display){
        var boleano_seleccionado=[];
        var boleano_requerido=[];
        boleano_seleccionado['none']=false;
        boleano_seleccionado['block']=false;
        boleano_requerido['none']=false;
        boleano_requerido['block']=true;
        preguntas=preguntas.split('|');
        $.each(preguntas, function( index, value ) {
          var pregunta_numero=parseInt(value);
          $('#p-'+pregunta_numero).css('display',display);
            document.querySelectorAll('.p'+pregunta_numero).forEach(function(x) { 
                x.checked=boleano_seleccionado[display];
                x.required=boleano_requerido[display];
            });
        });

        }
    </script>
@endsection