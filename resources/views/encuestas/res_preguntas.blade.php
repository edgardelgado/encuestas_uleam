              @foreach($preguntas as $preg)

                      @if($preg->opciones_respuesta==1)
                          @php
                              $opciones_prin=App\opcionModel::where('pregunta_principal_id',$preg->id)->get();
                          @endphp
                          <div class="col-md-12">
                              <div class="card">
                                  <div class="card-header" >
                                      <h3 class="card-title">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h3></div>
                                  <div class="card-body container-fluid">
                                      <div class="row">
                                              <!-- Opciones -->
                                                  @php

                                                  $opxx=0;
                                                  @endphp
                                                  @foreach($preg->opciones as $op)
                                                    @php
                                                  $opxx++;
                                                  @endphp
                                                  <div class="col-md-12">
                                                      <div class="example-wrap">
                                                      <a onclick="mostrar_info({{ $preg->id }},{{ $op->id }})">
                                                              <div class="card">
                                                                  <div class="card-header">
                                                                      <h6>{{ intval($preg->order) }}.{{ $opxx }} .-{{ $op->opcion }}</h6>
                                                                  </div>
                                                                  <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                           <div id="pie{{ $preg->id.$op->id }}" style="height: 300px;"></div>
                                                                          </div> 
                                                                          <div class="col-md-5">                                                    
                                                                             <div id="tabla_resultado{{ $preg->id.$op->id }}">
                                                                            </div>                                        
                                                                         </div>                                                                   
                                                                    </div>
                                                                  </div>
                                                              </div>



                                                      </a>
                                                      </div>
                                                  </div>
                                                  @endforeach
                                      </div>
                                  </div>
                              </div>
                          </div>
                      @endif



                    @if($preg->opciones_respuesta==0)
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                <h3 class="card-title">{{ intval($preg->order) }}) {{ $preg->pregunta }} </h3></div>
                                <a onclick="mostrar_info({{ $preg->id }},{{ $preg->opciones_respuesta }})">
                                <div class="card-body container-fluid">
                                    <div class="row row-lg">
                                        <div class="col-md-8">
                                            <!-- Opciones -->
                                            <div class="example-wrap">                                                      
                                                 <div class="card-body">
                                                    <div id="pie{{ $preg->id }}" style="height: 300px;"></div>
                                                </div>                            
                                            </div>
                                        </div>
                                         <div class="col-md-4">                                                    
                                                <div id="tabla_resultado{{ $preg->id }}">
                                                </div>                                        
                                        </div>
                                    </div> 
                                </div>                             
                                </a>
                              </div>
                        </div>
                    @endif                  
            @endforeach              
