@extends('layouts.master')
@section('header_styles')

<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.bubble.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/echart/quill.snow.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/alertifyjs/css/alertify.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/js/alertifyjs/css/themes/alertify.core.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendors/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap4.css')}}">
@endsection

@section('title')
    {{ $encuesta->nombre}} @parent
@stop
@section('titulo',$encuesta->nombre)
@section('content')
        <div class="breadcrumb" >
            <h1>
                {{ $encuesta->nombre }}
            </h1>
        </div>
        <div class="separator-breadcrumb border-top" >
        </div>
        <div class="row">
            <div class="col-md-12">
                    <div class="card" >
                        <div class="card-header">                            
                            <div class="row" id="filtros">
                                <div class="form-group col-md-3">
                                    <label>Años de Graduación</label>
                                    {!!Form::select('anios[]',$anios,null, ['class' => 'select2','multiple','id'=>'anios'])!!}
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Periodos Académicos</label>
                                    {!!Form::select('periodos[]', $periodos,null, ['class' => 'select2','multiple','id'=>'periodos'])!!}
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <a class="btn btn-primary" style="color: white;" onclick="cargar_informacion_encuestados();">FILTRAR</a>
                                </div>                                                
                            </div>
                        </div>    

                    </div>

                          <div class="card">
                    <div class="card-header row">
                      <div class="col-md-6"> 
                      <h3 class="card-title" id="total_respuestas_txt"></h3>
                      </div>
                       <div class="col-md-3"> 
                        {!!Form::select('tipo_respuestas', array('SI'=>'SI RESPONDIÓ',
                                                            'NO'=>'NO RESPONDIÓ'),null, ['class' => 'select2','id'=>'tipo_respuestas','placeholder'=>'TODOS','onchange'=>'cargar_total_respuestas()'])!!}
                        </div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-5" id="estadistico_encuestados"> >               
                        </div>
                        <div class="col-md-7">                     
                              <div class="table-responsive">
                                  <table cellspacing="0" class="table" id="tabla_encuestados" width="120%">
                                      <thead>
                                          <tr>
                                              <th>
                                                  Respondió
                                              </th>
                                              <th>
                                                  Identificación
                                              </th>
                                              <th>
                                                  Nombres
                                              </th>
                                              <th>
                                                  Período
                                              </th>
                                              <th>
                                                  Correo electrónico
                                              </th>  
                                               <th>
                                                  Teléfono
                                              </th>                                              
                                          </tr>
                                      </thead>
                                      <tbody>
                                      </tbody>
                                  </table>
                                </div>
                            </div>                        
                      </div>                             
                    </div>
                </div>

                    <div class="card">
                    <div class="card-header">
                      <center><h3 class="card-title" id="encuestados"></h3></center>    
                    </div>
                    <div class="card-body">
                      <center>
                    <a class="btn btn-success right" style="color: white;" onclick="obtener_pdf();"><i class="fa fa-print"></i> IMPRIMIR REPORTE</a>
                    </center>                      
                    </div>
                </div>
            </div>
            <div id="res_preguntas">
              
            </div>


<style type="text/css">
.ct-chart .ct-label.ct-horizontal {
  text-align: left;
}
 .dot {
  height: 15px;
  width: 15px;
  border-radius: 100%;
 display: inline-block;
 margin-left: 1%;
 margin-top: 1%;
}


</style>
@endsection
@section('footer_scripts')
<script src="{{asset('assets/vendors/echart/echarts.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/alertifyjs/alertify.min.js') }}"></script>
<script src="{{asset('js/underscore.min.js')}}"></script>
<script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datatables/js/buttons.print.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datatables/js/buttons.html5.min.js')}}" type="text/javascript"></script>
<script>
  var colores=[];
  colores['Total']="#5CB65F";
  colores['que han respondido']="#B4DF10";
  colores['que no han respondido']="#DF4C10";

    var tabla_resultados=null;
    var datos_resultados=[];
    var total_encuestados=0;
    var preguntas_set=[];
    function mostrar_info(id,tipo=0){

        var act= _.findWhere(datos_resultados, {id});
        var pregunta=act.pregunta;
        var preguntax=act.pregunta;

        //si son respuestas con opciones ya
        if(tipo>0){
            var opcion = _.findWhere(preguntas_set, {id});
            opcion=_.findWhere(opcion.opciones, {id:tipo});
            preguntax=opcion.opcion;
            act=_.findWhere(act.resultados, {id: tipo});
        }

        var labelsx=[];
        var datasx=[];
        var colorsx=[];
        var columnas='';
        var porcentaje_total=0;
                for (var i = 0; i < act.resultados.length; i++) {
                    var color=act.resultados[i].color;
                    var porcentaje=(parseInt(act.resultados[i].total)/parseInt(act.resultados[i].total_respuestas))*100;
                    porcentaje=parseFloat(porcentaje.toFixed(2));
                    porcentaje_total+=porcentaje;
                    columnas+=`
                    <tr>
                    <td style="font-size:9px;">${act.resultados[i].opcion}<span class="dot" style=" background-color:  ${color} ;"></span></td>
                    <td align="right" style="font-size:10px;">${parseInt(act.resultados[i].total)}</td>
                    <td align="right" style="font-size:10px;">${porcentaje} %</td>
                    </tr>
                    `;
                    colorsx.push(color);
                    datasx.push({value:parseInt(act.resultados[i].total),name:act.resultados[i].opcion});
                }

            porcentaje_total=Math.round(porcentaje_total).toFixed(2);
            var tabla=`<div class="col-md-12" >
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">${preguntax}</h3></div>
                                <div class="card-body container-fluid">
                                    <div class="row row-lg">
                                        <div class="col-md-12 col-lg-7">
                                            <div class="example-wrap">

                                                 <div class="card-body">
                                                    <div id="pie_modal" style="height: 300px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-lg-5">
                                            <div class="example-wrap">
                                                 <div class="card-body" id="tabla_resultado">
                                                    <table class="table">
                                                    <thead>
                                                        <tr>
                                                        <th>Opción</th>
                                                        <th align="right">Valor</th>
                                                        <th align="right">%</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    ${columnas}
                                                    </tbody>
                                                    <tfooter>
                                                    <tr>
                                                    <td>TOTAL</td>
                                                    <td align="right" style="font-size:10px;">${total_encuestados}</td>
                                                    <td align="right" style="font-size:10px;">${porcentaje_total} %</td>
                                                    </tr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;

          alertify.ubicacion_modal ||alertify.dialog('ubicacion_modal',function(){
            return {
                main:function(content){
                    this.setContent(content);
                },
                setup: function() {
                      return {
                          buttons: [{
                              text: "Cerrar",
                              scope: 'primary',
                              key: 27 /*Esc*/ ,
                              className: 'btn btn-primary'
                          }],
                          focus: {
                              element: function() {},
                              select: true
                          },
                          options: {
                              basic: false,
                              modal: true,
                              maximizable: false,
                              resizable: true,
                              padding: true,
                              transition: 'zoom',
                              pinned: true,
                              pinnable: true
                          }
                      };
                  },
                  settings: {}
              };
        });
        alertify.ubicacion_modal(tabla).resizeTo('95%','80%').setHeader(`<div style="text-align: center; ">
        <div class="centr_alert" ><strong>${pregunta}</strong></div></div> `);
          var D=document.getElementById("pie_modal");
          if(D)
          {
              var T=echarts.init(D);
              T.setOption({
                  color:colorsx,
                  grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                  xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                  yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                  tooltip:{
                      trigger:"item",
                      formatter:"{a} <br/>{b} : {c} ({d}%)"},
                  series:[
                  {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                  data:datasx,
                      itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                  ]
              }),
            $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
          }
        }
    function obtener_pdf(){
        var carreras=$('#carreras').val();
        var anios=$('#anios').val();
        var periodos=$('#periodos').val();
        carga_ajax();
        $.ajax({
                url: '{{url('obtener_pdf')}}',
                type: 'GET',
                data:{id:"{{ encrypt($encuesta->id) }}",carreras,anios,periodos}
            }).done(function(resultado){ 
               var mywindow = window.open('', 'Reporte de Resultados', 'height=400,width=600');
                mywindow.document.write(resultado);
                mywindow.document.close();
                setTimeout(function() {
                   mywindow.focus();
                    mywindow.print();
                    mywindow.close();

                }, 3000);
            }).always(function(){
              desaparecer_carga_ajax();
        });
    }


    function cargar_total_respuestas(){
      carga_ajax();
      var carreras=$('#carreras').val();
      var tipo_respuestas=$('#tipo_respuestas').val();
      var anios=$('#anios').val();
      var periodos=$('#periodos').val();
      var id={{ $encuesta->id }};    
      if(tabla_resultados!=null){        
        tabla_resultados.destroy();
      }
      tabla_resultados=$('#tabla_encuestados').DataTable({
                responsive : true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                language: {
                    "emptyTable":     "No hay Usuarios  aún",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ Usuarios",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 Usuarios",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
              
                    "order": ([ 0, 'desc' ]),
        "dom": '<"m-t-10"B><"m-t-10 pull-left"f><"m-t-10 pull-right"l>rt<"pull-left m-t-10"i><"m-t-10 pull-right"p>',
        buttons: [
            'copy', 'csv', 'print'
        ],
        "ordering": false,
        scrollY:        300,
        deferRender:    true,
        scroller:       true,
        "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('total_respuestas_ajax') }}",
                     "dataType": "json",
                     "type": "GET",
                     "data":{ _token: "{{csrf_token()}}",carreras,anios,periodos,id,tipo_respuestas,id:"{{ $encuesta->id }}"}
                   },
            "columns": [
                { "data": "respuesta","orderable":false },
                { "data": "identificacion","orderable":false },
                { "data": "nombres","orderable":false },                
                { "data": "periodo_academico","orderable":false }, 
                { "data": "email","orderable":false },                
                { "data": "telefono","orderable":false },
                ]                             
            });  


                $.ajax({
                url: '{{url('consultar_resultados_encuestas_cantidad')}}',
                type: 'GET',
                data:{id,carreras,anios,periodos}
            }).done(function(data){
              var datax=[];                
              var colorsx=[];
              colorsx.push(colores['que han respondido']);
              datax.push({value:parseInt(data['que han respondido']),name:"que han respondido"});
              colorsx.push(colores['que no han respondido']);
              datax.push({value:parseInt(data['que no han respondido']),name:"que no han respondido"});
              $('#total_respuestas_txt').html('TOTAL DE GRADUADOS '+data['Total']);
              /*
              $.each(data,function(index,value){
                var col=randomColor();
                colorsx.push(colores[index]);
                datax.push({value:parseInt(value),name:index});
              });
              */
      
              
                  var D=document.getElementById("estadistico_encuestados");
                  if(D)
                  {
                    var T=echarts.init(D);
                    T.setOption({
                    color:colorsx,
                    grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                    xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                    yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                    tooltip:{
                        trigger:"item",
                        formatter:"{a} <br/>{b} : {c} ({d}%)"},
                        series:[
                                  {   
                                    name:"Profesionales",type:"pie",radius:"75%",center:["50%","50%"],
                                    data:datax,
                                    itemStyle:
                                    {
                                      emphasis:
                                      {
                                        shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"
                                      }
                                    }
                                  }
                              ]
                    }),
                    $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
                    }
                    
            }).always(function(){
              desaparecer_carga_ajax();
            });






    }

    function cargar_informacion_encuestados(){
      cargar_total_respuestas();
      cargar_resultados();
    }

    function cargar_resultados(){   
        carga_ajax();    
        var carreras=$('#carreras').val();
        var anios=$('#anios').val();
        var periodos=$('#periodos').val();
         $.ajax({
                url: '{{url('consultar_resultados_encuestas')}}',
                type: 'GET',
                data:{id:"{{ $encuesta->id }}",carreras,anios,periodos}
            }).done(function(data){
              preguntas_set=data.resultados;
            $('#res_preguntas').html(data.vista);
            datos_resultados=[];
                    var datos=data.resultados;
                    total_encuestados=parseInt(data.total);
                    $('#encuestados').html("<strong>Total Encuestados:</strong> "+data.total);
                    for (var i = 0; i < datos.length; i++) {
                        if(datos[i].resultados){
                                var resultados=datos[i].resultados;
                                if(datos[i].opciones_respuesta==0){
                                    var labelsx=[];
                                    var datasx=[];
                                    var colorsx=[];
                                    var resultadosx=[];
                                    var columnas='';    
                                    var porcentaje_total=0;
                                    if(datos[i].resultados.length>0){
                                          for (var j = 0; j < resultados.length; j++) {
                                                var col=randomColor();
                                                colorsx.push(col);
                                                datasx.push({value:parseInt(resultados[j].total),name:resultados[j].opcion});
                                                resultadosx.push({total:resultados[j].total,opcion:resultados[j].opcion,color:col,total_respuestas:parseInt(datos[i].total_respuestas)});
                                                var porcentaje=(parseInt(resultados[j].total)/parseInt(datos[i].total_respuestas))*100;
                                                porcentaje=parseFloat(porcentaje.toFixed(2));
                                                porcentaje_total+=porcentaje;            
                                                columnas+=`
                                                <tr>
                                                <td style="font-size:9px;">${resultados[j].opcion}<span class="dot" style=" background-color:  ${col} ;"></span></td>
                                                <td align="right" style="font-size:10px;">${parseInt(resultados[j].total)}</td>
                                                <td align="right" style="font-size:10px;">${porcentaje} %</td>
                                                </tr>
                                                `;    
                                            }
                                    }else{
                                        datasx.push({value:0,name:"Sin datos"});
                                    }
                                    datos_resultados.push({id:datos[i].id,resultados:resultadosx,pregunta:datos[i].pregunta}); 
                                    porcentaje_total=Math.round(porcentaje_total).toFixed(2);  
                                    var tabla=`<table class="table">
                                                 <thead>
                                                    <tr>
                                                    <th>Opción</th>
                                                    <th align="right">Valor</th>
                                                    <th align="right">%</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                ${columnas}
                                                </tbody>
                                                <tfooter>
                                                <tr>
                                                <td>TOTAL</td>
                                                <td align="right" style="font-size:10px;">${datos[i].total_respuestas}</td>
                                                <td align="right" style="font-size:10px;">${porcentaje_total} %</td>
                                                </tr>
                                                </table>
                                     `;
                                     $('#tabla_resultado'+datos[i].id).html(tabla);
                                    var D=document.getElementById("pie"+datos[i].id);
                                    if(D)
                                    {
                                        var T=echarts.init(D);
                                        T.setOption({
                                            color:colorsx,
                                            grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                                            xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                            yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                            tooltip:{
                                                trigger:"item",
                                                formatter:"{a} <br/>{b} : {c} ({d}%)"},
                                            series:[
                                            {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                                            data:datasx,
                                                itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                                            ]
                                        }),
                                        $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
                                    }
                                }else{

                                    var resultadospreguntas=[];
                                    for (var j = 0; j < resultados.length; j++) {
                                        if(resultados[j].resultados){
                                                var labelsx=[];
                                                var datasx=[];
                                                var colorsx=[];
                                                var result=resultados[j].resultados;
                                                var resultados_opciones=[]; 
                                                var columnas='';    
                                                var porcentaje_total=0;  
                                                 if(result.length>0){
                                                    for (var k = result.length - 1; k >= 0; k--) {
                                                    var col=randomColor();
                                                    datasx.push({value:parseInt(result[k].total),name:result[k].opcion});
                                                    colorsx.push(col);
                                                    resultados_opciones.push({total:result[k].total,opcion:result[k].opcion,color:col,id:result[k].id,total_respuestas:parseInt(resultados[j].total_respuestas)});
                                                    var porcentaje=(parseInt(result[k].total)/parseInt(resultados[j].total_respuestas))*100;
                                                  porcentaje=parseFloat(porcentaje.toFixed(2));
                                                  porcentaje_total+=porcentaje;            
                                                  columnas+=`
                                                  <tr>
                                                  <td style="font-size:9px;">${result[k].opcion}<span class="dot" style=" background-color:  ${col} ;"></span></td>
                                                  <td align="right" style="font-size:10px;">${parseInt(result[k].total)}</td>
                                                  <td align="right" style="font-size:10px;">${porcentaje} %</td>
                                                  </tr>
                                                  `;  
                                                    }


                                                  
                                                  }  
                                                 else{
                                                    resultados_opciones.push({total:0,opcion:"Sin Datos",color:col});
                                                    datasx.push({value:0,name:"Sin datos"});
                                                 }


                                                resultadospreguntas.push({id:resultados[j].id,resultados:resultados_opciones});


                                                porcentaje_total=Math.round(porcentaje_total).toFixed(2);  
                                                var tabla=`<table class="table">
                                                             <thead>
                                                                <tr>
                                                                <th>Opción</th>
                                                                <th align="right">Valor</th>
                                                                <th align="right">%</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            ${columnas}
                                                            </tbody>
                                                            <tfooter>
                                                            <tr>
                                                            <td>TOTAL</td>
                                                            <td align="right" style="font-size:10px;">${parseInt(resultados[j].total_respuestas)}</td>
                                                            <td align="right" style="font-size:10px;">${porcentaje_total} %</td>
                                                            </tr>
                                                            </table>
                                                 `;

                                                $('#tabla_resultado'+datos[i].id+""+resultados[j].id).html(tabla);

                                            
                                                var selector="pie"+datos[i].id+""+resultados[j].id;       
                                                var D=document.getElementById(selector);
                                                if(D)
                                                {
                                                    var T=echarts.init(D);
                                                    T.setOption({
                                                        color:colorsx,
                                                        grid:{left:"3%",right:"4%",bottom:"3%",containLabel:!0},
                                                        xAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                                        yAxis:[{axisLine:{show:!1},splitLine:{show:!1}}],
                                                        tooltip:{
                                                            trigger:"item",
                                                            formatter:"{a} <br/>{b} : {c} ({d}%)"},
                                                        series:[
                                                        {   name:"Respuesta",type:"pie",radius:"75%",center:["50%","50%"],
                                                        data:datasx,
                                                            itemStyle:{emphasis:{shadowBlur:10,shadowOffsetX:0,shadowColor:"rgba(0, 0, 0, 0.5)"}}}
                                                        ]
                                                    }),
                                                    $(window).on("resize",function(){setTimeout(function(){T.resize()},500)})
                                                }
                                        }
                                    }
                                datos_resultados.push({id:datos[i].id,resultados:resultadospreguntas,pregunta:datos[i].pregunta});
                                }
                        }
                      }
                }).always(function(){
                  desaparecer_carga_ajax();
                });
    }

 $(".select2").select2({width: "100%"});
    $(document).ready(function() {
        cargar_informacion_encuestados();        
       //cargar_total_respuestas();
    });

function randomColor()
{
  return 'rgba('+randomColorFactor()+','+randomColorFactor()+','+randomColorFactor()+',.7)';
}

function randomColorFactor()
{
  return Math.round(Math.random()*255);
}
        </script>
@endsection
