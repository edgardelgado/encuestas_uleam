@extends('layouts.correos')
@section('titulo')
	Confirmación de Correo Electrónico
@endsection
@section('encabezado')
@endsection
@section('contenido')
<div>
    <table border="0" cellpadding="0" cellspacing="0" style="margin:20px auto 0px auto; font-size:14px; font-family:Helvetica,Arial,Sans-serif; color:#333; background:#fff; border:0px; padding:30px 20px" width="600">
        <tbody>
            <tr>
                <td style="">
                    <h2 style="font-size:20px; color:#333; margin:0 0 5px 0; padding:0px">
                        Hola {{$nombres}}:
                    </h2>
                    <p style="line-height:22px; color:#666">
                        Este es el código para que puedas iniciar sesión:
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="margin-top:0px; margin-left:auto; margin-right:auto; font-size:24px; font-family:Helvetica,Arial,Sans-serif; background:#EEE; border:1px solid #ccc; padding:30px 10px" width="600">
        <tbody>
            <tr>
                <td>
                    <p align="center" style="line-height:0px; color:#666">
                        <strong>
                            {{$clave2}}
                        </strong>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="font-size:12px; font-family:Helvetica,Arial,Sans-serif; color:#333; margin-top:40px" width="600">
        <tbody>
            <tr>
                <td>
                    <p style="padding:0px; margin:0 0 0 20px; font-size:11px; color:#848484">
                        ©
                        <span class="markkc04wmthm" data-markjs="true" data-ogab="" data-ogac="" data-ogsb="" data-ogsc="" style="background-color: rgb(255, 241, 0); color: black;">
                            Uleam
                        </span>
                        2019
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
@section('pie')
<p>
    Gracias por su atención,
</p>
<p>
    Atentamente,
    <br>
        <strong>
            Sistema de Encuestas Uleam.
        </strong>
    </br>
</p>
@endsection
