
var tipos_val_caja=[];
tipos_val_caja[1]='Apertura';
tipos_val_caja[2]='Cierre';
var val_caja=[];
val_caja[1]='Abrir';
val_caja[2]='Cerrar';
var valor_caja=[];
valor_caja[1]='Inicial';
valor_caja[2]='Final';



function toastx(head, content, icon, time=0,position='top-right') {
    if (time == 0) {
        time = 5000;
    }
    $.toast({
    heading:head,
    text:content,
    //position: 'top-right',
    position: position,
    stack: 3,
    icon: icon,
    hideAfter: time,
    showHideTransition: 'plain',
    allowToastClose: true,
    loader: false
    });
}



function numeros(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[0-9]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function letras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[A-Za-zÑñ\síÍéÉáÁóÓúÚ]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function decimales(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[0-9.]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function numerosletras(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[A-Za-zÑñ0-9]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function mail(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true;
    var patron =/[A-Za-zÑñ0-9@.\-_]/;
    var te = String.fromCharCode(tecla);
    return patron.test(te);
}

function cedula(num_cedula) {
    var cedula = num_cedula;

    //Preguntamos si la cedula consta de 10 digitos
    if(cedula.length == 10){

        //Obtenemos el digito de la region que sonlos dos primeros digitos
        var digito_region = cedula.substring(0,2);

        //Pregunto si la region existe ecuador se divide en 24 regiones
        if( digito_region >= 1 && digito_region <=24 ){

          // Extraigo el ultimo digito
          var ultimo_digito   = cedula.substring(9,10);

          //Agrupo todos los pares y los sumo
          var pares = parseInt(cedula.substring(1,2)) + parseInt(cedula.substring(3,4)) + parseInt(cedula.substring(5,6)) + parseInt(cedula.substring(7,8));

          //Agrupo los impares, los multiplico por un factor de 2, si la resultante es > que 9 le restamos el 9 a la resultante
          var numero1 = cedula.substring(0,1);
          var numero1 = (numero1 * 2);
          if( numero1 > 9 ){ var numero1 = (numero1 - 9); }

          var numero3 = cedula.substring(2,3);
          var numero3 = (numero3 * 2);
          if( numero3 > 9 ){ var numero3 = (numero3 - 9); }

          var numero5 = cedula.substring(4,5);
          var numero5 = (numero5 * 2);
          if( numero5 > 9 ){ var numero5 = (numero5 - 9); }

          var numero7 = cedula.substring(6,7);
          var numero7 = (numero7 * 2);
          if( numero7 > 9 ){ var numero7 = (numero7 - 9); }

          var numero9 = cedula.substring(8,9);
          var numero9 = (numero9 * 2);
          if( numero9 > 9 ){ var numero9 = (numero9 - 9); }

          var impares = numero1 + numero3 + numero5 + numero7 + numero9;

          //Suma total
          var suma_total = (pares + impares);

          //extraemos el primero digito
          var primer_digito_suma = String(suma_total).substring(0,1);

          //Obtenemos la decena inmediata
          var decena = (parseInt(primer_digito_suma) + 1)  * 10;

          //Obtenemos la resta de la decena inmediata - la suma_total esto nos da el digito validador
          var digito_validador = decena - suma_total;

          //Si el digito validador es = a 10 toma el valor de 0
          if(digito_validador == 10)
            var digito_validador = 0;

          //Validamos que el digito validador sea igual al de la cedula
          if(digito_validador == ultimo_digito){
            console.log('la cedula es correcta');
          }else{
            console.log('la cedula:' + cedula + ' es incorrecta');
          }
        }else{
          // imprimimos en consola si la region no pertenece
          console.log('Esta cédula no pertenece a ninguna region');
        }
    }else{
        //imprimimos en consola si la cedula tiene mas o menos de 10 digitos
        console.log('Esta cédula tiene menos de 10 Digitos');
    }
}

function carga_ajax() {
  $(".page-loader-wrapper").show();
}

function desaparecer_carga_ajax() {
 $(".page-loader-wrapper").fadeOut();
}

function validacionCampos(jqXHR, textStatus, errorThrown) {
  toastr.remove();
  if (jqXHR.status == 422) {
    var errores = '<ul>';
    var ObjetoDeErrores = jqXHR.responseJSON;
    ObjetoDeErrores = jqXHR.responseJSON.errors;
    for (var clave in ObjetoDeErrores){
      if (ObjetoDeErrores.hasOwnProperty(clave)) {
        for (var i = 0; i < ObjetoDeErrores[clave].length; i++) {
          errores+='<li>'+ObjetoDeErrores[clave][i]+'</li>';
        }
      }
    }
    errores+='</ul>';
    return toastr['error'](errores);
  }else if (jqXHR.status == 404) {
    return toastr['error']('No se encontró la URL solicitada');
  }else if (jqXHR.status == 500) {
    return toastr['error']('Error interno del servidor, recargue la página e inténtelo de nuevo, si el problema persiste comuniquelo a sistema');
  }
}


//para caja

 